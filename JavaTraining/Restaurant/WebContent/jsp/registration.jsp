<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title><tag:getresource key="jsp.title.registration"/></title>
</head>
<body class="body">
	<div class="authorization">
		<tag:getresource key="jsp.title.registration.description"/>
		<form method="POST" action="/Restaurant/controller">
			<table>
				<tr>
					<td><label for="login"><tag:getresource key="jsp.title.login"/></label></td>
					<td><input type="text" name="login" id="login" required pattern="<tag:getresource key="pattern.login"/>"/></td>
				</tr>
				<tr>
					<td><label for="email">E-Mail</label></td>
					<td><input type="email" name="email" id="email" pattern="<tag:getresource key="pattern.mail"/>" /></td>
				</tr>
				<tr>
					<td><label for="password"><tag:getresource key="jsp.title.password"/></label></td>
					<td><input type="password" name="password" id="password" required pattern="<tag:getresource key="pattern.password"/>"/></td>
				<tr>
					<td><label for="password2"><tag:getresource key="jsp.title.confirm.password"/></label></td>
					<td><input type="password" name="password2" id="password2" required pattern="<tag:getresource key="pattern.password"/>"/></td>
				</tr>

			</table>
			<p>
				<input type="hidden" name="commandName" value="REGISTRATION" />
				<button type="submit"><tag:getresource key="jsp.button.register"/></button>
			</p>
			<font color="red"><tag:messages/></font>
		</form>
		<a class="button" href="/Restaurant"><tag:getresource key="jsp.title.back"/></a>
	</div>
</body>
</html>