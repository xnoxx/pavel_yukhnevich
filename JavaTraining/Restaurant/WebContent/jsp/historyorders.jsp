<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />

<c:if test="${user.authority == 2}">
	<c:if test="${not empty ordersList}">
		<h3>
			<tag:getresource key="jsp.title.history.paid" />
		</h3>


		<form ACTION="controller">

			<table class="simple-little-table" cellspacing='0'>

				<tr>
					<th><tag:getresource key="jsp.table.order" /> ID</th>
					<th><tag:getresource key="jsp.table.client" /> ID</th>
					<th><tag:getresource key="jsp.table.content" /></th>
					<th><tag:getresource key="jsp.table.price.total" /></th>
				</tr>

				<c:forEach items="${ordersList}" var="position">

					<tr>
						<td>${position.id}</td>
						<td>${position.clientId}</td>
						<td><c:forEach items="${position.dishsList}" var="position1">
								${position1.dish}<br>
							</c:forEach></td>
						<td>${position.price}<br></td>
					</tr>

				</c:forEach>

			</table>

			<input type="hidden" name="commandName" value="CLEAR_HISTORY">
			<button type="submit">
				<tag:getresource key="jsp.button.history.clear" />
			</button>
			<div id="contentBody"></div>
		</form>
	</c:if>
	<c:if test="${empty ordersList}">
		<tag:messages />
	</c:if>

</c:if>