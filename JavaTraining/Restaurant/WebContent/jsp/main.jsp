<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
<title>Chili Pizza <tag:getresource key="jsp.title.main"/></title>
</head>
<body class="body">
	<table width="100%" style="height: 100%">
		<tr>
			<td colspan="3"><jsp:include page="header.jsp" /></td>
		</tr>
		<tr>
			<td class="menu">
				<c:if test="${user.authority == 2}">
                	<jsp:include page="adminmenu.jsp"/>
                </c:if>
          		<c:if test="${user.authority == 1}">
                	<jsp:include page="usermenu.jsp"/>
                </c:if>
                <c:if test="${user == null}">
                	<jsp:include page="unauthmenu.jsp"/>
                </c:if>
			</td>
			<td class="delimiter" />
			<td class="main">
				<c:if test="${not empty bodyPage}">
                        <jsp:include page="${bodyPage}"/>
                    </c:if>
				<div id="contentBody">
					<jsp:include page="welcome.jsp" />
					<tag:messages/>
				</div>
				<div id="loading" style="display: none"><tag:getresource key="jsp.title.loading"/></div>
			
			</td>
		</tr>
		<tr><td colspan="3"><jsp:include page="footer.jsp"/></td></tr>
	</table>
</body>
</html>