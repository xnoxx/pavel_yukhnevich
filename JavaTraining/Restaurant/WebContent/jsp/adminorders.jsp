<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />

<c:if test="${user.authority == 2}">
	<c:if test="${empty ordersList && empty ordersOnKitchenList}">
	<tag:getresource key="jsp.title.orders.no"/>
	</c:if>
	<c:if test="${not empty ordersList}">
		<h3><tag:getresource key="jsp.title.orders.not.accepted"/>:</h3>
		

		<form ACTION="controller">

			<table class="simple-little-table" cellspacing='0'>

				<tr>
					<th><tag:getresource key="jsp.table.order"/> ID</th>
					<th><tag:getresource key="jsp.table.client"/> ID</th>
					<th><tag:getresource key="jsp.table.content"/></th>
					<th><tag:getresource key="jsp.table.price.total"/></th>
					<th><tag:getresource key="jsp.table.onkitchen"/></th>
					<th><tag:getresource key="jsp.table.payroll"/></th>
					<th><tag:getresource key="jsp.table.choose"/></th>
				</tr>

				<c:forEach items="${ordersList}" var="position">

					<tr>
						<td>${position.id}</td>
						<td>${position.clientId}</td>
						<td><c:forEach items="${position.dishsList}" var="position1">
								${position1.dish}<br>
							</c:forEach></td>
						<td>${position.price}<br>
						</td>
						<td>${position.onKitchen}</td>
						<td>${position.payment}</td>
						<td><input type="checkbox" name="id" value="${position.id}">
						</td>
					</tr>

				</c:forEach>

			</table>

			<input type="hidden" name="commandName" value="TAKE_AND_BILL">
			<button type="submit"><tag:getresource key="jsp.button.bill.take"/></button>
			<div id="contentBody"></div>
		</form>
	</c:if>
	
	<c:if test="${not empty ordersOnKitchenList}">
		<h3>Non-paid orders:</h3>

			<table class="simple-little-table" cellspacing='0'>

				<tr>
					<th><tag:getresource key="jsp.table.order"/> ID</th>
					<th><tag:getresource key="jsp.table.client"/> ID</th>
					<th><tag:getresource key="jsp.table.content"/></th>
					<th><tag:getresource key="jsp.table.price.total"/></th>
					<th><tag:getresource key="jsp.table.onkitchen"/></th>
					<th><tag:getresource key="jsp.table.payroll"/></th>
				</tr>

				<c:forEach items="${ordersOnKitchenList}" var="position">

					<tr>
						<td>${position.id}</td>
						<td>${position.clientId}</td>
						<td><c:forEach items="${position.dishsList}" var="position1">
								${position1.dish}<br>
							</c:forEach></td>
						<td>${position.price}<br>
						</td>
						<td>${position.onKitchen}</td>
						<td>${position.payment}</td>
					</tr>

				</c:forEach>

			</table>

			<div id="contentBody"></div>
	</c:if>
</c:if>