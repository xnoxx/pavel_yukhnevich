<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />
<div class="header">
<table  width="100%" style="height:100%">
    <tr>
        <td class="logo">
           
        </td>
        <td class="mainlanguages">
        	<tag:languagemenu forwardString="jsp/main.jsp"/><br/>
           <c:if test="${user != null}">
              	<tag:getresource key="jsp.title.entered"/> <b>${user.login}</b>.
              	<a href="controller?commandName=EXIT">Exit</a>
           </c:if>
           <c:if test="${user == null}">
              	<a href="jsp/authorization.jsp"><tag:getresource key="jsp.title.enter"/></a>
           </c:if>
        </td>
    </tr>
</table>
</div>

