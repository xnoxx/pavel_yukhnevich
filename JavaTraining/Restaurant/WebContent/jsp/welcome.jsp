<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<h4>
	<c:if test="${user != null}">
		<tag:getresource key="jsp.message.hello"/><c:out value=", ${user.login}! " />
	</c:if>
	<tag:getresource key="jsp.message.welcome"/>
</h4>
