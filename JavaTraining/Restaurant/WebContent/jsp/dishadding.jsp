<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<c:if test="${user.authority == 2}">
<div id="contentBody">					
<h3><tag:getresource key="jsp.title.dish.add"/></h3>
		<form action="controller">
			<table>
				<tr>
					<td><tag:getresource key="jsp.title.dish.name"/></td>
					<td><input type="text" name="dish" /></td>
				</tr>
				<tr>
					<td><tag:getresource key="jsp.table.price"/></td>
					<td><input type="text" name="cost" /></td>
				</tr>
				<tr>
					<td></td>
					<td>
					<input type="hidden" name="commandName" value="ADD_DISH" /> 
    				<button type="submit"><tag:getresource key="jsp.button.add"/></button>
					</td>
				</tr>
			</table>
		</form>
</div>
</c:if>