<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tag:getresource key="jsp.title.error"/></title>
    </head>
    <body>
        <h3><tag:getresource key="jsp.title.error"/></h3>
        <div class="messages">
            <tag:messages/>
        </div>
        <form action="controller" method="POST">
            <input type="hidden" name="commandName" value="TO_PAGE"/>
            <input type="hidden" name="forwardPage" value="<tag:getresource key="forward.authorization"/>"/>
            <input type="submit" value="<tag:getresource key="jsp.button.to.authorization"/>"/>
        </form>
    </body>
</html>
