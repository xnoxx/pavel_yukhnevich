<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css"href="/Restaurant/css/style.css" />
<meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
<script>
	function showContent(link) {
		var cont = document.getElementById('contentBody');
		var loading = document.getElementById('loading');

		cont.innerHTML = loading.innerHTML;

		var http = createRequestObject();
		if (http) {
			http.open('get', link);
			http.onreadystatechange = function() {
				if (http.readyState == 4) {
					cont.innerHTML = http.responseText;
				}
			}
			http.send(null);
		} else {
			document.location = link;
		}
	}

	// создание ajax объекта  
	function createRequestObject() {
		try {
			return new XMLHttpRequest()
		} catch (e) {
			try {
				return new ActiveXObject('Msxml2.XMLHTTP')
			} catch (e) {
				try {
					return new ActiveXObject('Microsoft.XMLHTTP')
				} catch (e) {
					return null;
				}
			}
		}
	}
</script>
<div class="menu">
	<a class="menu" onclick="location.replace(/Restaurant/)"> 
    	<tag:getresource key="jsp.link.main"/> 
	</a>
	 <a class="menu" onclick="showContent('controller?commandName=LOAD_MENU')">
        <tag:getresource key="jsp.link.menu"/>
    </a> 
    <a class="menu" onclick="showContent('controller?commandName=LOAD_ORDER')"> 
    	<tag:getresource key="jsp.title.orders.your"/>
	</a>
</div>


