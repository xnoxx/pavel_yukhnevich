<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<h3><tag:getresource key="jsp.title.menu"/></h3>
<c:if test="${user == null}">
    <table class="simple-little-table" cellspacing='0'>
 
        <tr>
            <th><tag:getresource key="jsp.table.dish"/></th>
            <th><tag:getresource key="jsp.table.price"/></th>
        </tr>

        <c:forEach items="${menuPositionList}" var="position">
            <tr>
                <td>${position.dish}</td>
				<td>${position.cost}</td>
            </tr>
        </c:forEach>        
    </table>
</c:if>    

<c:if test="${user.authority == 1}">
<form ACTION="controller" >
    <table class="simple-little-table" cellspacing='0'>
 
        <tr>
            <th><tag:getresource key="jsp.table.dish"/></th>
            <th><tag:getresource key="jsp.table.price"/></th>
            <th><tag:getresource key="jsp.table.choose"/></th>
        </tr>

        <c:forEach items="${menuPositionList}" var="position">
            <tr>
                <td>${position.dish}</td>
				<td>${position.cost}</td>
				<td><input type="checkbox" name="id" value="${position.id}"></td>
            </tr>
        </c:forEach>
    </table>
    <input type="hidden" name="userId" value="${user.id}">
    <input type="hidden" name="commandName" value="ORDER">
    <button type="submit"><tag:getresource key="jsp.table.order"/></button>
   <div id="contentBody">
						
</div>
</form>

</c:if>

<c:if test="${user.authority == 2}">
<form ACTION="controller" >
    <table class="simple-little-table" cellspacing='0'>
 
        <tr>
            <th><tag:getresource key="jsp.table.dish"/></th>
            <th><tag:getresource key="jsp.table.price"/></th>
            <th><tag:getresource key="jsp.table.choose"/></th>
        </tr>

        <c:forEach items="${menuPositionList}" var="position">
            <tr>
                <td>${position.id}</td>
                <td>${position.dish}</td>
				<td>${position.cost}</td>
				<td><input type="checkbox" name="id" value="${position.id}"></td>
            </tr>
        </c:forEach>
    </table>
    <input type="hidden" name="userId" value="${user.id}">
    <input type="hidden" name="commandName" value="DELETE_DISH">
    <button type="submit"><tag:getresource key="jsp.button.delete"/></button>
</form>
    
<div id="contentBody">					
</div>

</c:if>