<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css" />

<c:if test="${user.authority == 1}">
	<h3><tag:getresource key="jsp.title.orders.your"/>:</h3>
	<tag:messages />
	<c:forEach items="${ordersList}" var="position">
		<h3><i><font color="gray"><tag:getresource key="jsp.table.order"/> №${position.id}:</font></i></h3>

		<form ACTION="controller">

			<table class="simple-little-table" cellspacing='0'>

				<tr>
					<th><tag:getresource key="jsp.table.dish"/></th>
           			<th><tag:getresource key="jsp.table.price"/></th>
					<th><tag:getresource key="jsp.table.onkitchen"/></th>
					<th><tag:getresource key="jsp.table.payroll"/></th>
					<c:if test="${position.onKitchen == false}">
						<th><tag:getresource key="jsp.table.choose"/></th>
					</c:if>		
				</tr>

				<c:forEach items="${position.dishsList}" var="position1">
					<tr>
						<td>${position1.dish}</td>
						<td>${position1.cost}</td>
						<td>${position.onKitchen}</td>
						<td>${position.payment}</td>
						<c:if test="${position.onKitchen == false}">
							<td><input type="checkbox" name="id" value="${position1.id}"></td>
						</c:if>	
					</tr>
				</c:forEach>

				
				<tr>
					<th><tag:getresource key="jsp.table.price.total"/>: ${position.price} <tag:getresource key="jsp.title.byr"/></th>
				</tr>

			</table>

			
			<input type="hidden" name="orderId" value="${position.id}">
			<c:if test="${position.onKitchen == false}">
				<input type="hidden" name="commandName" value="DELETE_ORDER">
				<button type="submit"><tag:getresource key="jsp.button.delete"/></button>
			</c:if>
			<c:if test="${position.onKitchen == true}">
				<button name="commandName" value="BILL" type="submit"><tag:getresource key="jsp.button.bill"/></button>
			</c:if>
			
			<div id="contentBody"></div>
		</form>

	</c:forEach>
</c:if>