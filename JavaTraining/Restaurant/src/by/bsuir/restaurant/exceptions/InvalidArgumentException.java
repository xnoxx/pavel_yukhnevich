package by.bsuir.restaurant.exceptions;

public class InvalidArgumentException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2743478931261760193L;

	public InvalidArgumentException(String message, Exception e)
    {
        super(message, e);
    }

    public InvalidArgumentException(String message)
    {
        super(message);
    }
	
}
