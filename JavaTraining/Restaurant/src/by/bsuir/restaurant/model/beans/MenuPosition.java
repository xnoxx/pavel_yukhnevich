package by.bsuir.restaurant.model.beans;

import java.io.Serializable;

public class MenuPosition implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6598180141403332820L;
	private String dish = "";
	private int id;
	private int cost;

	public MenuPosition() {
	}

	public String getDish() {
		return dish;
	}

	public void setDish(String dish) {
		this.dish = dish;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
