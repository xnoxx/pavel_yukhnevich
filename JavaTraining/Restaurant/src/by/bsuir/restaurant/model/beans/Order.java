package by.bsuir.restaurant.model.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Order implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1970138702212350979L;
	private int id;
	private int clientId;
	private List<MenuPosition> dishsList = new ArrayList<MenuPosition>();
	private boolean onKitchen;
	private boolean payment;
	private int price;

	public Order() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public boolean isOnKitchen() {
		return onKitchen;
	}

	public void setOnKitchen(boolean onKitchen) {
		this.onKitchen = onKitchen;
	}

	public boolean isPayment() {
		return payment;
	}

	public void setPayment(boolean payment) {
		this.payment = payment;
	}

	public List<MenuPosition> getDishsList() {
		return dishsList;
	}

	public void setDishsList(List<MenuPosition> dishsList) {
		this.dishsList = dishsList;
	}
	
	public int getPrice() {
		price = 0;
		for(MenuPosition mp: dishsList){
			price += mp.getCost();
		}
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
}
