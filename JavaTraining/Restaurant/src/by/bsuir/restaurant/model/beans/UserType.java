package by.bsuir.restaurant.model.beans;

import by.bsuir.restaurant.exceptions.InvalidArgumentException;


public enum UserType {
	ADMIN("admin"), CLIENT("client");

	private String typeValue;

	private UserType(String type) {
		typeValue = type;
	}

	static public UserType getUserType(String string) throws InvalidArgumentException {
		for (UserType type : UserType.values()) {
			if (type.getTypeValue().equals(string.toLowerCase())) {
				return type;
			}
		}
		throw new InvalidArgumentException("Invalid argument " + string);
	}

	public String getTypeValue() {
		return typeValue;
	}
	    
}