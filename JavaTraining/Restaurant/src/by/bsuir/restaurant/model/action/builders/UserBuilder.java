package by.bsuir.restaurant.model.action.builders;

import java.security.NoSuchAlgorithmException;

import by.bsuir.restaurant.model.beans.User;
import by.bsuir.restaurant.model.beans.UserType;
import by.bsuir.restaurant.other.MD5Creator;
import by.bsuir.restaurant.other.MessageManager;
import by.bsuir.restaurant.resource.Resource;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * This class gets user data from
 * request and builds a user
 */
public class UserBuilder {

    /**
     * This is new user which  will is built
     */
    private User user = new User();
    /**
     * This object in which are written reports on incorrect data
     */
    private MessageManager messages;
    protected static Logger logger = Logger.getLogger(UserBuilder.class);
    
    public UserBuilder(MessageManager messages) {
        this.messages = messages;
        user.setUserType(UserType.CLIENT);
    }

    public User getUser() {
        return user;
    }

    /**
     * This method builds a user
     * This method read all user properties, if each property
     * is successfully read, that user is successfully built
     * @param request a HttpServletRequest
     * @return <code>true</code> if user has built successfully
     */
    public boolean build(HttpServletRequest request) {
        boolean readingSuccessful = true;
        readingSuccessful &= readLogin(request);
		readingSuccessful &= readPassword(request);
        return readingSuccessful;
    }

    /**
     * This method gets login from request and check it
     * @param request a HttpServletRequest
     * @return <code>true</code> if login is correct
     */
    private boolean readLogin(HttpServletRequest request) {
        String login = request.getParameter("login");
        if (login != null) {
            user.setLogin(login);
            if (login.matches(Resource.getStr("pattern.login"))) {
                return true;
            }
        }
        messages.addMessage(Resource.getStr("error.pattern.login") + " " + Resource.getStr("pattern.login"));
        return false;
    }

    /**
     * This method gets password from request and check it
     * @param request a HttpServletRequest
     * @return <code>true</code> if password is correct and is equal to confirm password
     */
    private boolean readPassword(HttpServletRequest request) {
        String password = request.getParameter("password");
        try {
        if (password != null) {
				if (password.matches(Resource.getStr("pattern.password"))) {
					if (password.equals(request.getParameter("password2"))) {
						MD5Creator md5Creator = new MD5Creator();
						String hash = "";

						hash = md5Creator.getMD5(password);

						user.setPassword(hash);
						return true;
					} else {
						messages.addMessage(Resource.getStr("error.confirm.password"));
					}
				}
			}
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		}
        messages.addMessage(Resource.getStr("error.pattern.password") + " " + Resource.getStr("pattern.password"));
        return false;
    }
}
