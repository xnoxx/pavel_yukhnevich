package by.bsuir.restaurant.model.action.builders;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.bsuir.restaurant.database.ConnectionPoolException;
import by.bsuir.restaurant.database.dao.MenuPositionDAO;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.model.beans.Order;
import by.bsuir.restaurant.other.MessageManager;
import by.bsuir.restaurant.resource.Resource;

public class OrderBuilder {

	private Order order = new Order();
	private MessageManager messages;

	public OrderBuilder(MessageManager messages) {
		this.messages = messages;
	}

	public Order getOrder() {
		return order;
	}
	
	public boolean build(HttpServletRequest request) throws NumberFormatException, ConnectionPoolException, SQLException {
        boolean readingSuccessful = true;
        readingSuccessful &= readUser(request);
		readingSuccessful &= readListOfDishs(request);
        return readingSuccessful;
    }
	
	private boolean readUser(HttpServletRequest request) {
		String userId = request.getParameter("userId");
		if (userId != null){
			order.setClientId(Integer.parseInt(userId));
			return true;
		}
		messages.addMessage(Resource.getStr("error.access.denied"));
		return false;
	}

	public boolean readListOfDishs(HttpServletRequest request) throws NumberFormatException, ConnectionPoolException, SQLException {
		String select[] = request.getParameterValues("id");
		if (select !=  null) {
			List<MenuPosition> listOfDishsId = new ArrayList<MenuPosition>();
			MenuPositionDAO menuPositionDAO = new MenuPositionDAO();
			for (int i = 0; i < select.length; i++) {
					listOfDishsId.add(menuPositionDAO.read(Integer.parseInt(select[i])));			
			}
			order.setDishsList(listOfDishsId);
			return true;
    	}
        return false;
    }

}
