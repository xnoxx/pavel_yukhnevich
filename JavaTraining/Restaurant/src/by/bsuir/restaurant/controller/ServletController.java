package by.bsuir.restaurant.controller;

import by.bsuir.restaurant.command.Command;
import by.bsuir.restaurant.command.CommandFactory;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 * This class implements the pattern MVC
 * This is Servlet which handles requests
 */
public class ServletController extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = -5225624757763604637L;
	/**
     * This is logger which print some messages to log file
     */
    private static Logger logger = Logger.getLogger(ServletController.class);

    /**
     * This method handles requests
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method handles requests
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method gets a instance of <code>Command</code> from <code>CommandFactory</code>
     * by request and execute this command. Then it gos to next jsp.
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Command command = CommandFactory.getCommand(request);
       response.setContentType("text/html;charset=UTF-8");
        try {
            command.processRequest(request, response);
            command.getMessages().saveMessages(request);
            request.getRequestDispatcher(command.getForward()).forward(request, response);
        } catch (IOException ex) {
            logger.error(ex);
            request.getRequestDispatcher("ERROR!!!").forward(request, response);
        } 
    }
}
