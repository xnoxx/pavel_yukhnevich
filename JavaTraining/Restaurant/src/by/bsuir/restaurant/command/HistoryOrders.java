package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.resource.Resource;

public class HistoryOrders extends Command {

	private List<by.bsuir.restaurant.model.beans.Order> ordersList;
	private OrderDAO orderDAO = new OrderDAO();
	public static final String MSG_NO_ORDER = "jsp.title.orders.no";
	public static final String FORWARD_HISTORY_ORDERS = "forward.historyorders";
	public static final String PARAM_ORDERS_LIST = "ordersList";

	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			ordersList = orderDAO.readByPayment();
			request.getSession().setAttribute(PARAM_ORDERS_LIST, ordersList);
			setForward(Resource.getStr(FORWARD_HISTORY_ORDERS));
			if (ordersList.isEmpty())
				getMessages().addMessage(Resource.getStr(MSG_NO_ORDER));
		} catch (DAOException e) {
			getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
            setForward(Resource.getStr(FORWARD_ERROR));
            logger.error(e);
		}
		
	}

}