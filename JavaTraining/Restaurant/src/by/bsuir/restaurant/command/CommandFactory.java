package by.bsuir.restaurant.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;


public class CommandFactory {
	
	public static final String PARAM_COMMAND = "commandName";
	private static Logger logger = Logger.getLogger(CommandFactory.class);
	
	public static Command getCommand(HttpServletRequest request) {
        Command command = null;
        CommandEnum commandType = getCommandEnum(request.getParameter(PARAM_COMMAND));
        
		switch (commandType) {
		case LOAD_MENU:
			command = new LoadMenu();
			break;
		case REGISTRATION:
			command = new Registration();
			break;
		case CHANGE_BODY:
			command = new ChangeBody();
			break;
		case TO_PAGE:
			command = new ToPage();
			break;
		case AUTHORIZATION:
			command = new Authorization();
			break;
		case EXIT:
			command = new Exit();
			break;
		case ORDER:
			command = new Order();
			break;
		case LOAD_ORDER:
			command = new LoadOrder();
			break;
		case DELETE_ORDER:
			command = new DeleteOrder();
			break;
		case ADD_DISH:
			command = new AddDish();
			break;
		case DELETE_DISH:
			command = new DeleteDish();
			break;
		case LOAD_ALL_ORDERS:
			command = new LoadAllOrders();
			break;
		case TAKE_AND_BILL:
			command = new TakeAndBill();
			break;
		case BILL:
			command = new Bill();
			break;
		case HISTORY_ORDERS:
			command = new HistoryOrders();
			break;
		case CLEAR_HISTORY:
			command = new ClearHistory();
			break;
		case CHANGE_LANGUAGE: 
			command = new ChangeLanguage();
			break;
		default:
			break;
		}
		
        return command;
	}
	
    private static CommandEnum getCommandEnum(String commandName) {
        CommandEnum commandEnum = null;
        try {
            commandEnum = CommandEnum.valueOf(commandName);
        } catch (IllegalArgumentException ex) {
            commandEnum = CommandEnum.UNKNOWN_COMMAND;
            logger.error(ex);
        } catch (NullPointerException ex) {
            commandEnum = CommandEnum.UNKNOWN_COMMAND;
            logger.error(ex);
        }
        return commandEnum;
    }
}
