package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.resource.Resource;

public class DeleteOrder extends Command {

	private OrderDAO orderDAO = new OrderDAO();
	private List<MenuPosition> listMenuPositions = new ArrayList<MenuPosition>();
	public static final String PARAM_ID = "id";
	public static final String PARAM_ORDER_ID = "orderId";
	
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String select[] = request.getParameterValues(PARAM_ID);

		int orderId = Integer.parseInt(request.getParameter(PARAM_ORDER_ID));
		
		if(select != null)
		for(int i = 0; i < select.length; i++){
			MenuPosition mp = new MenuPosition();
			mp.setId(Integer.parseInt(select[i]));
			listMenuPositions.add(mp);
		} else {
			getMessages().addMessage(Resource.getStr(MSG_EMPTY_CHOOSE));
		}
		
		try {
			orderDAO.deleteOrderPosition(listMenuPositions, orderId);
			setForward(Resource.getStr(FORWARD_MAIN));
		} catch (DAOException e) {
			getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
            setForward(Resource.getStr(FORWARD_ERROR));
            logger.error(e);
		}
	}

}
