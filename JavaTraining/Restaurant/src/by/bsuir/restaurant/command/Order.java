package by.bsuir.restaurant.command;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.ConnectionPoolException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.model.action.builders.OrderBuilder;
import by.bsuir.restaurant.resource.Resource;

public class Order extends Command {

	public static final String MESSAGE_ORDER_SUCCESS = "message.order.success";
	public static final String MESSAGE_ORDER_EMPTY = "message.order.empty";
	private by.bsuir.restaurant.model.beans.Order order;
	
	@Override
	public void processRequest(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
			
		OrderDAO orderDAO = new OrderDAO();		
		OrderBuilder orderBuilder = new OrderBuilder(getMessages());
		
		try {
			if (orderBuilder.build(request)) {
				order = orderBuilder.getOrder();
				orderDAO.create(order);
				getMessages().addMessage(Resource.getStr(MESSAGE_ORDER_SUCCESS));
			} else {
				getMessages().addMessage(Resource.getStr(MESSAGE_ORDER_EMPTY));
			}
			setForward(Resource.getStr(FORWARD_MAIN));
		} catch (NumberFormatException e) {
			setForward(Resource.getStr(FORWARD_ERROR));
			logger.error(e);
		} catch (ConnectionPoolException e) {
			getMessages().addMessage(Resource.getStr(MSG_CP_ERROR));
			setForward(Resource.getStr(FORWARD_ERROR));
			logger.error(e);
		} catch (SQLException e) {
			getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
			setForward(Resource.getStr(FORWARD_ERROR));
			logger.error(e);
		}	
		
	}

}
