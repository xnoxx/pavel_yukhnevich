package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.model.beans.Order;
import by.bsuir.restaurant.resource.Resource;

public class TakeAndBill extends Command {
	public static final String MESSAGE_ORDER_SUCCESS = "message.order.success";
	public static final String MESSAGE_EMPTY = "message.empty";
	public static final String PARAM_ID = "id";
	private by.bsuir.restaurant.model.beans.Order order;
	private OrderDAO orderDAO = new OrderDAO();
	private List<Order> ordersList = new ArrayList<Order>();

	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String select[] = request.getParameterValues(PARAM_ID);
		
		try {
			if (select.length != 0) {
				for (int i = 0; i < select.length; i++) {
					order = new Order();
					order.setId(Integer.parseInt(select[i]));
					ordersList.add(order);
				}
				orderDAO.updateIsOnKitchen(ordersList);
			} else {
				getMessages().addMessage(Resource.getStr(MESSAGE_EMPTY));
			}

			setForward(Resource.getStr(FORWARD_MAIN));
			} catch (DAOException e) {
				getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
				setForward(Resource.getStr(FORWARD_ERROR));
				logger.error(e);
			}
			getMessages().addMessage(Resource.getStr(MESSAGE_ORDER_SUCCESS));
			
		
	}
}
