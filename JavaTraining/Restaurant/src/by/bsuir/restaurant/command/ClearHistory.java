package by.bsuir.restaurant.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.resource.Resource;

public class ClearHistory extends Command {

	private OrderDAO orderDAO = new OrderDAO();
	
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			orderDAO.deleteByPayment();
			setForward(Resource.getStr(FORWARD_MAIN));
			getMessages().addMessage(Resource.getStr(MSG_SUCCESS));
		} catch (DAOException e) {
			getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
            setForward(Resource.getStr(FORWARD_ERROR));
            logger.error(e);
		}
	}

}
