package by.bsuir.restaurant.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.MenuPositionDAO;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.resource.Resource;

public class AddDish extends Command {

	private MenuPositionDAO menuPositionDAO;
	public static final String FORWARD_MENU = "forward.menu";
	public static final String PARAM_COST = "cost";
	public static final String PARAM_DISH = "dish";
	
	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		menuPositionDAO = new MenuPositionDAO();
		MenuPosition menuPosition = new MenuPosition();
		menuPosition.setCost(Integer.parseInt(request.getParameter(PARAM_COST)));
		menuPosition.setDish(request.getParameter(PARAM_DISH));
		setForward(Resource.getStr(FORWARD_MAIN));
		getMessages().addMessage(Resource.getStr(MSG_SUCCESS));

			try {
				menuPositionDAO.add(menuPosition);
			} catch (DAOException e) {
				getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
	            setForward(Resource.getStr(FORWARD_ERROR));
	            logger.error(e);
			}
	}

}
