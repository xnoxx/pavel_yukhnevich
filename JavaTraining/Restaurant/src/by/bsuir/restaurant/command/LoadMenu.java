package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.MenuPositionDAO;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.resource.Resource;

public class LoadMenu extends Command {

	private List<MenuPosition> menuPositionList;
	private MenuPositionDAO menuPositionDAO = new MenuPositionDAO();
	public static final String PARAM_MENU_POSITION_LIST = "menuPositionList";
	public static final String FORWARD_MENU = "forward.menu";

	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			menuPositionList = menuPositionDAO.getAll();
			request.getSession().setAttribute(PARAM_MENU_POSITION_LIST, menuPositionList);
			setForward(Resource.getStr(FORWARD_MENU));
		} catch (DAOException e) {
			getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
			setForward(Resource.getStr(FORWARD_ERROR));
			logger.error(e);
		}

	}

}
