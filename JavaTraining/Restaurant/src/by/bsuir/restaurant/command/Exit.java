package by.bsuir.restaurant.command;

import by.bsuir.restaurant.command.Command;
import by.bsuir.restaurant.resource.Resource;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * This class implements a pattern command
 * This class provides the user exits
 */
public class Exit extends Command {

    public static final String FORWARD_AUTHORIZATION = "forward.authorization";
    public static final String FORWARD_MAIN = "forward.main";
    
    /**
     * This method provides the user exits
     * This method invalidates user session and makes a transition to a authorization page
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().invalidate();
        setForward(Resource.getStr(FORWARD_MAIN));
    }
}
