package by.bsuir.restaurant.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.PersistException;
import by.bsuir.restaurant.database.dao.UserDAO;
import by.bsuir.restaurant.model.action.builders.UserBuilder;
import by.bsuir.restaurant.model.beans.User;
import by.bsuir.restaurant.resource.Resource;

public class Registration extends Command{
	
	private UserDAO userDAO = new UserDAO();	
	private User user;
	public static final String MSG_ERR_EXIST = "error.login.exist";
    public static final String MSG_USER_CREATED = "message.user.created";
    public static final String LOGGER_MSG_USER_CREATED = "logger.message.user.has.registred";
    public static final String FORWARD_AUTHORIZATION = "forward.authorization";
    public static final String FORWARD_REGISTRATION = "forward.registration";
    public static final String PARAM_USER = "user";
	
	@Override
	public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserBuilder userBuilder = new UserBuilder(getMessages());
	     
		try {
			if (userBuilder.build(request)) {
				user = userBuilder.getUser();
				
				if (!userDAO.isUserExist(user.getLogin())) {
					userDAO.create(user);
					getMessages().addMessage(Resource.getStr(MSG_USER_CREATED));
                    logger.info(Resource.getStr(LOGGER_MSG_USER_CREATED).replace("1", user.getLogin()));
                    setForward(Resource.getStr(FORWARD_MAIN));
				} else {
					 getMessages().addMessage(Resource.getStr(MSG_ERR_EXIST));
	                    setForward(Resource.getStr(FORWARD_REGISTRATION));
				}
				
			} else {
				request.setAttribute(PARAM_USER, userBuilder.getUser());
				setForward(Resource.getStr(FORWARD_REGISTRATION));
			}
			
			} catch (DAOException e) {
				getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
				setForward(Resource.getStr(FORWARD_ERROR));
				logger.error(e);
			} catch (PersistException e) {
				setForward(Resource.getStr(FORWARD_REGISTRATION));
				logger.error(e);
			}
		
	}

}
