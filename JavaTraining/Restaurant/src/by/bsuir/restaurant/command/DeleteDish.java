package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.MenuPositionDAO;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.resource.Resource;

public class DeleteDish extends Command {

	private MenuPositionDAO menuPositionDAO = new MenuPositionDAO();
	public static final String PARAM_ID = "id";

	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String select[] = request.getParameterValues(PARAM_ID);
		if (select != null) {
			List<MenuPosition> listOfDishsId = new ArrayList<MenuPosition>();
			for (int i = 0; i < select.length; i++) {
				MenuPosition mp = new MenuPosition();
				mp.setId(Integer.parseInt(select[i]));
				listOfDishsId.add(mp);
			}
			try {
				menuPositionDAO.delete(listOfDishsId);
				setForward(Resource.getStr(FORWARD_MAIN));
			} catch (DAOException e) {
				getMessages().addMessage(Resource.getStr(MSG_DATABASE_ERROR));
				setForward(Resource.getStr(FORWARD_ERROR));
				logger.error(e);
			}
			getMessages().addMessage(Resource.getStr(MSG_SUCCESS));
		} else {
			getMessages().addMessage(Resource.getStr(MSG_EMPTY_CHOOSE));
		}
	}
}
