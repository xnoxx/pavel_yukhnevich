package by.bsuir.restaurant.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.database.dao.OrderDAO;
import by.bsuir.restaurant.resource.Resource;

public class LoadAllOrders extends Command {

	private List<by.bsuir.restaurant.model.beans.Order> ordersList;
	private List<by.bsuir.restaurant.model.beans.Order> ordersOnKitchenList;
	private OrderDAO orderDAO = new OrderDAO();
	public static final String MSG_NO_ORDER = "message.order.no_orders";
	public static final String PARAM_ORDERS_LIST = "ordersList";
	public static final String PARAM_ORDERS_ON_KITCHEN_LIST = "ordersOnKitchenList";
	public static final String FORWARD_HISTORY_ORDERS = "forward.adminorders";

	@Override
	public void processRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		try {
			ordersList = orderDAO.readByOnKitchen(false);
			ordersOnKitchenList = orderDAO.readByOnKitchen(true);
			request.getSession().setAttribute(PARAM_ORDERS_LIST, ordersList);
			request.getSession().setAttribute(PARAM_ORDERS_ON_KITCHEN_LIST, ordersOnKitchenList);
			setForward(Resource.getStr(FORWARD_HISTORY_ORDERS));
			if (ordersList.isEmpty())
				getMessages().addMessage(Resource.getStr(MSG_NO_ORDER));
		} catch (DAOException e) {
			getMessages().addMessage(Resource.getStr(MSG_CP_ERROR));
			setForward(Resource.getStr(FORWARD_ERROR));
			logger.error(e);
		}

	}

}
