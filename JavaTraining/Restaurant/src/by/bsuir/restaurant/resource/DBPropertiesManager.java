package by.bsuir.restaurant.resource;

import java.util.ResourceBundle;

import by.bsuir.restaurant.other.LocationConstants;

public class DBPropertiesManager {
	private final static DBPropertiesManager instance = new DBPropertiesManager();

	private ResourceBundle bundle = ResourceBundle.getBundle(LocationConstants.DB_RESOURCE_LOCATION);

	public static DBPropertiesManager getInstance() {
		return instance;
	}

	public String getValue(String key) {
		return bundle.getString(key).trim();
	}

}