package by.bsuir.restaurant.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import by.bsuir.restaurant.resource.DBPropertiesManager;

/**
 * This class initializes connection with database
 */

public class ConnectionPool {
	private static ConnectionPool instance;
	private BlockingQueue<Connection> freeConnections;
	private BlockingQueue<Connection> busyConnections;
	DBPropertiesManager dbPropertiesManager;

	public static synchronized ConnectionPool getInstance()
			throws ConnectionPoolException {
		if (instance == null) {
			instance = new ConnectionPool();
		}
		return instance;
	}

	private ConnectionPool() throws ConnectionPoolException {
		dbPropertiesManager = DBPropertiesManager.getInstance();
		int size = Integer.parseInt(dbPropertiesManager.getValue(DBParameter.DB_POLL_SIZE));
		freeConnections = new ArrayBlockingQueue<>(size);
		busyConnections = new ArrayBlockingQueue<>(size);

		try {
			Class.forName(dbPropertiesManager.getValue(DBParameter.DB_DRIVER));

			for (int i = 0; i < size; i++) {
				Connection connection = DriverManager.getConnection(
						dbPropertiesManager.getValue(DBParameter.DB_URL),
						dbPropertiesManager.getValue(DBParameter.DB_USER),
						dbPropertiesManager.getValue(DBParameter.DB_PASSWORD));
				freeConnections.add(connection);
			}
		} catch (ClassNotFoundException e) {
			throw new ConnectionPoolException(ExceptionConstants.DB_DRIVER_NOT_FOUND, e);
		} catch (SQLException e) {
			throw new ConnectionPoolException(ExceptionConstants.CP_INIT_FAIL, e);
		}
	}

	public Connection getFreeConnection() throws ConnectionPoolException {
		Connection connection = null;
		try {
			connection = freeConnections.take();
			busyConnections.add(connection);
		} catch (InterruptedException e) {
			throw new ConnectionPoolException(ExceptionConstants.TAKING_INTERRUPTED, e);
		}
		return connection;
	}

	public void freeConnection(Connection connection)
			throws ConnectionPoolException {
		if (busyConnections.contains(connection)) {
			try {
				freeConnections.put(connection);
				busyConnections.remove(connection);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		} else {
			throw new ConnectionPoolException(ExceptionConstants.NONPOOLED_CONNECTION);
		}
	}

	public void closeAll() throws ConnectionPoolException {
		try {
			for (Connection freeConnection : freeConnections) {
				freeConnection.close();
			}
			for (Connection busyConnection : busyConnections) {
				busyConnection.close();
			}
		} catch (SQLException e) {
			throw new ConnectionPoolException(ExceptionConstants.CP_CLOSE_FAIL, e);
		}
	}

}
