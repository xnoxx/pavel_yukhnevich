package by.bsuir.restaurant.database.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import by.bsuir.restaurant.database.ConnectionPool;
import by.bsuir.restaurant.database.ConnectionPoolException;
import by.bsuir.restaurant.database.DAOException;

/**
 * This abstract class holds objects needed to work with database Also contains
 * a method of closing the connection
 */
public abstract class AbstractDAO {

	public static final String CHARSET = "UTF-8";
	/**
	 * This object stores data obtained from the database
	 */
	protected ResultSet resultSet;
	/**
	 * This is logger which print some messages to log file
	 */
	protected Logger logger = Logger.getLogger(UserDAO.class);
	/**
	 * This object creates connection with database
	 */
	protected Connection connection;
	protected ConnectionPool connectionPool;

	/**
	 * This method tries to close <code>resultSet</code> but afterwards and
	 * connection
	 * 
	 * @throws ConnectionPoolException
	 */

	public Connection getConnection() throws DAOException {
		Connection connection = null;
		try {
		connectionPool = ConnectionPool.getInstance();
		connection = connectionPool.getFreeConnection();
		} catch (ConnectionPoolException e) {
			throw new DAOException("Problems with DAO connection", e);
		}
		return connection;
	}

	public void close() throws DAOException {
		try {
			connectionPool.freeConnection(connection);

			if (resultSet != null)
				resultSet.close();
		} catch (ConnectionPoolException | SQLException e) {
			throw new DAOException("Problems with DAO connection", e);
		}
	}
}
