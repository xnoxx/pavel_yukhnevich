package by.bsuir.restaurant.database.dao;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.model.beans.MenuPosition;

public class MenuPositionDAO extends AbstractDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2223065807125995861L;
	private String sqlReadByKey = "SELECT * FROM restaurant.menu WHERE id = ?";
	private String sqlAddMenuPosition = "INSERT INTO menu (dish, cost) VALUES (?, ?)";
	private String sqlDeleteMenuPositionById = "DELETE FROM menu WHERE id = ?";
	private String sqlGetAllMenuPositions = "SELECT * FROM restaurant.menu";
	
	public MenuPosition create() {
		return null;
	}

	public MenuPosition read(int key) throws DAOException {	
		MenuPosition mp = new MenuPosition();
		try {
			this.connection = getConnection();
			PreparedStatement stm = connection.prepareStatement(sqlReadByKey);
			stm.setInt(1, key);
			ResultSet rs = stm.executeQuery();
			rs.next();
			mp.setId(rs.getInt("id"));
			mp.setDish(rs.getString("dish"));
			mp.setCost(rs.getInt("cost"));
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
		return mp;
	}

	public void add(MenuPosition menuPosition) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement stm = connection.prepareStatement(sqlAddMenuPosition);
			stm.setString(1, menuPosition.getDish());
			stm.setInt(2, menuPosition.getCost());
			stm.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	public void delete(List<MenuPosition> menuPositionList) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement stm = connection.prepareStatement(sqlDeleteMenuPositionById);
			for(MenuPosition menuPosition: menuPositionList){
			stm.setInt(1, menuPosition.getId());
			stm.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	public List<MenuPosition> getAll() throws DAOException {
		List<MenuPosition> list = new ArrayList<MenuPosition>();
		try {
			this.connection = getConnection();
			PreparedStatement stm = connection.prepareStatement(sqlGetAllMenuPositions);
			ResultSet rs = stm.executeQuery();
			while (rs.next()) {
				MenuPosition mp = new MenuPosition();
				mp.setId(rs.getInt("id"));
				mp.setDish(rs.getString("dish"));
				mp.setCost(rs.getInt("cost"));
				list.add(mp);
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}

		return list;
	}

}
