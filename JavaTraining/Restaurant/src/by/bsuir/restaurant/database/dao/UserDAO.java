package by.bsuir.restaurant.database.dao;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.exceptions.InvalidArgumentException;
import by.bsuir.restaurant.model.beans.User;
import by.bsuir.restaurant.model.beans.UserType;

public class UserDAO extends AbstractDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6693755945985188873L;
    private String sqlCreateUser = "INSERT INTO users (login, pass, user_type) VALUES (?, ?, ?)";
    private String sqlCheckIsUserExist = "SELECT login FROM users WHERE login = ?";
    private String sqlReadUserByLoginAndPass = "Select id, login, pass, user_type from users where login=? and pass=?";
	
	public void create(User user) throws PersistException, DAOException{
		
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sqlCreateUser);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setString(3, user.getUserType().toString());

			int count = statement.executeUpdate();
			if (count != 1) {
				throw new PersistException("On persist modify more then 1 record: " + count);
			}
		} catch (Exception e) {
			throw new PersistException(e);
		} finally {
			close();
		}
	}

	public boolean isUserExist(String login) throws DAOException {
		boolean b = false;
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sqlCheckIsUserExist);
			statement.setString(1, login);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				b = true;
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
		return b;
	}

	public User getUserByLoginPassword(String login, String password) throws  UnsupportedEncodingException, DAOException, InvalidArgumentException {
		 User user = null;
	        try {
	        	this.connection = getConnection();
	        	PreparedStatement statement = connection.prepareStatement(sqlReadUserByLoginAndPass);
	            statement.setBytes(1, login.getBytes(CHARSET));
	            statement.setBytes(2, password.getBytes(CHARSET));
	            resultSet = statement.executeQuery();
	            if (resultSet.next()) {
	                user = buildUser();
	            }
	        } catch (SQLException e) {
	        	throw new DAOException("Problems with DAO request", e);
			} finally {
	            close();
	        }
	        return user;
	}
	
	 /**
     * This method builds a user
     * This method gets data from <code>resultSet</code> and
     * sets this data to a new user
     * @return a user
     * @throws SQLException a SQLException
	 * @throws InvalidArgumentException 
     */
    private User buildUser() throws DAOException, InvalidArgumentException {
        User user = new User();
        try {
        user.setId(resultSet.getInt(1));
        user.setLogin(resultSet.getString(2));
        user.setUserType(UserType.getUserType(resultSet.getString(4)));
        UserType userType = user.getUserType();
        
        if(userType.getTypeValue().equals("admin"))
			user.setAuthority(2);
		
        if(userType.getTypeValue().equals("client"))
			user.setAuthority(1);
        
		user.setPassword(resultSet.getString(3));
        } catch(SQLException e) {
        	throw new DAOException("Problems with DAO request", e);
        } 
        return user;
    }
    
}
