package by.bsuir.restaurant.database.dao;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.bsuir.restaurant.database.DAOException;
import by.bsuir.restaurant.model.beans.MenuPosition;
import by.bsuir.restaurant.model.beans.Order;

public class OrderDAO extends AbstractDAO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4122233424089314902L;
	private String sqlOrders = "INSERT INTO orders (id_client) VALUES (?)";
	private String sqlOrdersContent = "INSERT INTO orders_content (id_order, id_dish) VALUES (?, ?)";
	private String sqlIdOrder = "SELECT id FROM orders WHERE id_client = ? GROUP BY id DESC LIMIT 1";
	private String sqlGetOrdersByid = "select * from menu inner join orders_content on menu.id = orders_content.id_dish inner join orders on orders.id = orders_content.id_order  where orders.id_client = ? and orders.payment = 0";
	private String sqlDeletePositionOrdersContent = "delete from orders_content where id_dish = ? and id_order = ? limit 1";
	private String sqlDeletePositionOrders = "delete from orders where id = ?";
	private String sqlCheckOrders = "select * from orders_content where id_order = ?";
	private String sqlGetOrdersByOnKitchen = "select * from menu inner join orders_content on menu.id = orders_content.id_dish inner join orders on orders.id = orders_content.id_order where orders.on_kitchen = ? and orders.payment = 0";
	private String sqlUpdateIsOnKitchen = "UPDATE orders SET on_kitchen = 1 WHERE id = ?";
	private String sqlUpdateIsPayment = "UPDATE orders SET payment = 1 WHERE id = ?";
	private String sqlGetOrdersByPayment = "select * from menu inner join orders_content on menu.id = orders_content.id_dish inner join orders on orders.id = orders_content.id_order where orders.payment = 1";
	private String sqlDeleteOrdersByPayment = "DELETE orders, orders_content FROM orders LEFT JOIN orders_content ON orders_content.id_order = orders.id WHERE orders.payment = 1";

	public void deleteByPayment() throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlDeleteOrdersByPayment);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	public List<Order> readByPayment() throws DAOException {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlGetOrdersByPayment);
			ResultSet rs = statement.executeQuery();
			BuilOrdersList(rs, ordersList);
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
		return ordersList;
	}

	public void create(Order order) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlOrders);
			statement.setInt(1, order.getClientId());
			statement.executeUpdate();

			statement = connection.prepareStatement(sqlIdOrder);
			statement.setInt(1, order.getClientId());
			ResultSet rs = statement.executeQuery();

			Integer idOrder = 0;
			if (rs.next())
				idOrder = rs.getInt(1);
			for (MenuPosition menuPosition : order.getDishsList()) {
				statement = connection.prepareStatement(sqlOrdersContent);
				statement.setInt(1, idOrder);
				statement.setInt(2, menuPosition.getId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}

	}

	public List<Order> readByOnKitchen(boolean onKitchen) throws DAOException {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlGetOrdersByOnKitchen);
			statement.setBoolean(1, onKitchen);
			ResultSet rs = statement.executeQuery();
			BuilOrdersList(rs, ordersList);
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
		return ordersList;
	}

	public List<Order> readByIdClient(int idClient) throws DAOException {
		List<Order> ordersList = new ArrayList<Order>();
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlGetOrdersByid);
			statement.setInt(1, idClient);
			ResultSet rs = statement.executeQuery();
			BuilOrdersList(rs, ordersList);
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
		return ordersList;
	}

	public void deleteOrderPosition(List<MenuPosition> listMenuPositions,
			int orderId) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement statement = connection
					.prepareStatement(sqlDeletePositionOrdersContent);
			for (MenuPosition mp : listMenuPositions) {
				statement.setInt(1, mp.getId());
				statement.setInt(2, orderId);
				statement.executeUpdate();
			}

			statement = connection.prepareStatement(sqlCheckOrders);
			statement.setInt(1, orderId);
			ResultSet rs = statement.executeQuery();
			if (!rs.next()) {
				statement = connection
						.prepareStatement(sqlDeletePositionOrders);
				statement.setInt(1, orderId);
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	public void updateIsOnKitchen(List<Order> ordersList) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement statement;
			for (Order order : ordersList) {
				statement = connection.prepareStatement(sqlUpdateIsOnKitchen);
				statement.setInt(1, order.getId());
				statement.executeUpdate();
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	public void updateIsPayment(Order order) throws DAOException {
		try {
			this.connection = getConnection();
			PreparedStatement statement;

			statement = connection.prepareStatement(sqlUpdateIsPayment);
			statement.setInt(1, order.getId());
			statement.executeUpdate();

		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		} finally {
			close();
		}
	}

	private List<Order> BuilOrdersList(ResultSet rs, List<Order> ordersList)
			throws DAOException {
		Order order = new Order();
		int id = 0;
		try {
			while (rs.next()) {

				if (id != rs.getInt("id_order")) {
					id = rs.getInt("id_order");
					order = new Order();
					order.setId(id);
					order.setClientId(rs.getInt("id_client"));
					order.setOnKitchen(rs.getBoolean("on_kitchen"));
					order.setPayment(rs.getBoolean("payment"));
					ordersList.add(order);
				}

				MenuPosition mp = new MenuPosition();
				mp.setId(rs.getInt("id_dish"));
				mp.setDish(rs.getString("dish"));
				mp.setCost(rs.getInt("cost"));
				order.getDishsList().add(mp);
			}
		} catch (SQLException e) {
			throw new DAOException("Problems with DAO request", e);
		}
		return ordersList;
	}
}
