package by.bsuir.restaurant.database;

import java.sql.SQLException;

public class DAOException extends SQLException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4678853208059250872L;

	public DAOException() {
	}

	public DAOException(String arg0) {
		super(arg0);
	}

	public DAOException(Throwable arg0) {
		super(arg0);
	}

	public DAOException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
