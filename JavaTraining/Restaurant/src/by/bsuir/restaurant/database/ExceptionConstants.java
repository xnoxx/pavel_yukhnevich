package by.bsuir.restaurant.database;

public class ExceptionConstants {

	public static final String DB_DRIVER_NOT_FOUND = "Data base driver not found!";
	public static final String CP_INIT_FAIL = "Connection pool initialisation fail";
	public static final String TAKING_INTERRUPTED = "Taking interrupted";
	public static final String NONPOOLED_CONNECTION = "Nonpooled connection";
	public static final String CP_CLOSE_FAIL = "Connection pool close fail";

}
