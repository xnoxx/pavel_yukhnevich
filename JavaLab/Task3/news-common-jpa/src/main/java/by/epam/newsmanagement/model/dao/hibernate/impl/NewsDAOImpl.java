package by.epam.newsmanagement.model.dao.hibernate.impl;

import by.epam.newsmanagement.model.dao.NewsDAO;
import by.epam.newsmanagement.model.dao.hibernate.impl.util.SizeOrder;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.*;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static by.epam.newsmanagement.model.dao.hibernate.impl.NewsDetachedCriteriaUtil.*;

@Transactional
public class NewsDAOImpl extends BaseDAO<News> implements NewsDAO {

    NewsDAOImpl(Class<News> type) {
        this.type = type;
    }

    @SuppressWarnings("unchecked")
    public List<News> getAll(SearchCriteria sc, int startFrom, int maxSize) {

        DetachedCriteria dc = getDetachedCriteria(sc);

        return getSession().createCriteria(News.class)
                .add(Subqueries.propertyIn("id", dc))
                .addOrder(SizeOrder.desc("commentsList"))
                .addOrder(Order.desc("modificationDate"))
                .setFirstResult(startFrom)
                .setMaxResults(maxSize)
                .list();
    }

    @SuppressWarnings("unchecked")
    public List<Long> getIds(SearchCriteria sc) {
        DetachedCriteria dc = getDetachedCriteria(sc);

        return getSession().createCriteria(News.class)
                .add(Subqueries.propertyIn("id", dc))
                .addOrder(SizeOrder.desc("commentsList"))
                .addOrder(Order.desc("modificationDate"))
                .setProjection(Projections.id())
                .list();
    }

    public Integer getCount() {
        return Integer.parseInt(getSession().createQuery("SELECT COUNT(*) FROM News").uniqueResult().toString());
    }

    public Integer getCount(SearchCriteria sc) {

        DetachedCriteria dc = getDetachedCriteria(sc);

        return Integer.parseInt(getSession().createCriteria(News.class)
                .add(Subqueries.propertyIn("id", dc))
                .setProjection(Projections.rowCount())
                .uniqueResult().toString());
    }

    public void initializeFields(News news) {
        Hibernate.initialize(news);
    }

}
