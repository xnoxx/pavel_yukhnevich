package by.epam.newsmanagement.model.dao.eclipselink.impl;

import by.epam.newsmanagement.model.dao.NewsDAO;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

import static by.epam.newsmanagement.model.dao.eclipselink.impl.NewsCriteriaBuilder.*;

@Transactional
public class NewsDAOImpl extends BaseDAO<News> implements NewsDAO {

    NewsDAOImpl(Class<News> type) {
        this.type = type;
    }

    public List<News> getAll(SearchCriteria sc, int startWith, int maxSize) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> cq = getNewsCriteriaQuery(cb, sc);

        return entityManager.createQuery(cq)
                .setFirstResult(startWith)
                .setMaxResults(maxSize)
                .getResultList();
    }

    public Integer getCount() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        cq.select(cb.count(cq.from(News.class)));

        return getIntResult(entityManager.createQuery(cq).getSingleResult());
    }

    public Integer getCount(SearchCriteria sc) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        return getIntResult(entityManager.createQuery(getCountCriteriaQuery(cb, sc)).getSingleResult());
    }

    public List<Long> getIds(SearchCriteria sc){
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = getNewsIdCriteriaQuery(cb, sc);

        return entityManager.createQuery(cq).getResultList();
    }

    public void initializeFields(News news) {
        news.getCommentsList().size();
        news.getTagsSet().size();
    }

    private Integer getIntResult(Long longResult) {
        Integer intResult = null;

        if (longResult != null) {
            intResult = Integer.parseInt(longResult.toString());
        }

        return intResult;
    }

}