package by.epam.newsmanagement.model.services.impl;

import by.epam.newsmanagement.model.dao.CommentDAO;
import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.model.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDAO commentDAO;

    public Long create(Comment comment) {
        return commentDAO.create(comment);
    }

    public Comment read(Long id) {
        return commentDAO.read(id);
    }

    public void update(Comment comment) {
        commentDAO.update(comment);
    }

    public void delete(Long id) {
        Comment comment = new Comment();
        comment.setId(id);
        commentDAO.delete(comment);
    }

    public List<Comment> getAll() {
        return commentDAO.getAll();
    }
}
