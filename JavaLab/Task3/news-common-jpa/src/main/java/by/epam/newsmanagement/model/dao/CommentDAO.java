package by.epam.newsmanagement.model.dao;

import by.epam.newsmanagement.model.entities.Comment;

public interface CommentDAO extends GenericCRUDOperationsDAO<Comment> {

}
