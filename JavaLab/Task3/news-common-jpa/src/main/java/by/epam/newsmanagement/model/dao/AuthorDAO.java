package by.epam.newsmanagement.model.dao;


import by.epam.newsmanagement.model.entities.Author;

import java.util.List;

public interface AuthorDAO extends GenericCRUDOperationsDAO<Author> {

    List<Author> getAllExceptExpired();

}
