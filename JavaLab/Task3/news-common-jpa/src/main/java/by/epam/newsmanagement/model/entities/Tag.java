package by.epam.newsmanagement.model.entities;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tags")
@SequenceGenerator(name = "TAGS_SEQ", sequenceName = "TAGS_SEQ")
public class Tag extends BaseEntity implements Serializable {

    @Id
    @Column(name = "tag_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAGS_SEQ")
    private Long id;

    @NotBlank(message = "tag.blank")
    @Length(min = 1, max = 30, message = "tag.size")
    @Column(name = "tag_name")
    private String name;

    public Tag() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
