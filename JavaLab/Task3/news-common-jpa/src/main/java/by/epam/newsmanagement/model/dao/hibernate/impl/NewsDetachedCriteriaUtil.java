package by.epam.newsmanagement.model.dao.hibernate.impl;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

class NewsDetachedCriteriaUtil {

    public static DetachedCriteria getDetachedCriteria(SearchCriteria sc) {

        DetachedCriteria dc = DetachedCriteria.forClass(News.class);

        if (sc != null) {

            if (sc.getAuthorId() != null) {
                dc.add(Restrictions.like("author.id", sc.getAuthorId()));
            }

            if (sc.getTagIdsSet() != null && !sc.getTagIdsSet().isEmpty()) {
                dc.createCriteria("tagsSet").add(Restrictions.in("id", sc.getTagIdsSet()));
            }
        }

        dc.setProjection(Projections.distinct(Projections.id()));

        return dc;
    }
}
