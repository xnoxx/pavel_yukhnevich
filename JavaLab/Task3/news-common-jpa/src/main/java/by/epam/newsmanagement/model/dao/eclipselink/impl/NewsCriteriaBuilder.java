package by.epam.newsmanagement.model.dao.eclipselink.impl;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.In;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collection;


class NewsCriteriaBuilder<T> {

    private NewsCriteriaBuilder() {
    }

    public static CriteriaQuery<Long> getNewsIdCriteriaQuery(CriteriaBuilder cb, SearchCriteria sc){
        NewsCriteriaBuilder<Long> newsCB = new NewsCriteriaBuilder<Long>();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<News> news = cq.from(News.class);

        cq = cq.select(news.<Long>get("id")).distinct(true);
        cq = newsCB.buildCriteria(cb, cq, news, sc);
        cq = newsCB.addSorting(cb, cq, news);

        return cq;
    }

    public static CriteriaQuery<News> getNewsCriteriaQuery(CriteriaBuilder cb, SearchCriteria sc) {

        NewsCriteriaBuilder<News> newsCB = new NewsCriteriaBuilder<News>();

        CriteriaQuery<News> cq = cb.createQuery(News.class);
        Root<News> news = cq.from(News.class);

        cq = cq.select(news).distinct(true);
        cq = newsCB.buildCriteria(cb, cq, news, sc);
        cq = newsCB.addSorting(cb, cq, news);

        return cq;
    }

    public static CriteriaQuery<Long> getCountCriteriaQuery(CriteriaBuilder cb, SearchCriteria sc) {

        NewsCriteriaBuilder<Long> newsCB = new NewsCriteriaBuilder<Long>();

        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<News> news = cq.from(News.class);

        cq.select(cb.countDistinct(news));
        cq = newsCB.buildCriteria(cb, cq, news, sc);


        return cq;
    }

    private CriteriaQuery<T> buildCriteria(CriteriaBuilder cb, CriteriaQuery<T> cq, Root<News> news, SearchCriteria sc) {

        if (sc != null) {
            boolean authorExist = sc.getAuthorId() != null;
            boolean tagsExist = sc.getTagIdsSet() != null && !sc.getTagIdsSet().isEmpty();

            if (authorExist && !tagsExist) {
                cq.where(cb.in(news.get("author").get("id")).value(sc.getAuthorId()));
            } else if (!authorExist && tagsExist) {
                cq.where(cb.or(addTags(cb, news, sc)));
            } else if (authorExist & tagsExist) {
                cq.where(
                        cb.and(
                                cb.equal(news.get("author").get("id"), sc.getAuthorId()),
                                cb.or(addTags(cb, news, sc))
                        ));
            }
        }

        return cq;
    }

    private Predicate addTags(CriteriaBuilder cb, Root<News> news, SearchCriteria sc) {
        In<Object> in = cb.in(news.get("tagsSet").get("id"));

        for (Long id : sc.getTagIdsSet()) {
            in.value(id);
        }

        return in;
    }

    private CriteriaQuery<T> addSorting(CriteriaBuilder cb, CriteriaQuery<T> cq, Root<News> news) {
        return cq.orderBy(cb.desc(cb.size(news.<Collection>get("commentsList"))),
                cb.desc(news.get("modificationDate")));
    }
}
