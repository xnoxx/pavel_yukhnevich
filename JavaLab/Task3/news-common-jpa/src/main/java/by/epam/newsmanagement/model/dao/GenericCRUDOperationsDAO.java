package by.epam.newsmanagement.model.dao;

import java.util.List;

public interface GenericCRUDOperationsDAO<T> {

    Long create(T entity);

    T read(Long id);

    void update(T entity);

    void delete(T entity);

    List<T> getAll();

}
