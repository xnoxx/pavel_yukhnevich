package by.epam.newsmanagement.model.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SearchCriteria {

    private Long authorId;
    private Set<Long> tagIdsSet;

    public SearchCriteria() {
    }

    public SearchCriteria(Long authorId, Set<Long> tagIdsSet) {
        this.authorId = authorId;
        this.tagIdsSet = tagIdsSet;
    }

    public Set<Long> getTagIdsSet() {
        return tagIdsSet;
    }

    public void setTagIdsSet(Set<Long> tagIdsSet) {
        this.tagIdsSet = tagIdsSet;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public void setTagIdFromArray(Long[] tagIds) {
        if (tagIds != null && tagIds.length != 0) {
            tagIdsSet = new HashSet<>();
            Collections.addAll(tagIdsSet, tagIds);
        }
    }
}
