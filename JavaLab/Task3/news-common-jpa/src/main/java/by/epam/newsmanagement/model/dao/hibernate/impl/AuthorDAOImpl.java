package by.epam.newsmanagement.model.dao.hibernate.impl;

import by.epam.newsmanagement.model.dao.AuthorDAO;
import by.epam.newsmanagement.model.entities.Author;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
public class AuthorDAOImpl extends BaseDAO<Author> implements AuthorDAO {

    AuthorDAOImpl(Class<Author> type) {
        this.type = type;
    }

    @SuppressWarnings("unchecked")
    public List<Author> getAllExceptExpired() {
        return getSession().createCriteria(Author.class)
                .add(Restrictions.disjunction()
                        .add(Restrictions.isNull("expiredDate"))
                        .add(Restrictions.gt("expiredDate", new Date())))
                .list();
    }
}
