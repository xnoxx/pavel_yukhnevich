package by.epam.newsmanagement.model.services.impl;

import by.epam.newsmanagement.model.dao.NewsDAO;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import by.epam.newsmanagement.exceptions.ResourceNotFoundException;
import by.epam.newsmanagement.model.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;

    public Integer getCount() {
        return newsDAO.getCount();
    }

    public Integer getCount(SearchCriteria sc) {
        return newsDAO.getCount(sc);
    }

    public List<News> getAll(SearchCriteria searchCriteria, int currentPage, int maxOnPage) {
        int start = (currentPage - 1) * maxOnPage;
        List<News> newsList = newsDAO.getAll(searchCriteria, start, maxOnPage);
        initializeFields(newsList);
        return newsList;
    }

    public List<Long> getIds(SearchCriteria sc) {
        return newsDAO.getIds(sc);
    }

    public Integer getPagesCount(Integer newsPerPage) {
        Integer intCount = getCount() / newsPerPage;
        return (getCount() % newsPerPage == 0) ? intCount : intCount + 1;
    }

    public Integer getPagesCount(Integer newsPerPage, SearchCriteria sc) {

        Integer intCount = (sc != null) ? getCount(sc) / newsPerPage : getCount() / newsPerPage;
        return (getCount() % newsPerPage == 0) ? intCount : intCount + 1;
    }

    public Long create(News news) {
        Date nowDate = new Date();
        if (news.getCreationDate() == null) {
            news.setCreationDate(nowDate);
        }
        return newsDAO.create(news);
    }

    public News read(Long id) {
        News news = newsDAO.read(id);
        if (news != null) {
            initializeFields(news);
            return news;
        } else {
            throw new ResourceNotFoundException("News with id: " + id + " not found");
        }
    }

    public void update(News news) {
        newsDAO.update(news);
    }

    public void delete(Long id) {
        News news = new News();
        news.setId(id);
        newsDAO.delete(news);
    }

    public List<News> getAll() {
        return newsDAO.getAll();
    }

    private void initializeFields(List<News> newsList) {
        if (newsList != null) {
            for (News news : newsList) {
                initializeFields(news);
            }
        }
    }

    private void initializeFields(News news) {
        if (news != null && news.getId() != null) {
            news.getTagsSet().size();
            news.getCommentsList().size();
        }
    }
}
