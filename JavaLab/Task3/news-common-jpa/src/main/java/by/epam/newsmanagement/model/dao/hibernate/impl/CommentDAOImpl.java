package by.epam.newsmanagement.model.dao.hibernate.impl;

import by.epam.newsmanagement.model.dao.CommentDAO;
import by.epam.newsmanagement.model.entities.Comment;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class CommentDAOImpl extends BaseDAO<Comment> implements CommentDAO {

    CommentDAOImpl(Class<Comment> type) {
        this.type = type;
    }
}
