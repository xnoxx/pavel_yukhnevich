package by.epam.newsmanagement.model.entities;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExceptionMessage implements Serializable {

    private static final long serialVersionUID = -12321L;

    private String url;
    private String message;
    private String errorDetails;

    public ExceptionMessage() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    @Override
    public String toString() {
        return "ExceptionMessage{" +
                "url:'" + url + '\'' +
                ", message:'" + message + '\'' +
                ", errorDetails:'" + errorDetails + '\'' +
                '}';
    }
}
