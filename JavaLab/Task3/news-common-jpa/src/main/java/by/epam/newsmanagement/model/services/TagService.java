package by.epam.newsmanagement.model.services;

import by.epam.newsmanagement.model.entities.Tag;


public interface TagService extends GenericService<Tag> {

}
