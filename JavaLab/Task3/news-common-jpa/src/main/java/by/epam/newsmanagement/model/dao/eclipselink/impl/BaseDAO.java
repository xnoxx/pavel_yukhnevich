package by.epam.newsmanagement.model.dao.eclipselink.impl;


import by.epam.newsmanagement.model.dao.GenericCRUDOperationsDAO;
import by.epam.newsmanagement.model.entities.BaseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
public abstract class BaseDAO<T extends BaseEntity> implements GenericCRUDOperationsDAO<T> {

    @PersistenceContext
    protected EntityManager entityManager;

    protected Class<T> type;

    public Long create(T entity) {
        entityManager.persist(entity);
        return entity.getId();
    }

    public T read(Long id) {
        T entity = entityManager.find(type, id);
        entityManager.refresh(entity);
        return entity;
    }

    public void update(T entity) {
        entityManager.merge(entity);
    }

    public void delete(T entity) {
        entity = entityManager.find(type, entity.getId());
        entityManager.remove(entity);
    }

    public List<T> getAll() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(type);
        Root<T> entity = cq.from(type);
        cq.select(entity);
        return entityManager.createQuery(cq).getResultList();
    }
}
