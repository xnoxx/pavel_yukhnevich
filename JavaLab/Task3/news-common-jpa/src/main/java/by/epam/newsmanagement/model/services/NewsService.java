package by.epam.newsmanagement.model.services;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;

import java.util.List;

public interface NewsService extends GenericService<News> {

    Integer getCount();

    Integer getCount(SearchCriteria sc);

    List<News> getAll(SearchCriteria searchCriteria, int currentPage, int maxOnPage);

    List<Long> getIds(SearchCriteria sc);

    Integer getPagesCount(Integer newsPerPage);

    Integer getPagesCount(Integer newsPerPage, SearchCriteria sc);
}
