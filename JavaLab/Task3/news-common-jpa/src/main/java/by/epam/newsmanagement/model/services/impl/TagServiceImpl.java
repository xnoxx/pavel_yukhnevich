package by.epam.newsmanagement.model.services.impl;

import by.epam.newsmanagement.model.dao.TagDAO;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.exceptions.ResourceNotFoundException;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class TagServiceImpl implements TagService {

    @Autowired
    private TagDAO tagDAO;

    public List<Tag> getAll() {
        return tagDAO.getAll();
    }

    public Long create(Tag tag) {
        return tagDAO.create(tag);
    }

    public Tag read(Long id) {
        Tag tag = tagDAO.read(id);
        if (tag != null) {
            return tag;
        } else {
            throw new ResourceNotFoundException("Tag with id: " + id + " not found");
        }
    }

    public void update(Tag tag) {
        tagDAO.update(tag);
    }

    public void delete(Long id) {
        Tag tag = new Tag();
        tag.setId(id);
        tagDAO.delete(tag);
    }
}
