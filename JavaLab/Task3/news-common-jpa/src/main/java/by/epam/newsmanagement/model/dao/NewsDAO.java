package by.epam.newsmanagement.model.dao;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;

import java.util.List;

public interface NewsDAO extends GenericCRUDOperationsDAO<News> {

    List<News> getAll(SearchCriteria sc, int startWith, int maxSize);

    List<Long> getIds(SearchCriteria sc);

    Integer getCount();

    Integer getCount(SearchCriteria sc);

    void initializeFields(News news);

}
