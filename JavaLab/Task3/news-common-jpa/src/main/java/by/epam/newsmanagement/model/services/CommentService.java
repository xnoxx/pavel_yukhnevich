package by.epam.newsmanagement.model.services;

import by.epam.newsmanagement.model.entities.Comment;


public interface CommentService extends GenericService<Comment> {

}
