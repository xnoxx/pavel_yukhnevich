package by.epam.newsmanagement.model.entities;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "news")
@SequenceGenerator(name = "NEWS_SEQ", sequenceName = "NEWS_SEQ", initialValue = 950)
public class News extends BaseEntity implements Serializable {

    @Id
    @Column(name = "news_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_SEQ")
    private Long id;

    @NotBlank(message = "title.blank")
    @Length(min = 1, max = 30, message = "title.size")
    @Column(name = "title")
    private String title;

    @NotBlank(message = "shortText.blank")
    @Length(min = 1, max = 100, message = "shortText.size")
    @Column(name = "short_text")
    private String shortText;

    @NotBlank(message = "fullText.blank")
    @Length(min = 1, max = 2000, message = "fullText.size")
    @Column(name = "full_text")
    private String fullText;

    @Column(name = "creation_date")
    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Column(name = "modification_date")
    @Temporal(TemporalType.DATE)
    private Date modificationDate;

    @OneToMany(mappedBy = "news", fetch = FetchType.LAZY)
    @OrderBy("creationDate asc")
    private List<Comment> commentsList;

    @ManyToOne
    @JoinTable(name = "NEWS_AUTHORS", joinColumns = {@JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {@JoinColumn(name = "AUTHOR_ID")})
    private Author author;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "NEWS_TAGS", joinColumns = {@JoinColumn(name = "NEWS_ID")}, inverseJoinColumns = {@JoinColumn(name = "TAG_ID")})
    private List<Tag> tagsSet;

    public News() {
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Comment> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comment> commentList) {
        this.commentsList = commentList;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Tag> getTagsSet() {
        return tagsSet;
    }

    public void setTagsSet(List<Tag> tagsList) {
        this.tagsSet = tagsList;
    }

}