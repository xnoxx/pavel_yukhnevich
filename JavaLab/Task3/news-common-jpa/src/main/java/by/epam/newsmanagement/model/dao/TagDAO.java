package by.epam.newsmanagement.model.dao;

import by.epam.newsmanagement.model.entities.Tag;

public interface TagDAO extends GenericCRUDOperationsDAO<Tag> {

}
