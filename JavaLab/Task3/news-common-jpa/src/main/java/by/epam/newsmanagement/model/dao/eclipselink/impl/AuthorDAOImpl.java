package by.epam.newsmanagement.model.dao.eclipselink.impl;

import by.epam.newsmanagement.model.dao.AuthorDAO;
import by.epam.newsmanagement.model.entities.Author;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Transactional
public class AuthorDAOImpl extends BaseDAO<Author> implements AuthorDAO {
    AuthorDAOImpl(Class<Author> type) {
        this.type = type;
    }

    public List<Author> getAllExceptExpired() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Author> cq = cb.createQuery(Author.class);
        Root<Author> author = cq.from(Author.class);
        Path<Date> date = author.get("expiredDate");
        cq.select(author).where(cb.or(
                date.isNull(),
                cb.greaterThan(date, new Date())
        ));

        return entityManager.createQuery(cq).getResultList();
    }


}
