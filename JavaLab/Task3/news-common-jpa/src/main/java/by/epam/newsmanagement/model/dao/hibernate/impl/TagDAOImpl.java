package by.epam.newsmanagement.model.dao.hibernate.impl;

import by.epam.newsmanagement.model.dao.TagDAO;
import by.epam.newsmanagement.model.entities.Tag;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class TagDAOImpl extends BaseDAO<Tag> implements TagDAO {

    public TagDAOImpl(Class<Tag> type) {
        this.type = type;
    }

}
