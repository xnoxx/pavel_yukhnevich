package by.epam.newsmanagement.model.services.impl;

import by.epam.newsmanagement.model.dao.AuthorDAO;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.exceptions.ResourceNotFoundException;
import by.epam.newsmanagement.model.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Transactional
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorDAO authorDAO;

    public void setExpired(Long id) {
        authorDAO.read(id).setExpiredDate(new Date());
    }

    public List<Author> getAll() {
        return authorDAO.getAll();
    }

    public List<Author> getAllExceptExpired() {
        return authorDAO.getAllExceptExpired();
    }

    public Long create(Author author) {
        return authorDAO.create(author);
    }

    public Author read(Long id) {
        Author author = authorDAO.read(id);
        if (author != null) {
            return author;
        } else {
            throw new ResourceNotFoundException("Author with id: " + id + " not found");
        }
    }

    public void update(Author author) {
        authorDAO.update(author);
    }

    public void delete(Long id) {
        throw new UnsupportedOperationException("Can not delete an Author. Use 'setExpire()' method please");
    }

}
