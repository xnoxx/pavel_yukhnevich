package by.epam.newsmanagement.model.services;

import by.epam.newsmanagement.model.entities.Author;

import java.util.List;

public interface AuthorService extends GenericService<Author> {

    void setExpired(Long id);

    List<Author> getAllExceptExpired();

}
