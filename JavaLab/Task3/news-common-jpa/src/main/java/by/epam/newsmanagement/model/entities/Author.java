package by.epam.newsmanagement.model.entities;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "authors")
@SequenceGenerator(name = "AUTHORS_SEQ", sequenceName = "AUTHORS_SEQ")
public class Author extends BaseEntity implements Serializable {

    @Id
    @Column(name = "author_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHORS_SEQ")
    private Long id;

    @NotBlank(message = "author.blank")
    @Length(min = 1, max = 30, message = "author.size")
    @Column(name = "author_name")
    private String name;

    @Column(name = "expired")
    @Temporal(TemporalType.DATE)
    private Date expiredDate;

    public Author() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
