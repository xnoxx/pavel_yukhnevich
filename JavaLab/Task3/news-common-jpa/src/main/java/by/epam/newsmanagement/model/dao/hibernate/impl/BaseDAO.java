package by.epam.newsmanagement.model.dao.hibernate.impl;


import by.epam.newsmanagement.model.dao.GenericCRUDOperationsDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public abstract class BaseDAO<T> implements GenericCRUDOperationsDAO<T> {

    protected SessionFactory sessionFactory;
    protected Class<T> type;

    public Long create(T entity) {
        return (Long) getSession().save(entity);
    }

    public T read(Long id) {
        return getSession().get(type, id);
    }

    public void update(T entity) {
        getSession().merge(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        return getSession().createQuery("FROM " + type.getName()).list();
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
