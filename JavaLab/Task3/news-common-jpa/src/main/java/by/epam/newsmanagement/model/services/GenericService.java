package by.epam.newsmanagement.model.services;

import java.util.List;

public interface GenericService<T> {

    String MESSAGE_SERVICE_EXCEPTION = "Problems with service request";

    Long create(T entity);

    T read(Long id);

    void update(T entity);

    void delete(Long id);

    List<T> getAll();


}
