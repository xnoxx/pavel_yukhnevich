<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<link href="<c:url value="/resources/css/single-news.css" />" rel="stylesheet">
<script src="<c:url value= "/resources/js/tagsEditing.js"/>"></script>

<tiles:insertDefinition name="base-template">
    <tiles:putAttribute name="body">
        
        <div class="news">
            <div class="title"><c:out value = "${news.title}"/></div>
            <fmt:message key="date.format" var="format" />
            <fmt:formatDate value="${news.modificationDate}" pattern="${format}" var="formattedDate" />
            <div class="date"><c:out value = "${formattedDate}"/></div>
            <div class="author"><c:out value = "(by ${news.author.name})"/></div>
            <div class="full-text"><c:out value = "${news.fullText}"/></div>
            <div class="comments">
                <c:forEach items="${news.commentsList}" var="comment">
                    <form action = "/deleteComment" method = "POST">
                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                        <fmt:formatDate value="${comment.creationDate}" pattern="${format}" var="commentDate" />
                        <div class="comment-date"><c:out value = "${commentDate}"/></div>
                        <div class="comment-text"><c:out value = "${comment.text}"/>
                            <button class="close" type = "submit" onclick="return confirm_alert('<spring:message code="alert.confirmation"/>');">x</button>
                        </div>
                        <input type="hidden" value="${comment.id}" name="id">
                        <input type="hidden" value="${news.id}" name="newsId">
                    </form>
                </c:forEach>
            </div>

            <form action="/postComment" method="POST">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<textarea name="text" class="comment-area" required="required"></textarea>
                <input type="hidden" value="${news.id}" name="newsId">
                <div>
                    <button type="submit" class="button">
                        <spring:message code="single-news.post" />
                    </button>
                </div>
            </form>
            <div class = "navigation">
                <a href = "/previous?newsId=${news.id}"><spring:message code="single-news.previous" /></a>
                <a href = "/next?newsId=${news.id}" class = "next"><spring:message code="single-news.next" /></a>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>