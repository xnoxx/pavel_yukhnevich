<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<script src="<c:url value= "/resources/js/tagsEditing.js"/>"></script>
<link href="<c:url value="/resources/css/allNews.css" />" rel="stylesheet">

<tiles:insertDefinition name="base-template">
    <tiles:putAttribute name="body">

        <div class="search-criteria">
            <form method="POST" action="/filter" class="filter-form">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/> <select name="author">
                <option value=""><spring:message code="news-list.author"/></option>
                <c:forEach items="${authors}" var="author">
                    <option value="${author.id}"><c:out
                            value="${author.name}"/></option>
                </c:forEach>
            </select> <select onclick="showCheckboxes()">
                <option disabled><spring:message code="news-list.tags"/></option>
            </select>

                <div id="checkboxes" style="display: none">
                    <c:forEach items="${tags}" var="tag">
                        <div class="checkbox">
                            <input type="checkbox" value="${tag.id}" name="tagId[]"/>
                            <c:out value="${tag.name}"/>
                        </div>
                    </c:forEach>
                </div>
                <button type="submit">
                    <spring:message code="news-list.filter"/>
                </button>
            </form>

            <form method="POST" action="/reset" class="reset-form">
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <button type="submit">
                    <spring:message code="news-list.reset"/>
                </button>
            </form>
        </div>

        <c:choose>
            <c:when test="${empty newsList}">
                <div class="message">
                    <spring:message code="label.no.news"/>
                </div>
            </c:when>
            <c:otherwise>
                <div class="news">
                    <form method="POST" action="/deleteNews">
                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}"/>
                        <c:forEach items="${newsList}" var="news">
                            <div class="single-news">
                                <div class="title">
                                    <a href="/singleNews/${news.id}"><c:out
                                            value="${news.title}"/></a>
                                </div>

                                <div class="author">(by ${news.author.name})</div>
                                <fmt:message key="date.format" var="format"/>
                                <fmt:formatDate value="${news.modificationDate}"
                                                pattern="${format}" var="formattedDate"/>

                                <div class="date">
                                    <c:out value="${formattedDate}"/>
                                </div>

                                <div class="short-text">
                                    <c:out value="${news.shortText}"/>
                                </div>

                                <div class="right-block">
                                    <div class="tags">
                                        <c:forEach items="${news.tagsSet}" var="tag">
                                            <c:out value="${tag.name}"/>
                                        </c:forEach>
                                    </div>
                                    <div class="comments">
                                        <spring:message code="news-list.comments"/>
                                        <c:out value="(${fn:length(news.commentsList)})"/>
                                    </div>
                                    <div class="edit">
                                        <a href="/editNews/${news.id}"><spring:message
                                                code="news-list.edit"/></a>
                                    </div>
                                    <input type="checkbox" name="newsToDelete[]"
                                           value="${news.id}"/>
                                </div>

                            </div>
                        </c:forEach>
                        <input type="hidden" value="${currentPage}">

                        <div class="delete">
                            <button class="delete-btn" type="submit"
                                    onclick="return confirm_alert('<spring:message
                                            code="alert.confirmation"/>');">
                                <spring:message code="news-list.delete"/>
                            </button>
                        </div>
                    </form>
                </div>
            </c:otherwise>
        </c:choose>

        <div class="pages-panel">
            <div class="pages">
                <c:forEach items="${pages}" var="page">

                    <form method="GET" action="/newsList">
                        <input type="hidden" name="${_csrf.parameterName}"
                               value="${_csrf.token}"/>
                        <c:choose>
                            <c:when test="${page == currentPage}">
                                <div class="button">
                                    <button class="current-page" id="${page}" type="submit"
                                            name="page" value="${page}">${page}</button>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="button">
                                    <button id="${page}" type="submit" name="page" value="${page}">
                                        <c:out value="${page}"/>
                                    </button>
                                </div>
                            </c:otherwise>

                        </c:choose>
                    </form>

                </c:forEach>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
<script src="<c:url value= "/resources/js/checkboxes.js"/>"></script>


