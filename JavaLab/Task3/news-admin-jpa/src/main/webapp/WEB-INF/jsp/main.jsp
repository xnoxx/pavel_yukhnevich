<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link href="<c:url value="/resources/css/layout.css" />" rel="stylesheet">
    <title> News management </title>
</head>

<body>
<div class="main">

    <div class="header">
        <tiles:insertAttribute name="header"/>
    </div>

    <div class="body">
        <div class="content">
            <div class="menu">
                <div class="menu-content">
                    <tiles:insertAttribute name="menu"/>
                </div>
            </div>
            <div class="inner-data">
                <tiles:insertAttribute name="body"/>
            </div>
        </div>
    </div>

    <div class="footer">
        <tiles:insertAttribute name="footer"/>
    </div>

</div>
</body>
</html>