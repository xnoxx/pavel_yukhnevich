<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<link href="<c:url value="/resources/css/tags.css" />" rel="stylesheet">
<script src="<c:url value= "/resources/js/tagsEditing.js"/>"></script>


<tiles:insertDefinition name="base-template">
    <tiles:putAttribute name="body">

        <div class="tag-content">
            <input type="hidden" value="${tagForUpdate.id}"/>
            <c:forEach items="${tags}" var="tag">
                <div class="tag">

                    <c:choose>
                        <c:when test="${tag.id == tagForUpdate.id}">


                            <sf:form method="POST" modelAttribute="tagForUpdate"
                                     action="/updateTag">
                                <div><sf:errors path="name" cssClass="error"/></div>
                                <label for="tag"><spring:message code="tag.label"/></label>

                                <sf:input name="tag" type="text" value="${tag.name}" path="name"
                                          size="60"/>

                                <sf:hidden path="id"/>
                                <button type="submit" value="update" class="button"
                                        onclick="return confirm_alert('<spring:message
                                                code="alert.confirmation"/>');"><spring:message
                                        code="tag.update"/></button>
                            </sf:form>
                            <div class="links">
                                <a href="/deleteTag?id=${tag.id}"
                                   onclick="return confirm_alert('<spring:message
                                           code="alert.confirmation"/>');"><spring:message
                                        code="tag.delete"/></a> <a href="/tags"><spring:message
                                    code="tag.cancel"/></a>
                            </div>
                        </c:when>

                        <c:otherwise>
                            <label for="tag"><spring:message code="tag.label"/></label>
                            <input id="${tag.id}" type="text" value="${tag.name}"
                                   size=60 disabled="disabled"/>
                            <a href="/editTag/${tag.id}" class="edit-link"><spring:message
                                    code="tag.edit"/></a>
                        </c:otherwise>
                    </c:choose>

                </div>
            </c:forEach>

            <div class="add-tag">
                <sf:form method="POST" modelAttribute="tagToAdd" action="/addTag">
                    <div><sf:errors path="name" cssClass="error-add"/></div>
                    <label for="add"><spring:message code="tag.add"/></label>
                    <sf:input type="text" id="add" size="60" path="name"/>

                    <button type="submit" value="addTag" class="button"
                            onclick="return confirm_alert('<spring:message
                                    code="alert.confirmation"/>');"><spring:message
                            code="tag.save"/></button>
                </sf:form>
            </div>
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>


