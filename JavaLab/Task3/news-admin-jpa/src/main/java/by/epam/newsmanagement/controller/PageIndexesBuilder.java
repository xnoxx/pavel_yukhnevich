package by.epam.newsmanagement.controller;

class PageIndexesBuilder {

    static final int DEFAULT_ON_PAGE_VALUE = 3;
    private int contentOnPage;

    PageIndexesBuilder(){
        this.contentOnPage = DEFAULT_ON_PAGE_VALUE;
    }

    PageIndexesBuilder(int contentOnPage) {
        this.contentOnPage = contentOnPage;
    }

    int[] getPageIndexesArray(int contentCount) {

        int intCount = contentCount / contentOnPage;
        int pagesCount = (contentCount % contentOnPage == 0) ? intCount : intCount + 1;

        int[] pagesArray = new int[pagesCount];

        for (int i = 0; i < pagesCount; i++) {
            pagesArray[i] = i + 1;
        }

        return pagesArray;
    }

    int getCurrentStart(int currentPage) {
        return (currentPage - 1) * contentOnPage;
    }

}
