package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.model.services.AuthorService;
import by.epam.newsmanagement.model.services.NewsService;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Controller
public class AddNewsController {

    private NewsService newsService;
    private TagService tagService;
    private AuthorService authorService;

    @Autowired
    NewsValidator validator;

    @Autowired
    public AddNewsController(NewsService newsService, TagService tagService, AuthorService authorService) {
        this.newsService = newsService;
        this.tagService = tagService;
        this.authorService = authorService;
    }

    @RequestMapping(value = {"/saveNews"}, method = RequestMethod.POST)
    public String saveNews(@ModelAttribute("news") News news, Locale locale, @RequestParam(value = "tagId[]", required = false) List<Tag> tagsSet, BindingResult result, Model model) throws GeneralServiceException {
        String relocate;
        validator.validate(news, result);
        if (!result.hasErrors()) {
            Long newsId = news.getId();
            news.setModificationDate(new Date());
            news.setTagsSet(tagsSet);

            if (newsId == null) {
                newsId = newsService.create(news);
            } else {
                newsService.update(news);
            }

            relocate = "redirect:/singleNews/" + newsId;

        } else {
            List<Tag> listTag = tagService.getAll();
            List<Author> authorList = authorService.getAllExceptExpired();
            model.addAttribute("tags", listTag);
            model.addAttribute("authors", authorList);
            model.addAttribute("news", news);

            relocate = "addNews";
        }

        return relocate;
    }

    @RequestMapping(value = {"/saveNews"}, method = RequestMethod.GET)

    public String language(Model model) throws GeneralServiceException {
        return "redirect:/addNews";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return "error";
    }

}
