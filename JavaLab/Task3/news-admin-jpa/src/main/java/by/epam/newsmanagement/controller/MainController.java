package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.entities.*;
import by.epam.newsmanagement.model.services.AuthorService;
import by.epam.newsmanagement.model.services.NewsService;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class MainController {

    private TagService tagService;
    private AuthorService authorService;
    private NewsService newsService;

    @Autowired
    public MainController(TagService tagService, AuthorService authorService, NewsService newsService) {
        this.tagService = tagService;
        this.authorService = authorService;
        this.newsService = newsService;
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String showNewsList() {
        return "login";
    }

    @RequestMapping(value = {"/addNews"}, method = RequestMethod.GET)
    public String showAddNews(Model model) throws GeneralServiceException {
        News news = new News();
        Date today = new Date();
        Author author = new Author();
        List<Comment> commentsList = new ArrayList<Comment>();
        model.addAttribute("tags", tagService.getAll());

        List<Author> authorList = authorService.getAllExceptExpired();
        model.addAttribute("authors", authorList);

        news.setAuthor(author);
        news.setCommentsList(commentsList);
        news.setCreationDate(today);
        news.setModificationDate(today);
        model.addAttribute("news", news);

        return "addNews";
    }

    @RequestMapping(value = {"/authors"}, method = RequestMethod.GET)
    public String showAuthors(Model model) throws GeneralServiceException {
        List<Author> authorList = authorService.getAll();
        model.addAttribute("authors", authorList);
        Author author = new Author();
        model.addAttribute("authorToAdd", author);
        return "authors";
    }

    @RequestMapping(value = {"/tags"}, method = RequestMethod.GET)
    public String showTags(Model model) throws GeneralServiceException {

        List<Tag> tagList = tagService.getAll();
        model.addAttribute("tags", tagList);
        Tag tag = new Tag();
        model.addAttribute("tagToAdd", tag);
        return "tags";
    }

    @RequestMapping(value = {"/newsList"}, method = RequestMethod.GET)
    public String showNewsList(@RequestParam(value = "page", defaultValue = "1") int currentPage, Model model, HttpSession session) throws GeneralServiceException {

        SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCriteria");
        if (sc == null) {
            sc = new SearchCriteria();
        }

        Integer newsOnPage = (Integer) session.getAttribute("newsOnPage");
        if(newsOnPage == null){
            newsOnPage = PageIndexesBuilder.DEFAULT_ON_PAGE_VALUE;
        }

        PageIndexesBuilder pageIndexesBuilder = new PageIndexesBuilder(newsOnPage);
        int[] pageIndexesArray = pageIndexesBuilder.getPageIndexesArray(newsService.getCount(sc));
        model.addAttribute("pages", pageIndexesArray);

        List<News> newsList = newsService.getAll(sc, currentPage, newsOnPage);
        model.addAttribute("currentPage", currentPage);
        model.addAttribute("newsList", newsList);

        List<Author> authorList = authorService.getAll();
        model.addAttribute("authors", authorList);

        List<Tag> tagList = tagService.getAll();
        model.addAttribute("tags", tagList);

        return "newsList";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return "error";
    }
}
