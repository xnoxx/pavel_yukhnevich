package by.epam.newsmanagement.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import by.epam.newsmanagement.model.entities.Comment;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import by.epam.newsmanagement.model.services.CommentService;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import static by.epam.newsmanagement.controller.ContentNavigator.*;

@Controller
public class SingleNewsController {

	private CommentService commentService;
	private NewsService newsService;

	@Autowired
	public SingleNewsController(NewsService newsService, CommentService commentService) {
		this.commentService = commentService;
		this.newsService = newsService;
	}

	@RequestMapping(value = { "/postComment" }, method = RequestMethod.POST)
	public String postComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "text") String commentText) throws SQLException, GeneralServiceException {
		commentText = commentText.replaceAll("\\s+", "");
		if (commentText != null && !commentText.isEmpty()  && commentText.length() < 100) {
			Comment comment = new Comment();
			comment.setCreationDate(new Timestamp(new Date().getTime()));
			comment.setText(commentText);
			News news = new News();
			news.setId(newsId);
			comment.setNews(news);
			commentService.create(comment);
		}
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/deleteComment" }, method = RequestMethod.POST)
	public String deleteComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "id") Long commentId) throws GeneralServiceException {
		commentService.delete(commentId);
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.GET)
	public String showNextNews(Long newsId, HttpSession session) throws GeneralServiceException {
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCriteria");
		List<Long> newsIds = newsService.getIds(sc);
		Long nextNewsId = getNextContentId(newsIds, newsId);
		return "redirect:/singleNews/" + nextNewsId;
	}

	@RequestMapping(value = { "/previous" }, method = RequestMethod.GET)
	public String showPreviousNews(Long newsId, HttpSession session) throws GeneralServiceException {
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCriteria");
		List<Long> newsIds = newsService.getIds(sc);
		Long previousId = getPreviousContentId(newsIds, newsId);
		return "redirect:/singleNews/" + previousId;
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
