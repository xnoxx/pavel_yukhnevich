package by.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.newsmanagement.model.entities.*;
import by.epam.newsmanagement.model.services.AuthorService;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.services.NewsService;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class NewsListController {

    private AuthorService authorService;
    private TagService tagService;
    private NewsService newsService;

    @Autowired
    public NewsListController(AuthorService authorService, TagService tagService, NewsService newsService) {
        this.authorService = authorService;
        this.tagService = tagService;
        this.newsService = newsService;
    }

    @RequestMapping(value = {"/editNews/{newsId}"}, method = RequestMethod.GET)
    public String editNews(@PathVariable("newsId") Long newsId, Model model) throws GeneralServiceException {

        News news = newsService.read(newsId);
        List<Tag> listTag = tagService.getAll();
        List<Author> authorList = authorService.getAll();
        List<Comment> commentsList = new ArrayList<Comment>();
        model.addAttribute("tags", listTag);
        model.addAttribute("authors", authorList);
        news.setCommentsList(commentsList);
        news.setModificationDate(new Date());
        model.addAttribute("news", news);
        return "addNews";
    }

    @RequestMapping(value = {"/filter"}, method = RequestMethod.POST)
    public String filterNews(HttpServletRequest request, @RequestParam(value = "tagId[]", required = false) Long[] tagIds, @RequestParam(value = "author", required = false) Long authorId) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIdFromArray(tagIds);
        request.getSession().setAttribute("searchCriteria", searchCriteria);
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/reset"}, method = RequestMethod.POST)
    public String reset(HttpSession session) {
        session.setAttribute("searchCriteria", null);
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/deleteNews"}, method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = "newsToDelete[]", required = false) Long[] newsToDeleteArray) throws GeneralServiceException {
        if (newsToDeleteArray != null) {
            for (Long id : newsToDeleteArray) {
                newsService.delete(id);
            }
        }
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/singleNews/{newsId}"}, method = RequestMethod.GET)
    public String showSingleNews(@PathVariable("newsId") Long newsId, Model model) throws GeneralServiceException {
        News news = newsService.read(newsId);
        model.addAttribute("news", news);
        return "singleNews";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return "error";
    }

}