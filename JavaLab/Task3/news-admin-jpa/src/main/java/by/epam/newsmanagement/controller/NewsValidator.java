package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.News;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsValidator implements Validator {

	public boolean supports(Class<?> type) {
		return News.class.isAssignableFrom(type);
	}

	public void validate(Object arg0, Errors arg1) {
		News news = (News) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "title", "validation.title.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "shortText", "validation.shortText.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "fullText", "validation.fullText.empty");

		String title = news.getTitle();

		if ((title.length()) > 30) {
			arg1.rejectValue("title", "validation.title.large");
		}

		String shortText = news.getShortText();

		if ((shortText.length()) > 100) {
			arg1.rejectValue("shortText", "validation.shortText.large");
		}

		String fullText = news.getFullText();

		if ((fullText.length()) > 2000) {
			arg1.rejectValue("fullText", "validation.fullText.large");
		}

	}
}
