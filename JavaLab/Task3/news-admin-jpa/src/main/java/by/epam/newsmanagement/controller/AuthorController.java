package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.services.AuthorService;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.sql.SQLException;
import java.util.List;

@Controller
public class AuthorController {

	private AuthorService authorService;

	@Autowired
	public AuthorController(AuthorService authorService) {
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/editAuthor/{id}" }, method = RequestMethod.GET)
	public String editAuthor(@PathVariable("id") Long authorId, Model model) throws GeneralServiceException, SQLException {
		Author authorForUpdate = authorService.read(authorId);
		List<Author> authorList = authorService.getAll();
		Author author = new Author();
		model.addAttribute("authors", authorList);
		model.addAttribute("authorForUpdate", authorForUpdate);
		model.addAttribute("authorToAdd", author);
		return "authors";
	}

	@RequestMapping(value = { "/deleteAuthor" }, method = RequestMethod.GET)
	public String deleteAuthor(@RequestParam(value = "id") Long authorId) throws GeneralServiceException {
		authorService.setExpired(authorId);
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.POST)
	public ModelAndView updateAuthor(@Valid @ModelAttribute("authorForUpdate") Author author, BindingResult result) throws GeneralServiceException {
		if (!result.hasErrors()) {
			authorService.update(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;

		} else {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModelUpdate();
		}
	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.GET)
	public String languageUpdate() throws GeneralServiceException {
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.POST)
	public ModelAndView addAuthor(@Valid @ModelAttribute("authorToAdd") Author author, BindingResult result) throws GeneralServiceException, SQLException {

		if (!result.hasErrors()) {
			authorService.create(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;
		} else {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModel();
		}
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.GET)
	public String languageAdd() throws GeneralServiceException {
		return "redirect:/authors";
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

	private ModelAndView formModel() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = authorService.getAll();
		model.addObject("authors", authorList);
		model.setViewName("authors");

		return model;
	}

	private ModelAndView formModelUpdate() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = authorService.getAll();
		model.addObject("authors", authorList);
		Author author = new Author();
		model.addObject("authorToAdd", author);
		model.setViewName("authors");

		return model;
	}

}
