package by.epam.newsmanagement.controller;

import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class TagController {

	private TagService tagService;

	@Autowired
	public TagController(TagService tagService) {
		this.tagService = tagService;
	}

	@RequestMapping(value = { "/deleteTag" }, method = RequestMethod.GET)
	public String deleteTag(@RequestParam(value = "id") Long tagId) throws GeneralServiceException {
		tagService.delete(tagId);
		return "redirect:/tags";
	}

	@RequestMapping(value = { "/editTag/{tagId}" }, method = RequestMethod.GET)
	public String editTag(@PathVariable("tagId") Long tagId, Model model) throws GeneralServiceException, SQLException {
		Tag tagForUpdate = tagService.read(tagId);
		model.addAttribute("tagForUpdate", tagForUpdate);
		List<Tag> tagList = tagService.getAll();
		model.addAttribute("tags", tagList);
		Tag tag = new Tag();
		model.addAttribute("tagToAdd", tag);
		return "tags";
	}

	@RequestMapping(value = { "/updateTag" }, method = RequestMethod.POST)
	public ModelAndView updateTag(@Valid @ModelAttribute("tagForUpdate") Tag tag, BindingResult result) throws GeneralServiceException {
		if (!result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView();
			tagService.update(tag);
			modelAndView.setViewName("redirect:/tags");
			return modelAndView;
		} else {
			return formModelUpdate();
		}
	}

	@RequestMapping(value = { "/updateTag" }, method = RequestMethod.GET)
	public String languageUpdate() throws GeneralServiceException {
		return "redirect:/tags";
	}

	@RequestMapping(value = { "/addTag" }, method = RequestMethod.POST)
	public ModelAndView addTag(@Valid @ModelAttribute("tagToAdd") Tag tag, BindingResult result) throws GeneralServiceException, SQLException {
		if (!result.hasErrors()) {
			ModelAndView modelAndView = new ModelAndView();
			tagService.create(tag);
			modelAndView.setViewName("redirect:/tags");
			return modelAndView;

		} else {
			return formModel();
		}
	}

	@RequestMapping(value = { "/addTag" }, method = RequestMethod.GET)
	public String languageAdd() throws GeneralServiceException {
		return "redirect:/tags";
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, GeneralServiceException ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

	private ModelAndView formModel() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tagList = tagService.getAll();

		model.addObject("tags", tagList);
		model.setViewName("tags");
		return model;
	}

	private ModelAndView formModelUpdate() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Tag> tagList = tagService.getAll();

		model.addObject("tags", tagList);
		model.setViewName("tags");
		Tag tag = new Tag();
		model.addObject("tagToAdd", tag);
		return model;
	}

}
