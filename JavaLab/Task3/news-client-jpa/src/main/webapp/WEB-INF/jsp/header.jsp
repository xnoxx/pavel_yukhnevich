<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="header-content">

    <h2>
        <spring:message code="header.label"/>
    </h2>

    <div class="languages">
        <a href="?lang=en"><spring:message code="header.language.en"/></a> |
        <a href="?lang=ru"><spring:message code="header.language.ru"/></a>
    </div>
</div>