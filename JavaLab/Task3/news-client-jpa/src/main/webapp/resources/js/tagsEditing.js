function edit(id, divId, linkId) {
			document.getElementById(id).disabled = false;
			
			var oldDiv = document.getElementById(divId);

			var newDiv = document.createElement("div");
			newDiv.className = "links";

			var updateLink = document.createElement("a");
			var deleteLink = document.createElement("a");
			var cancelLink = document.createElement("a");

			newDiv.appendChild(updateLink);
			newDiv.appendChild(deleteLink);
			newDiv.appendChild(cancelLink);

			updateLink.appendChild(document.createTextNode("update"));
			updateLink.href = "updateTag";

			deleteLink.appendChild(document.createTextNode("delete"));
			var deleteHref = "deleteTag?tagId=" + id;
			deleteLink.href = deleteHref;

			cancelLink.appendChild(document.createTextNode("cancel"));
			cancelLink.href = "cancelTag";
			
			document.getElementById(divId).appendChild(newDiv);

			var editLink = document.getElementById(linkId);
			editLink.style.display = (editLink.style.display == 'none') ? ''
					: 'none';
}

function confirm_alert(node) {
    return confirm(node);
}

function changeButtonColour(id){
	document.getElementById(id).className = "current-button";
}


