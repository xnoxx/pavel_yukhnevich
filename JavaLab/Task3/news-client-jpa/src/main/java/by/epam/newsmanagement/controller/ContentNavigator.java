package by.epam.newsmanagement.controller;

import java.util.List;

class ContentNavigator {

    private ContentNavigator(){

    }

    static Long getNextContentId(List<Long> ids, Long id) {

        int index = ids.indexOf(id);
        int next = index + 1;
        boolean isNotLast = index < ids.size() - 1;

        return isNotLast ? ids.get(next) : ids.get(0);
    }

    static Long getPreviousContentId(List<Long> ids, Long id) {

        int index = ids.indexOf(id);
        int previous = index - 1;
        int last = ids.size() - 1;
        boolean isNotFirst = index > 0;

        return isNotFirst ? ids.get(previous) : ids.get(last);
    }
}
