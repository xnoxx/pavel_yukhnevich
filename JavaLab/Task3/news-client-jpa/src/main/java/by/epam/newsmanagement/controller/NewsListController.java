package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class NewsListController {

    private NewsService newsService;

    @Autowired
    public NewsListController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = {"/filter"}, method = RequestMethod.POST)
    public String filterNews(HttpServletRequest request, @RequestParam(value = "tagId[]", required = false) Long[] tagIds, @RequestParam(value = "author", required = false) Long authorId) {

        SearchCriteria sc = new SearchCriteria();
        sc.setAuthorId(authorId);
        sc.setTagIdFromArray(tagIds);
        request.getSession().setAttribute("searchCriteria", sc);

        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/reset"}, method = RequestMethod.POST)
    public String reset(HttpSession session) {

        session.setAttribute("searchCriteria", null);

        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/singleNews/{newsId}"}, method = RequestMethod.GET)
    public String showSingleNews(@PathVariable("newsId") Long newsId, Model model) throws GeneralServiceException {

        News news = newsService.read(newsId);
        model.addAttribute("news", news);

        return "singleNews";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());

        return "error";
    }

}