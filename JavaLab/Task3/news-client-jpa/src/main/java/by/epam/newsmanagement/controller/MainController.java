package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.model.services.AuthorService;
import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.model.services.NewsService;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/")
public class MainController {

    private NewsService newsService;
    private TagService tagService;
    private AuthorService authorService;

    @Autowired
    public MainController(NewsService newsService, TagService tagService, AuthorService authorService) {
        this.newsService = newsService;
        this.tagService = tagService;
        this.authorService = authorService;
    }

    @RequestMapping(value = {"/newsList"}, method = RequestMethod.GET)
    public String loadNewsList(@RequestParam(value = "page", defaultValue = "1") int currentPage, Model model, HttpSession session) throws GeneralServiceException {

        SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCriteria");
        if (sc == null) {
            sc = new SearchCriteria();
        }

        Integer newsOnPage = (Integer) session.getAttribute("newsOnPage");
        if (newsOnPage == null) {
            newsOnPage = PageIndexesBuilder.DEFAULT_ON_PAGE_VALUE;
        }

        PageIndexesBuilder pageIndexesBuilder = new PageIndexesBuilder();

        int[] pagesNumbersArray = pageIndexesBuilder.getPageIndexesArray(newsService.getCount(sc));
        model.addAttribute("pages", pagesNumbersArray);

        List<News> newsList = newsService.getAll(sc, currentPage, newsOnPage);

        model.addAttribute("currentPage", currentPage);
        model.addAttribute("newsList", newsList);

        List<Author> authorList = authorService.getAll();
        model.addAttribute("authors", authorList);

        List<Tag> tagList = tagService.getAll();
        model.addAttribute("tags", tagList);

        return "newsList";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return "error";
    }

}
