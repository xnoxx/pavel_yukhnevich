package by.epam.newsmanagement.controller.command;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.services.GeneralServiceException;
import org.apache.log4j.Logger;

public abstract class Command {

    protected static final String FORWARD_MAIN = "jsp/main.jsp";
    protected static final String PARAM_CONTENT = "content";
    protected static final String CONTENT_NEWS_PAGE = "allnews.jsp";
    protected static final String PARAM_NEWSVO_LIST = "newsVOList";
    protected static final String PARAM_TAGS_LIST = "tagsList";
    protected static final String PARAM_AUTHORS_LIST = "authorsList";
    protected static final String PARAM_NEWSVO = "newsVO";
    protected static final String CONTENT_SINGLE_NEWS = "singlenews.jsp";
    protected static final String PARAM_NEWS_ID = "newsId";
    protected static final String PARAM_SEARCH_CRITERIA = "sc";
    protected static final int EMPTY = -1;

    /**
     * This address to which will is realized transition after performing the command
     */
    private String forward;

    /**
     * This is logger which print some messages to log file
     */
    protected static Logger logger = Logger.getLogger(Command.class);

    public String getForward() {
        return forward;
    }

    public void setForward(String forward) {
        this.forward = forward;
    }

    /**
     * This is abstract method which causes methods a business-logic and sends results on jsp
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException      a IOException
     */
    public abstract void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, GeneralServiceException;

}
