package by.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.newsmanagement.services.GeneralServiceException;
import org.apache.log4j.Logger;

import by.epam.newsmanagement.controller.command.Command;
import by.epam.newsmanagement.controller.command.CommandFactory;

public class ServletController extends HttpServlet {

    /**
     *
     */
    private static Logger logger = Logger.getLogger(ServletController.class);
    private static final long serialVersionUID = -3096202475997328442L;
    private static final String SYMBOLS_CONTENT_TYPE = "text/html;charset=UTF-8";
    private static final String PARAM_LANGUAGE = "language";
    private static final String MSG_SERVICE_PROBLEMS = "Problems in service layer";
    private static final String LOCALE_EN = "en";
    private static final String MSG_ERROR = "ERROR!!!";

    /**
     * This method handles requests
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException      a IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method handles requests
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException      a IOException
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * This method gets
     * by request and execute this command. Then it goes to next jsp.
     *
     * @param request  a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException      a IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Command command = CommandFactory.getCommand(request);
        response.setContentType(SYMBOLS_CONTENT_TYPE);
        try {

            if (request.getSession().getAttribute(PARAM_LANGUAGE) == null)
                request.getSession().setAttribute(PARAM_LANGUAGE, LOCALE_EN);

            command.processRequest(request, response);

            if (command.getForward() != null) {
                request.getRequestDispatcher(command.getForward()).forward(request, response);
            }

        } catch (IOException ex) {
            logger.error(ex);
            request.getRequestDispatcher(MSG_ERROR).forward(request, response);
        } catch (GeneralServiceException e) {
            logger.error(MSG_SERVICE_PROBLEMS);
        }
    }
}
