package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.controller.util.Navigator;
import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class Previous extends Command {

    private NewsService newsService;

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {

        Navigator navigator = new Navigator();
        SearchCriteria sc = (SearchCriteria) request.getSession().getAttribute(PARAM_SEARCH_CRITERIA);
        Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
        int  newsCount = newsService.getNewsCount();
        List<NewsVO> newsVOList = newsService.getListOfNewsVO(sc, 0, newsCount);
        NewsVO previousNews = navigator.getPreviousNewsVO(newsVOList, newsId);
        request.setAttribute(PARAM_CONTENT, CONTENT_SINGLE_NEWS);
        request.setAttribute(PARAM_NEWSVO, previousNews);
        setForward(FORWARD_MAIN);
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }
}
