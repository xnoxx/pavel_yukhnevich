package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ViewSingleNews extends Command {

    private NewsService newsService;

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {

        Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
        NewsVO newsVO = newsService.getSingleNewsVOByNewsId(newsId);
        request.setAttribute(PARAM_CONTENT, CONTENT_SINGLE_NEWS);
        request.setAttribute(PARAM_NEWSVO, newsVO);
        setForward(FORWARD_MAIN);
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }
}
