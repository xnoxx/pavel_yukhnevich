package by.epam.newsmanagement.controller.command;

import java.io.IOException;
import java.util.Locale;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;

/**
 * This class implements a pattern command
 * This class changes language of user interface
 */
public class ChangeLanguage extends Command {

    private static final String PARAM_LANGUAGE = "language";

    /**
     * This method changes language of user interface
     * @param request a httpServletRequest
     * @param response a httpServletResponse
     * @throws ServletException a ServletException
     * @throws IOException a IOException
     */
    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String language = request.getParameter(PARAM_LANGUAGE);
        request.getSession().setAttribute(PARAM_LANGUAGE, language);
        setForward(FORWARD_MAIN);
    }
}

