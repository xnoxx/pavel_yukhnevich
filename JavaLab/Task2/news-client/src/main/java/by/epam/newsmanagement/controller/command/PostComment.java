package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.services.CommentService;
import by.epam.newsmanagement.services.GeneralServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class PostComment extends Command {

    private static final String VIEW_SINGLE_NEWS = "controller?commandName=VIEW_SINGLE_NEWS&newsId=";
    private static final String PARAM_COMMENT_TEXT = "commentText";
    private CommentService commentService;

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {

        String commentText = request.getParameter(PARAM_COMMENT_TEXT);
        Long newsId = Long.parseLong(request.getParameter(PARAM_NEWS_ID));
        Comment comment = new Comment();
        comment.setText(commentText);
        comment.setNewsId(newsId);

        commentService.create(comment);
        response.sendRedirect(VIEW_SINGLE_NEWS + newsId);
    }

    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
}
