package by.epam.newsmanagement.controller.command;

public enum CommandEnum {
	UNKNOWN_COMMAND, LOAD_NEWS, NEXT, PREVIOUS, CHANGE_LANGUAGE, VIEW_SINGLE_NEWS, POST_COMMENT, RESET, TO_PAGE;

}
