package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.services.GeneralServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ToPage extends Command {

    private static final String PARAM_FORWARD_TO_PAGE = "forwardToPage";

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {
        String forward = request.getParameter(PARAM_FORWARD_TO_PAGE);
        setForward(forward);
    }
}
