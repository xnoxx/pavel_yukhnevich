package by.epam.newsmanagement.controller.command;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CommandFactory {

    private static final String SPRING_CONTEXT = "Spring-Context-Client.xml";
    private static final String PARAM_COMMAND = "commandName";

    private static Logger logger = Logger.getLogger(CommandFactory.class);
    public static ApplicationContext context = new ClassPathXmlApplicationContext(SPRING_CONTEXT);

    public static Command getCommand(HttpServletRequest request) {
        Command command = null;
        CommandEnum commandType = getCommandEnum(request.getParameter(PARAM_COMMAND).toUpperCase());

        switch (commandType) {
            case LOAD_NEWS:
                command = context.getBean(LoadNews.class);
                break;
            case CHANGE_LANGUAGE:
                command = new ChangeLanguage();
                break;
            case VIEW_SINGLE_NEWS:
                command = context.getBean(ViewSingleNews.class);
                break;
            case POST_COMMENT:
                command = context.getBean(PostComment.class);
                break;
            case PREVIOUS:
                command = context.getBean(Previous.class);
                break;
            case NEXT:
                command = context.getBean(Next.class);
                break;
            case RESET:
                command = context.getBean(Reset.class);
                break;
            case TO_PAGE:
                command = context.getBean(ToPage.class);
                break;
            default:
                break;
        }

        return command;
    }

    private static CommandEnum getCommandEnum(String commandName) {
        CommandEnum commandEnum;
        try {
            commandEnum = CommandEnum.valueOf(commandName);
        } catch (IllegalArgumentException ex) {
            commandEnum = CommandEnum.UNKNOWN_COMMAND;
            logger.error(ex);
        } catch (NullPointerException ex) {
            commandEnum = CommandEnum.UNKNOWN_COMMAND;
            logger.error(ex);
        }
        return commandEnum;
    }
}
