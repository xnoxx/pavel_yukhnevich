package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.controller.util.PageBuilder;
import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;
import by.epam.newsmanagement.services.TagService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoadNews extends Command {

    private static final String PARAM_TAG_ID = "tagId";
    private static final String PARAM_AUTHOR = "author";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_PAGES_ARRAY = "pages";
    private static final String PARAM_CURRENT_PAGE = "currentPage";
    private NewsService newsService;
    private TagService tagService;
    private AuthorService authorService;

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {

        SearchCriteria searchCriteria;
        if (request.getSession().getAttribute(PARAM_SEARCH_CRITERIA) != null) {
            searchCriteria = (SearchCriteria) request.getSession().getAttribute(PARAM_SEARCH_CRITERIA);
        } else {
            searchCriteria = new SearchCriteria();
        }

        String tagIds[] = request.getParameterValues(PARAM_TAG_ID);
        searchCriteria.setTagsList(buildTagsList(tagIds));

        if (request.getParameter(PARAM_AUTHOR) != null) {
            Long authorId = Long.parseLong(request.getParameter(PARAM_AUTHOR));
            Author author = new Author();
            author.setId(authorId);
            searchCriteria.setAuthor(author);
        }

        List<NewsVO> newsVOList;
        List<Tag> allTags;
        List<Author> allAuthors;

        int currentPage = 1;

        if (request.getParameter(PARAM_PAGE) != null) {
            currentPage = Integer.parseInt(request.getParameter(PARAM_PAGE));
        }

        PageBuilder pageBuilder = new PageBuilder();
        List<NewsVO> newsList = newsService.getListOfNewsVO(searchCriteria, EMPTY, EMPTY);
        int[] pagesNumbersArray = pageBuilder.getNewsPagesArray(newsList);
        int start = pageBuilder.getCurrentStart(currentPage);
        int end = pageBuilder.getCurrentEnd(currentPage);
        newsVOList = newsService.getListOfNewsVO(searchCriteria, start, end);
        allTags = tagService.getAllTags();
        allAuthors = authorService.getAllAuthors();

        request.getSession().setAttribute(PARAM_SEARCH_CRITERIA, searchCriteria);
        request.getSession().setAttribute(PARAM_CURRENT_PAGE, currentPage);
        request.getSession().setAttribute(PARAM_PAGES_ARRAY, pagesNumbersArray);
        request.getSession().setAttribute(PARAM_CONTENT, CONTENT_NEWS_PAGE);
        request.getSession().setAttribute(PARAM_NEWSVO_LIST, newsVOList);
        request.getSession().setAttribute(PARAM_TAGS_LIST, allTags);
        request.getSession().setAttribute(PARAM_AUTHORS_LIST, allAuthors);
        setForward(FORWARD_MAIN);
    }

    public void setNewsService(NewsService newsService) {
        this.newsService = newsService;
    }

    public void setTagService(TagService tagService) {
        this.tagService = tagService;
    }

    public void setAuthorService(AuthorService authorService) {
        this.authorService = authorService;
    }

    private List<Tag> buildTagsList(String[] tagIds) {
        List<Tag> tagsList = null;
        if (tagIds != null) {
            tagsList = new ArrayList<Tag>();

            for (String tagId : tagIds) {
                Tag tag = new Tag();
                Long id = Long.parseLong(tagId);
                tag.setId(id);
                tagsList.add(tag);
            }
        }
        return tagsList;
    }
}
