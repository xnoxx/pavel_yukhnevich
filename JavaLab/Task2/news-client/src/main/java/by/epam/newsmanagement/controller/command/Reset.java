package by.epam.newsmanagement.controller.command;

import by.epam.newsmanagement.services.GeneralServiceException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Reset extends Command{

    private static final String URL_RESET = "controller?commandName=LOAD_NEWS";
    private static final String PARAM_LANG = "language";

    @Override
    public void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, GeneralServiceException {
        Object obj = request.getSession().getAttribute(PARAM_LANG);
        request.getSession().invalidate();
        request.getSession().setAttribute(PARAM_LANG, obj);
        response.sendRedirect(URL_RESET);
    }
}
