<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<link href="<c:url value="/css/layout.css" />" rel="stylesheet">

<div class="header-content">
    <h1>
        <tag:resource key="header.label"/>
    </h1>

    <div class="languages">
        <form method="GET" action="controller">
            <button type="submit" name="commandName" value="CHANGE_LANGUAGE" style="background: none;border: none;display: inline;font: inherit;margin: 0;padding: 0;outline: none;outline-offset: 0;color: #4C50F2;cursor: pointer;text-decoration: underline;"><tag:resource key="header.language.en"/></button>
            <input type="hidden" name="language" value="en">
        </form>
        <form method="GET" action="controller">
            <button type="submit" name="commandName" value="CHANGE_LANGUAGE" style="background: none;border: none;display: inline;font: inherit;margin: 0;padding: 0;outline: none;outline-offset: 0;color: #4C50F2;cursor: pointer;text-decoration: underline;"><tag:resource key="header.language.ru"/></button>
            <input type="hidden" name="language" value="ru">
        </form>
    </div>
</div>

