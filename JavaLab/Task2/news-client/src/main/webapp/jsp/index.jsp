<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>

<jsp:forward page="/controller">
    <jsp:param name="commandName" value="LOAD_NEWS"/>
    <jsp:param name="forwardPage" value="jsp/main.jsp"/>
</jsp:forward>

</body>
</html>
