<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link href="<c:url value="/css/single-news.css" />" rel="stylesheet">
</head>
<body>

<form method="GET" action="controller">
    <button type="submit" name="commandName" value="TO_PAGE"  style="background: none;border: none;display: inline;font: inherit;margin: 0;padding: 0;outline: none;outline-offset: 0;color: #4C50F2;cursor: pointer;text-decoration: underline;"><tag:resource key="single-news.back"/></button>
    <input type="hidden" name="forwardToPage" value="jsp/main.jsp">
</form>

<c:set var="pattern"><tag:resource key="date.format"/></c:set>

<div class="news">

    <div class="title"><b>${newsVO.news.title}</b></div>
    <div class="author">(<tag:resource key="label.author"/> ${newsVO.author.name})</div>
    <div class="date"><fmt:formatDate value="${newsVO.news.creationDate}" pattern="${pattern}"/></div>
    <div class="full-text">${newsVO.news.fullText}</div>
    <div class="comments">
        <c:forEach items="${newsVO.commentsList}" var="comment">
            <div class="comment-date"><fmt:formatDate value="${comment.creationDate}" pattern="${pattern}"/></div>
            <div class="comment-text"> ${comment.text}</div>
        </c:forEach>
    </div>

    <form action="/controller" method="POST">
        <textarea name="commentText" class="comment-area" required="true"></textarea>
        <input type="hidden" value="${newsVO.news.id}" name="newsId"/>

        <div>
            <button type="submit" name="commandName" value="POST_COMMENT" class="button" onclick="<tag:resource key="pattern.name"/>" ><tag:resource key="single-news.post"/> </button>
        </div>
    </form>

    <div class="navigation">
        <div><a href="/controller?commandName=Previous&newsId=${newsVO.news.id}"><tag:resource key="single-news.previous"/></a></div>
        <div><a href="/controller?commandName=Next&newsId=${newsVO.news.id}" class="next"><tag:resource key="single-news.next"/></a></div>
    </div>
</div>
<script type="text/javascript" src="<c:url value= "/js/validator.js"/>"></script>
</body>
</html>