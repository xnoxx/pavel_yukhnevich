<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <link href="<c:url value="/css/home.css" />" rel="stylesheet">
</head>
<body>
<div class="search-criteria">
    <form method="post" action="/controller" class="filter-form">
        <select name="author">
            <option disabled><tag:resource key="author.label"/></option>
            <c:forEach items="${authorsList}" var="author">
                <option value="${author.id}">${author.name}</option>
            </c:forEach>
        </select>
        <select onclick="showCheckboxes()">
            <option disabled><tag:resource key="tag.label"/></option>
        </select>
        <div id="checkboxes" style="display: none">
            <c:forEach items="${tagsList}" var="tag">
                <div class="checkbox">
                    <input type="checkbox" value="${tag.id}" name="tagId"/>${tag.name}
                </div>
            </c:forEach>
        </div>

        <button type="submit" name="commandName" value="LOAD_NEWS"><tag:resource key="news-list.filter"/></button>
    </form>

    <form method="post" action="/controller" class="reset">
        <button type="submit" name="commandName" value="RESET"><tag:resource key="news-list.reset"/></button>
    </form>
</div>

<c:set var="pattern"><tag:resource key="date.format"/></c:set>

<c:forEach items="${newsVOList}" var="newsVO">
    <div class="single-news">
        <div class="title"><a
                href="controller?commandName=VIEW_SINGLE_NEWS&newsId=${newsVO.news.id}"> ${newsVO.news.title} </a></div>
        <div class="author"> (<tag:resource key="label.author"/> ${newsVO.author.name})</div>
        <div class="date"><fmt:formatDate value="${newsVO.news.creationDate}" pattern="${pattern}"/></div>
        <div class="short-text"> ${newsVO.news.shortText} </div>
        <div class="right-block">
            <div class="tags">
                <c:forEach items="${newsVO.tagsList}" var="tag">
                    ${tag.name}
                </c:forEach>
            </div>
            <div class="comments">
                <tag:resource key="news-list.comments"/>(${newsVO.commentsList.size()})
            </div>
            <div class="view">
                <a href="controller?commandName=VIEW_SINGLE_NEWS&newsId=${newsVO.news.id}"><tag:resource
                        key="news-list.view"/></a>
            </div>
        </div>
    </div>
</c:forEach>

<div class="pages-panel">
    <div class="pages">
        <c:forEach items="${pages}" var="page">

            <form method="GET" action="controller">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                <c:choose>
                    <c:when test="${page == currentPage}">
                        <div class="button">
                            <button class="current-page" id="${page}" type="submit"
                                    name="commandName" value="LOAD_NEWS"><c:out value="${page}"/></button>
                            <input type="hidden" name="page" value="${page}"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="button">
                            <button id="${page}" type="submit" name="commandName" value="LOAD_NEWS"><c:out
                                    value="${page}"/></button>
                            <input type="hidden" name="page" value="${page}"/>
                        </div>
                    </c:otherwise>

                </c:choose>
            </form>

        </c:forEach>
    </div>
</div>

<script type="text/javascript" src="<c:url value= "/js/checkboxes.js"/>"></script>
</body>
</html>