<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
    <link href="<c:url value="/css/layout.css" />" rel="stylesheet">
    <title><tag:resource key="header.label"/></title>
</head>

<div class="main">
    <div class="header">
        <jsp:include page="header.jsp"/>
    </div>
    <div class="body">
        <div class="body-content">
            <c:if test="${not empty content}">
                <jsp:include page="${content}"/>
            </c:if>
        </div>
    </div>
    <div class="footer">
        <jsp:include page="footer.jsp"/>
    </div>
</div>

</html>
