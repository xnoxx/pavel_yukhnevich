<%@page contentType="text/html" pageEncoding="UTF-8" isErrorPage="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css"/>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tag:resource key="jsp.error"/></title>
    </head>
    <body>
        <h3><tag:resource key="jsp.error"/></h3>
        <%--<p><jsp:expression>exception.toString()</jsp:expression></p>--%>
        <form action="controller" method="POST">
            <input type="hidden" name="commandName" value="TO_PAGE"/>
            <input type="hidden" name="forwardPage" value="/index.jsp"/>
            <input type="submit" value="<tag:resource key="jsp.button.to.authorization"/>"/>
        </form>
    </body>
</html>
