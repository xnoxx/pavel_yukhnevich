<%@page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/taglib.tld" prefix="tag"%>
<link rel="stylesheet" type="text/css" href="/Restaurant/css/style.css"/>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tag:resource key="jsp.error"/></title>
    </head>
    <body>
        <h3><tag:resource key="jsp.title.error"/></h3>
        <form action="controller" method="POST">
            <input type="hidden" name="commandName" value="TO_PAGE"/>
            <input type="hidden" name="forwardPage" value="<tag:resource key="forward.authorization"/>"/>
            <input type="submit" value="<tag:resource key="jsp.button.to.authorization"/>"/>
        </form>
    </body>
</html>
