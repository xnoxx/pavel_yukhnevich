<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<tiles:insertDefinition name="base-template">
    <tiles:putAttribute name="body">
        <div class="auth">
            <form method="POST" action="<c:url value="/secure/j_spring_security_check" />">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

                <div class="form">
                    <div class="field">
                        <spring:message code="body.login"/>
                        <input type="text" name="username" pattern="<spring:message code="validation.login.pattern"/>"
                               title="<spring:message code="validation.login.title"/>" required/>
                    </div>
                    <div class="field">
                        <spring:message code="body.password"/>
                        <input type="password" name="password"
                               pattern="<spring:message code="validation.password.pattern"/>"
                               title="<spring:message code="validation.password.title"/>" required/>
                    </div>
                    <button class="btn" type="submit">
                        <spring:message code="body.button"/>
                    </button>
                </div>
            </form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>