<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="<c:url value="/resources/css/addNews.css" />"
	rel="stylesheet">

<tiles:insertDefinition name="base-template">
	<tiles:putAttribute name="body">

			<sf:form method="POST" modelAttribute="newsVO"
				action="/saveNews">
				<div class="add-content">
					<div class="field">
						<div><sf:errors path="news.title" cssClass="error-add" /></div>
						<label for="title"><spring:message code="add-news.title" /></label>
						<sf:input type="text" id="title" class="data" path="news.title" />
						
					</div>
					<div class="field">
						<label for="date"><spring:message code="add-news.date" /></label>
						<fmt:message key="date.format" var="format" />
						<fmt:formatDate value="${newsVO.news.creationDate}"
							pattern="${format}" var="formattedDate" />
						<sf:input name="date" type="text" value="${formattedDate}"
							path="news.creationDate" class="data" readonly="true"/>
					</div>
					<div class="field">
						<div><sf:errors path="news.shortText" cssClass="error-add" /></div>
						<label for="brief"><spring:message code="add-news.brief" /></label>
						<sf:textarea id="brief" class="data" path="news.shortText"></sf:textarea>
						
					</div>
					<div class="field">
						<div><sf:errors path="news.fullText" cssClass="error-add" /></div>
						<label for="content"><spring:message
								code="add-news.content" /></label>
						<sf:textarea id="content" class="data" path="news.fullText"></sf:textarea>
					</div>
					<sf:input type="hidden" path="news.id" />

				</div>
		
			
				<div class="selects">
					<sf:select path="author.id">
						<c:forEach items="${authors}" var="authors">
							<option ${newsVO.author.id == authors.id ? 'selected' : '' } value="${authors.id}"><c:out value = "${authors.name}"/></option>
						</c:forEach>

					</sf:select>
					<select onclick="showCheckboxes()">

					</select>
					<div id="checkboxes" style="display: none">
					
					
						<c:forEach items="${tags}" var="tag">
							<div class="checkbox">

								<input ${newsVO.tagsList.contains(tag) ? 'checked' : '' } type = "checkbox" value = "${tag.id}" name = "tagId[]" />
								<c:out value = "${tag.name}"/>

							</div>
						</c:forEach> 
						
					</div>
				</div>
				<button type="submit"><spring:message code="add-news.save" /></button>

			</sf:form>
		</tiles:putAttribute>
	</tiles:insertDefinition>
<script src="<c:url value= "/resources/js/checkboxes.js"/>"></script>

