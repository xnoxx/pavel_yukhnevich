<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="<c:url value="/resources/css/authors.css" />"
	rel="stylesheet">
<script src="<c:url value= "/resources/js/tagsEditing.js"/>"></script>

<tiles:insertDefinition name="base-template">
	<tiles:putAttribute name="body">

			<div class="author-content">
				<input type="hidden" value="${authorForUpdate.id}" />
				<c:forEach items="${authors}" var="author">
					<div class="author">
						<c:choose>
							<c:when test="${author.id == authorForUpdate.id}">

								<sf:form method="POST" modelAttribute="authorForUpdate"
									action="/updateAuthor">
									<div><sf:errors path="name" cssClass="error" /></div>
									<label for="author"><spring:message code="author.label" /></label>
									
									<sf:input name="author" type="text" value="${author.name}"
										path="name" size="60" />
									
									<sf:hidden path="id" />
									<button type="submit" value="update" class="button"
										onclick="return confirm_alert('<spring:message
										code="alert.confirmation"/>');">
										<spring:message code="tag.update" />
									</button>
									
								</sf:form>
								<div class="links">
									<a href="/deleteAuthor?id=${author.id}"
										onclick="return confirm_alert('<spring:message
										code="alert.confirmation"/>');"><spring:message
											code="author.expire" /></a> <a href="/authors"><spring:message
											code="tag.cancel" /></a>
								</div>
							</c:when>
							<c:otherwise>
								<label for="author"><spring:message code="author.label" /></label>
								<input id="${author.id}" type="text"
									value="${author.name}" size=60 disabled="disabled" />
								<a href="/editAuthor/${author.id}"
									class="edit-link"><spring:message code="tag.edit" /></a>
							</c:otherwise>
						</c:choose>

					</div>
				</c:forEach>
				<div class = "add-author">
					<sf:form method="POST" modelAttribute="authorToAdd" action = "/addAuthor">
					<div><sf:errors path="name" cssClass="error-add" /></div>
						<label for="add"><spring:message code="author.add" /></label>
						<sf:input type="text" id="add" size="60" path="name" value = ""/>
						
						
						<sf:hidden path="id" />
						<button type="submit" value="addAuthor" class="button"
										onclick="return confirm_alert('<spring:message
										code="alert.confirmation"/>');"><spring:message
										code="tag.save" /></button>
					</sf:form>
					
					
					
				</div>
			</div>




		</tiles:putAttribute>
	</tiles:insertDefinition>