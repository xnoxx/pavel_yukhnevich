<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/newsList"><spring:message code="menu.list"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/addNews"><spring:message code="menu.addnews"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/authors"><spring:message code="menu.authors"/></a></p>
<p><img src = "<c:url value="/resources/images/arrow.png" />">  <a href="/tags"><spring:message code="menu.tags"/></a></p>
