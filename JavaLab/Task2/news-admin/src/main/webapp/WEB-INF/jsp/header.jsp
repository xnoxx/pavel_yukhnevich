<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<div class="header-content">
    <security:authorize access="isAuthenticated()">
        <div class="logout">
            <div class="hello"><spring:message code="header.hello"/>
                <c:out value="${pageContext.request.userPrincipal.name}"/>
            </div>
            <sf:form action="/logout" method="post">
                <div class="logout-btn">
                    <input type="submit" value="<spring:message code="header.logout" />"/>
                </div>
            </sf:form>
        </div>
    </security:authorize>
    <h2>
        <spring:message code="header.label"/>
    </h2>

    <div class="languages">
        <a href="?lang=en"><spring:message code="header.language.en"/></a> |
        <a href="?lang=ru"><spring:message code="header.language.ru"/></a>
    </div>
</div>