package by.epam.newsmanagement.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.services.CommentService;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;
import by.epam.newsmanagement.controller.util.Navigator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class SingleNewsController {

	private CommentService commentService;
	private NewsService newsService;

	@Inject
	public SingleNewsController(NewsService newsService, CommentService commentService) {
		this.commentService = commentService;
		this.newsService = newsService;
	}

	@RequestMapping(value = { "/postComment" }, method = RequestMethod.POST)
	public String postComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "text") String commentText) throws SQLException, GeneralServiceException {
		String text = commentText.replaceAll("\\s+", "");
		if (text.length() > 1 && commentText != null && commentText.length() < 100) {
			Comment comment = new Comment();
			Date today = new Date();
			comment.setCreationDate(new Timestamp(today.getTime()));
			comment.setText(commentText);
			comment.setNewsId(newsId);
			commentService.create(comment);
		}
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/deleteComment" }, method = RequestMethod.POST)
	public String deleteComment(@RequestParam(value = "newsId") Long newsId, @RequestParam(value = "id") Long commentId) throws GeneralServiceException {
		commentService.delete(commentId);
		return "redirect:/singleNews/" + newsId;
	}

	@RequestMapping(value = { "/next" }, method = RequestMethod.GET)
	public String showNextNews(Long newsId, HttpSession session) throws GeneralServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCount();
		List<NewsVO> newsVOList = newsService.getListOfNewsVO(sc, 0, newsCount);
		NewsVO nextNews = navigator.getNextNewsVO(newsVOList, newsId);
		Long nextNewsId = nextNews.getNews().getId();
		return "redirect:/singleNews/" + nextNewsId;
	}

	@RequestMapping(value = { "/previous" }, method = RequestMethod.GET)
	public String showPreviousNews(Long newsId, HttpSession session) throws GeneralServiceException {
		Navigator navigator = new Navigator();
		SearchCriteria sc = (SearchCriteria) session.getAttribute("searchCtiteria");
		int newsCount = newsService.getNewsCount();
		List<NewsVO> newsVOList = newsService.getListOfNewsVO(sc, 0, newsCount);
		NewsVO previousNews = navigator.getPreviousNewsVO(newsVOList, newsId);
		Long previousNewsId = previousNews.getNews().getId();
		return "redirect:/singleNews/" + previousNewsId;
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
