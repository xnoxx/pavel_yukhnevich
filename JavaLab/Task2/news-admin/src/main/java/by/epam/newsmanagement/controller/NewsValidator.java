package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.NewsVO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class NewsValidator implements Validator {

	public boolean supports(Class<?> type) {
		return NewsVO.class.isAssignableFrom(type);
	}

	public void validate(Object arg0, Errors arg1) {
		NewsVO newsVO = (NewsVO) arg0;
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.title", "validation.title.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.shortText", "validation.shortText.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(arg1, "news.fullText", "validation.fullText.empty");
		String title = newsVO.getNews().getTitle();
		if ((title.length()) > 30) {
			arg1.rejectValue("news.title", "validation.title.large");
		}
		String shortText = newsVO.getNews().getShortText();
		if ((shortText.length()) > 100) {
			arg1.rejectValue("news.shortText", "validation.shortText.large");
		}
		String fullText = newsVO.getNews().getFullText();
		if ((fullText.length()) > 2000) {
			arg1.rejectValue("news.fullText", "validation.fullText.large");
		}

	}

}
