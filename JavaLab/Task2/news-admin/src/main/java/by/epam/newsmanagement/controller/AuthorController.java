package by.epam.newsmanagement.controller;

import java.sql.SQLException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.GeneralServiceException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class AuthorController {

	private AuthorService authorService;

	@Inject
	public AuthorController(AuthorService authorService) {
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/editAuthor/{id}" }, method = RequestMethod.GET)
	public String editAuthor(@PathVariable("id") Long authorId, Model model) throws GeneralServiceException, SQLException {
		Author authorForUpdate = authorService.getById(authorId);
		model.addAttribute("authorForUpdate", authorForUpdate);
		List<Author> authorList = authorService.getAllAuthors();
		model.addAttribute("authors", authorList);
		Author author = new Author();
		model.addAttribute("authorToAdd", author);
		return "authors";
	}

	@RequestMapping(value = { "/deleteAuthor" }, method = RequestMethod.GET)
	public String deleteAuthor(@RequestParam(value = "id") Long authorId) throws GeneralServiceException {
		authorService.setExpired(authorId);
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.POST)
	public ModelAndView updateAuthor(@Valid @ModelAttribute("authorForUpdate") Author author, BindingResult result) throws GeneralServiceException {
		if (!result.hasErrors()) {
			authorService.update(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;

		} else {
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModelUpdate();
		}
	}

	@RequestMapping(value = { "/updateAuthor" }, method = RequestMethod.GET)
	public String languageUpdate() throws GeneralServiceException {
		return "redirect:/authors";
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.POST)
	public ModelAndView addAuthor(@Valid @ModelAttribute("authorToAdd") Author author, BindingResult result) throws GeneralServiceException, SQLException {

		if (!result.hasErrors()) {
			authorService.create(author);
			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("redirect:/authors");
			return modelAndView;
		} else {

			ModelAndView modelAndView = new ModelAndView();
			modelAndView.setViewName("authors");
			return formModel();
		}
	}

	@RequestMapping(value = { "/addAuthor" }, method = RequestMethod.GET)
	public String languageAdd() throws GeneralServiceException {
		return "redirect:/authors";
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

	private ModelAndView formModel() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = null;
		authorList = authorService.getAllAuthors();
		model.addObject("authors", authorList);

		model.setViewName("authors");

		return model;
	}

	private ModelAndView formModelUpdate() throws GeneralServiceException {
		ModelAndView model = new ModelAndView();
		List<Author> authorList = null;
		authorList = authorService.getAllAuthors();
		model.addObject("authors", authorList);
		Author author = new Author();
		model.addObject("authorToAdd", author);
		model.setViewName("authors");

		return model;
	}

}
