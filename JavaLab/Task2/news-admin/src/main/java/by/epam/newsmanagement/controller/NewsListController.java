package by.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.newsmanagement.services.NewsService;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.model.entities.Tag;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.TagService;
import by.epam.newsmanagement.services.GeneralServiceException;

@Controller
public class NewsListController {

    private AuthorService authorService;
    private TagService tagService;
    private NewsService newsService;

    @Inject
    public NewsListController(AuthorService authorService, TagService tagService, NewsService newsService) {
        this.authorService = authorService;
        this.tagService = tagService;
        this.newsService = newsService;
    }

    @RequestMapping(value = {"/editNews/{newsId}"}, method = RequestMethod.GET)
    public String editNews(@PathVariable("newsId") Long newsId, Model model) {

        NewsVO newsVO = null;
        List<Tag> listTag = null;
        List<Author> authorList = null;

        try {
            newsVO = newsService.getSingleNewsVOByNewsId(newsId);
            listTag = tagService.getAllTags();
            authorList = authorService.getAllAuthors();
        } catch (GeneralServiceException e) {
            e.printStackTrace();
        }

        Date today = new Date();

        List<Comment> commentsList = new ArrayList<Comment>();

        model.addAttribute("tags", listTag);

        model.addAttribute("authors", authorList);

        newsVO.setCommentsList(commentsList);

        newsVO.getNews().setModificationDate(today);
        model.addAttribute("newsVO", newsVO);
        return "addNews";
    }

    @RequestMapping(value = {"/filter"}, method = RequestMethod.POST)
    public String filterNews(HttpServletRequest request, @RequestParam(value = "tagId[]", required = false) Long[] tagIds, @RequestParam(value = "author", required = false) Long authorId) {
        List<Tag> tagList = new ArrayList<Tag>();
        if (tagIds != null) {
            for (int i = 0; i < tagIds.length; i++) {
                Tag tag = new Tag();
                Long tagId = Long.parseLong(String.valueOf(tagIds[i]));
                tag.setId(tagId);
                tagList.add(tag);
            }
        }
        SearchCriteria searchCriteria = new SearchCriteria();
        Author author = new Author();
        author.setId(authorId);
        searchCriteria.setAuthor(author);
        searchCriteria.setTagsList(tagList);
        request.getSession().setAttribute("searchCriteria", searchCriteria);
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/reset"}, method = RequestMethod.POST)
    public String reset(HttpSession session) {
        session.setAttribute("searchCriteria", null);
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/deleteNews"}, method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = "newsToDelete[]", required = false) Long[] newsToDeleteArray) {
        if (newsToDeleteArray != null) {
            for (int i = 0; i < newsToDeleteArray.length; i++) {
                long id = newsToDeleteArray[i];
                try {
                    newsService.delete(id);
                } catch (GeneralServiceException e) {
                    e.printStackTrace();
                }
            }
        }
        return "redirect:/newsList";
    }

    @RequestMapping(value = {"/singleNews/{newsId}"}, method = RequestMethod.GET)
    public String showSingleNews(@PathVariable("newsId") Long newsId, Model model) {
        NewsVO newsVO = null;
        try {
            newsVO = newsService.getSingleNewsVOByNewsId(newsId);
        } catch (GeneralServiceException e) {
            e.printStackTrace();
        }
        model.addAttribute("newsVO", newsVO);

        return "singleNews";
    }

    @ExceptionHandler(GeneralServiceException.class)
    public String handleException(HttpServletRequest request, Exception ex) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("exception", ex);
        modelAndView.addObject("url", request.getRequestURL());
        return "error";
    }

}