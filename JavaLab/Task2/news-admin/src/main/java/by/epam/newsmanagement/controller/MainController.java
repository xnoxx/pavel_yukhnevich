package by.epam.newsmanagement.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.*;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;
import by.epam.newsmanagement.services.TagService;
import by.epam.newsmanagement.controller.util.PageBuilder;
import by.epam.newsmanagement.model.entities.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/")
public class MainController {

	private TagService tagService;
	private AuthorService authorService;
	private NewsService newsService;

	@Inject
	public MainController(TagService tagService, AuthorService authorService, NewsService newsService) {
		this.tagService = tagService;
		this.authorService = authorService;
		this.newsService = newsService;

	}

	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String showNewsList(Principal principal) {

		return "login";
	}

	@RequestMapping(value = { "/addNews" }, method = RequestMethod.GET)
	public String showAddNews(Model model) throws GeneralServiceException {
		NewsVO newsVO = new NewsVO();
		News news = new News();
		Date today = new Date();
		Author author = new Author();
		List<Comment> commentsList = new ArrayList<Comment>();
		List<Tag> listTag = tagService.getAllTags();
		model.addAttribute("tags", listTag);

		List<Author> authorList = authorService.getAllAuthorsExceptEspired();
		model.addAttribute("authors", authorList);

		newsVO.setNews(news);
		newsVO.setAuthor(author);
		newsVO.setCommentsList(commentsList);
		newsVO.setTagsList(listTag);
		newsVO.getNews().setCreationDate(today);
		newsVO.getNews().setModificationDate(today);
		model.addAttribute("newsVO", newsVO);

		return "addNews";
	}

	@RequestMapping(value = { "/authors" }, method = RequestMethod.GET)
	public String showAuthors(Model model) throws GeneralServiceException {
		List<Author> authorList = authorService.getAllAuthors();
		model.addAttribute("authors", authorList);
		Author author = new Author();
		model.addAttribute("authorToAdd", author);
		return "authors";
	}

	@RequestMapping(value = { "/tags" }, method = RequestMethod.GET)
	public String showTags(Model model) throws GeneralServiceException {

		List<Tag> tagList = tagService.getAllTags();
		model.addAttribute("tags", tagList);
		Tag tag = new Tag();
		model.addAttribute("tagToAdd", tag);
		return "tags";
	}

	@RequestMapping(value = { "/newsList" }, method = RequestMethod.GET)
	public String showNewsList(@RequestParam(value = "page", defaultValue = "1") int currentPage, Model model, HttpSession session) throws GeneralServiceException {

		PageBuilder pageBuilder = new PageBuilder();
		SearchCriteria sc;
		sc = (SearchCriteria) session.getAttribute("searchCriteria");
		if(sc == null)
			sc = new SearchCriteria();

		List<NewsVO> newsList = newsService.getListOfNewsVO(sc, -1, -1);
		int[] pagesNumbersArray = pageBuilder.getNewsPagesArray(newsList);
		model.addAttribute("pages", pagesNumbersArray);

		int start = pageBuilder.getCurrentStart(currentPage);
		int end = pageBuilder.getCurrentEnd(currentPage);
		newsList = newsService.getListOfNewsVO(sc, start, end);

		model.addAttribute("currentPage", currentPage);
		model.addAttribute("newsList", newsList);

		List<Author> authorList = authorService.getAllAuthors();
		model.addAttribute("authors", authorList);

		List<Tag> tagList = tagService.getAllTags();
		model.addAttribute("tags", tagList);

		return "newsList";
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
