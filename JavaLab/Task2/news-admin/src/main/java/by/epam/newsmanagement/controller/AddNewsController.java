package by.epam.newsmanagement.controller;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;
import by.epam.newsmanagement.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AddNewsController {

	private NewsService newsService;
	private TagService tagService;
	private AuthorService authorService;
	@Autowired
	NewsValidator validator;

	@Inject
	public AddNewsController(NewsService newsService, TagService tagService, AuthorService authorService) {
		this.newsService = newsService;
		this.tagService = tagService;
		this.authorService = authorService;
	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.POST)
		public String saveNews(@ModelAttribute("newsVO") NewsVO newsVO, Locale locale, @RequestParam(value = "tagId[]", required = false) Long[] tagIdArray, BindingResult result, Model model)
			throws GeneralServiceException, SQLException {
		validator.validate(newsVO, result);
		if (!result.hasErrors()) {
			Long newsId = newsVO.getNews().getId();
			Date today = new Date();
			newsVO.getNews().setModificationDate(today);
			List<Tag> tagList = new ArrayList<Tag>();
			if (tagIdArray != null) {
				for (int i = 0; i < tagIdArray.length; i++) {
					Tag tag = tagService.getById(tagIdArray[i]);
					tagList.add(tag);
				}
			}
			newsVO.setTagsList(tagList);
			if (newsId == null) {
				newsId = newsService.createByAuthorWithTags(newsVO.getNews(), newsVO.getAuthor().getId(), newsVO.getTagsList());
			} else {
				newsService.editNewsVO(newsVO);
			}

			return "redirect:/singleNews/" + newsId;
		} else {
			List<Tag> listTag = tagService.getAllTags();
			model.addAttribute("tags", listTag);

			List<Author> authorList = authorService.getAllAuthorsExceptEspired();
			model.addAttribute("authors", authorList);
			model.addAttribute("newsVO", newsVO);
			return "addNews";
		}

	}

	@RequestMapping(value = { "/saveNews" }, method = RequestMethod.GET)

	public String language(Model model) throws GeneralServiceException {
		return "redirect:/addNews";
	}

	@ExceptionHandler(GeneralServiceException.class)
	public String handleException(HttpServletRequest request, Exception ex) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("exception", ex);
		modelAndView.addObject("url", request.getRequestURL());
		return "error";
	}

}
