package test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class NewsCRUDTest {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8888/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testNewsCRUD() throws Exception {
        driver.get(baseUrl + "/");
        driver.findElement(By.linkText("Add News")).click();
        new Select(driver.findElement(By.xpath("//form[@id='news']/div/div/select"))).selectByVisibleText("Vasia Pupkin");
        driver.findElement(By.xpath("//select[@onclick='showCheckboxes()']")).click();
        // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
        // ERROR: Caught exception [Error: Dom locators are not implemented yet!]
        driver.findElement(By.xpath("//select[@onclick='showCheckboxes()']")).click();
        driver.findElement(By.id("title")).clear();
        driver.findElement(By.id("title")).sendKeys("qwerty");
        driver.findElement(By.id("brief")).clear();
        driver.findElement(By.id("brief")).sendKeys("qwerty");
        driver.findElement(By.id("content")).clear();
        driver.findElement(By.id("content")).sendKeys("qwerty");
        driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();
        assertEquals("News created successful", closeAlertAndGetItsText());
        driver.findElement(By.linkText("News List")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Vasia Pupkin");
        driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();
        driver.findElement(By.linkText("Edit")).click();
        driver.findElement(By.id("title")).clear();
        driver.findElement(By.id("title")).sendKeys("qwerty123");
        driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();
        driver.findElement(By.linkText("News List")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
        new Select(driver.findElement(By.xpath("//select"))).selectByVisibleText("Vasia Pupkin");
        driver.findElement(By.cssSelector("button[type=\"submit\"]")).click();
        driver.findElement(By.linkText("Delete")).click();
        driver.findElement(By.xpath("(//button[@type='submit'])[2]")).click();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
