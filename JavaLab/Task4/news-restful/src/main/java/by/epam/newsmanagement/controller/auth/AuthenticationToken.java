package by.epam.newsmanagement.controller.auth;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import java.util.Collection;

public class AuthenticationToken extends AbstractAuthenticationToken {
    private static final long serialVersionUID = 1L;
    private final Object principal;
    private Object details;

    Collection authorities;

    public AuthenticationToken(String jwtToken) {
        super(null);
        super.setAuthenticated(true); // must use super, as we override
        ParserStub parser = new ParserStub(jwtToken);

        this.principal = parser.getSub();

       // this.setDetailsAuthorities();

    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    private void setDetailsAuthorities() {
        String username = principal.toString();
        UserDetailsAdapter adapter = new UserDetailsAdapter(username);
        details = adapter;
        authorities = (Collection) adapter.getAuthorities();

    }

    @Override
    public Collection getAuthorities() {
        return authorities;
    }
}