package by.epam.newsmanagement.controller.auth;

public class ParserStub {
    private String sub;

    public ParserStub(String jwtToken) {
        this.sub = jwtToken;
    }

    public String getSub() {
        return sub;
    }
}
