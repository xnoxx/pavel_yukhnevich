package by.epam.newsmanagement.controller.exception;

import by.epam.newsmanagement.exceptions.GeneralServiceException;
import by.epam.newsmanagement.exceptions.ResourceNotFoundException;
import by.epam.newsmanagement.model.entities.ExceptionMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class ExceptionHandlerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerAdvice.class);
    private static final String MSG_VALIDATION_ERROR = "error.validationError";
    private static final String MSG_NOT_FOUND_ERROR = "error.notFound";
    private static final String MSG_NO_HANDLER_ERROR = "error.noHandler";
    private static final String MSG_METHOD_NOT_SUPPORTED_ERROR = "error.methodNotSupport";
    private static final String MSG_OTHER_ERROR = "error.other";
    private static final String MSG_PARAM_ERROR = "error.param";
    private static final String MSG_NOT_READABLE_ERROR = "error.httpNotReadable";

    @Autowired
    private MessageSource messageSource;

    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    protected @ResponseBody ResponseEntity<Object> handleInvalidRequest(MethodArgumentNotValidException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_VALIDATION_ERROR), HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = AuthenticationException.class)
    protected @ResponseBody ResponseEntity<Object> handleUnauthorizedRequest(AuthenticationException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_VALIDATION_ERROR), HttpStatus.UNAUTHORIZED);
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = MethodArgumentTypeMismatchException.class)
    public @ResponseBody ResponseEntity<Object> argumentTypeMismatchExceptionHandler(MethodArgumentTypeMismatchException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_PARAM_ERROR), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = ResourceNotFoundException.class)
    protected @ResponseBody ResponseEntity<Object> newsNotFoundHandler(ResourceNotFoundException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_NOT_FOUND_ERROR), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = NoHandlerFoundException.class)
    protected @ResponseBody ResponseEntity<Object> noHandlerExceptionHandler(NoHandlerFoundException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_NO_HANDLER_ERROR), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    protected @ResponseBody ResponseEntity<Object> methodNotSupportExceptionHandler(HttpRequestMethodNotSupportedException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_METHOD_NOT_SUPPORTED_ERROR), HttpStatus.NOT_FOUND);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = GeneralServiceException.class)
    protected @ResponseBody ResponseEntity<Object> serviceExceptionHandler(GeneralServiceException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_OTHER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public @ResponseBody ResponseEntity<Object> invalidFormatHandler(HttpMessageNotReadableException exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_NOT_READABLE_ERROR),HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = Exception.class)
    protected @ResponseBody ResponseEntity<Object> generalExceptionHandler(Exception exception) {
        return new ResponseEntity<Object>(handleException(exception, MSG_OTHER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ExceptionMessage handleException(Exception exception, String message) {
        LOGGER.error(exception.getMessage(), exception);
        ExceptionMessage exceptionMessage = new ExceptionMessage();
        exceptionMessage.setMessage(messageSource.getMessage(message, null, LocaleContextHolder.getLocale()));
        exceptionMessage.setErrorDetails(exception.getMessage());
        return exceptionMessage;
    }
}