package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.controller.auth.LinkedInAuth;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private static final String PATH_TOKEN = "/token";
    private static final String HEADER_AUTHORIZATION_CODE = "X-Authorization-Code";


    @RequestMapping(method = RequestMethod.GET, value = PATH_TOKEN)
    public
    @ResponseBody
    String login(HttpServletRequest request) throws IOException {
        String code = request.getHeader(HEADER_AUTHORIZATION_CODE);
        String result = "";
        if (code != null) {
            LinkedInAuth linkedInAuth = new LinkedInAuth();
            result = linkedInAuth.getTokenByAuthorizationCode(code);
        }
        return result;
    }

    @RequestMapping(method = RequestMethod.GET)
    public void authStub() {
    }


}
