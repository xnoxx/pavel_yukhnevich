package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.model.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/tags")
public class TagController {

    private static final String PATH_TAGS_ID = "/{tagId}";
    private static final String HEADER_LOCATION = "Location";
    private static final String PATH_TAGS = "/tags/";

    private TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @RequestMapping(method = RequestMethod.GET, value = PATH_TAGS_ID)
    public Tag getTag(@PathVariable Long tagId) {
        return tagService.read(tagId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Tag> getAllTags() {
        return tagService.getAll();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody Tag postTag(@Valid @RequestBody Tag tag, HttpServletResponse response) {
        Long id = tagService.create(tag);
        tag.setId(id);
        response.setHeader(HEADER_LOCATION, PATH_TAGS + id);
        return tag;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.PUT, value = PATH_TAGS_ID)
    public void putTag(@Valid @RequestBody Tag tag, @PathVariable long tagId) {
        tag.setId(tagId);
        tagService.update(tag);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.DELETE, value = PATH_TAGS_ID)
    public void deleteTag(@PathVariable Long tagId) {
        tagService.delete(tagId);
    }

}
