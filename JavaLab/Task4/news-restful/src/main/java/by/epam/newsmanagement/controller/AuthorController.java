package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.services.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/authors")
public class AuthorController {

    private static final String PATH_AUTHORS = "/authors/";
    private static final String PATH_AUTHOR_ID = "/{authorId}";
    private static final String HEADER_LOCATION = "Location";
    private static final String PARAM_EXPIRED = "unexpired";

    private AuthorService authorService;

    @Autowired
    public AuthorController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @RequestMapping(method = RequestMethod.GET, value = PATH_AUTHOR_ID)
    public Author getAuthor(@PathVariable Long authorId) {
        return authorService.read(authorId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Author> getAllAuthors(@RequestParam(value = PARAM_EXPIRED, required = false) Boolean unexpired) {
        List<Author> authors;
        if(unexpired != null && unexpired){
            authors = authorService.getAllExceptExpired();
        } else {
            authors = authorService.getAll();
        }
        return authors;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody Author postAuthor(@Valid @RequestBody Author author, HttpServletResponse response) {
        Long id = authorService.create(author);
        author.setId(id);
        response.setHeader(HEADER_LOCATION, PATH_AUTHORS + id);
        return author;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.PUT, value = PATH_AUTHOR_ID)
    public void putAuthor(@Valid @RequestBody Author author, @PathVariable long authorId) {
        author.setId(authorId);
        authorService.update(author);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.DELETE, value = PATH_AUTHOR_ID)
    public void expireAuthor(@PathVariable Long authorId) {
        authorService.setExpired(authorId);
    }

}
