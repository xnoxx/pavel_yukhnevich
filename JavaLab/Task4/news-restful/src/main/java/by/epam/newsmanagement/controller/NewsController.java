package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.SearchCriteria;
import by.epam.newsmanagement.model.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/news")
public class NewsController {

    private static final String PATH_NEWS = "/news/";
    private static final String PATH_NEWS_ID = "/{newsId}";
    private static final String PATH_SEARCH_CRITERIA = "/search-criteria";
    private static final String PARAM_SEARCH_CRITERIA = "search-criteria";
    private static final String PARAM_PAGE = "page";
    private static final String PARAM_AUTHOR = "author";
    private static final String PARAM_TAG = "tag";
    private static final String PARAM_NEWS_PER_PAGE = "news-per-page";
    private static final String HEADER_LOCATION = "Location";
    private static final int DEFAULT_NEWS_PER_PAGE = 3;
    private static final String PATH_PAGES_COUNT = "pages-count";

    private NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(method = RequestMethod.GET, value = PATH_NEWS_ID)
    public News getSingleNews(@PathVariable Long newsId) {
        return newsService.read(newsId);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<News> getAllNews(HttpSession session,
                                 @RequestParam(value = PARAM_PAGE, defaultValue = "1") int currentPage,
                                 @RequestParam(value = PARAM_AUTHOR, required = false) Long authorId,
                                 @RequestParam(value = PARAM_TAG, required = false) Set<Long> tagIds) {
        SearchCriteria sc = getSearchCriteria(session);
        if (authorId != null || tagIds != null) {
            sc.setAuthorId(authorId);
            sc.setTagIdsSet(tagIds);
        }
        session.setAttribute(PARAM_SEARCH_CRITERIA, sc);
        int newsPerPage = getNewsPerPage(session);
        return newsService.getAll(sc, currentPage, newsPerPage);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    News postNews(@Valid @RequestBody News news, HttpServletResponse response) {
        news.setModificationDate(new Date());
        Long newsId = newsService.create(news);
        response.setHeader(HEADER_LOCATION, PATH_NEWS + newsId);
        return news;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.PUT, value = PATH_NEWS_ID)
    public
    @ResponseBody
    void putNews(@Valid @RequestBody News news, @PathVariable Long newsId) {
        news.setId(newsId);
        news.setModificationDate(new Date());
        newsService.update(news);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.DELETE, value = PATH_NEWS_ID)
    public void deleteNews(@PathVariable Long newsId) {
        newsService.delete(newsId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.PUT, value = PATH_SEARCH_CRITERIA)
    public void resetSearchCriteria(HttpSession session) {
        session.setAttribute(PARAM_SEARCH_CRITERIA, null);
    }

    @RequestMapping(method = RequestMethod.GET, value = PATH_PAGES_COUNT)
    public String getPagesCount(HttpSession session,
                                @RequestParam(value = PARAM_AUTHOR, required = false) Long authorId,
                                @RequestParam(value = PARAM_TAG, required = false) Set<Long> tagIds) {
        SearchCriteria sc = getSearchCriteria(session);
        if (authorId != null || tagIds != null) {
            sc.setAuthorId(authorId);
            sc.setTagIdsSet(tagIds);
        }
        session.setAttribute(PARAM_SEARCH_CRITERIA, sc);
        int newsPerPage = getNewsPerPage(session);
        return intValueToJson(newsService.getPagesCount(newsPerPage, sc));
    }

    private Integer getNewsPerPage(HttpSession session) {
        Integer newsPerPage = (Integer) session.getAttribute(PARAM_NEWS_PER_PAGE);
        if (newsPerPage == null) {
            newsPerPage = DEFAULT_NEWS_PER_PAGE;
        }
        return newsPerPage;
    }

    private SearchCriteria getSearchCriteria(HttpSession session) {
        SearchCriteria sc = (SearchCriteria) session.getAttribute(PARAM_SEARCH_CRITERIA);
        if (sc == null) {
            sc = new SearchCriteria();
        }
        return sc;
    }

    private String intValueToJson(Integer value) {
        return "{\"value\":" + value + "}";
    }

}