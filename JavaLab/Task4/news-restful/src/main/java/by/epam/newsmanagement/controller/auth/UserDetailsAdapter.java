package by.epam.newsmanagement.controller.auth;

public class UserDetailsAdapter {
    private Object authorities;

    public UserDetailsAdapter(String username) {
        this.authorities = username;
    }

    public Object getAuthorities() {
        return authorities;
    }
}
