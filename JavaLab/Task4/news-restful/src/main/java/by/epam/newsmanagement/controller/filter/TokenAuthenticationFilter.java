package by.epam.newsmanagement.controller.filter;

import by.epam.newsmanagement.controller.auth.AuthenticationToken;
import by.epam.newsmanagement.controller.auth.NoOpAuthenticationManager;
import by.epam.newsmanagement.controller.auth.TokenUrlAuthenticationSuccessHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.MessageFormat;

public class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    public TokenAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
        super.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(defaultFilterProcessesUrl));
        System.out.println(defaultFilterProcessesUrl);
        setAuthenticationManager(new NoOpAuthenticationManager());
        setAuthenticationSuccessHandler(new TokenUrlAuthenticationSuccessHandler());
    }

    public final String HEADER_SECURITY_TOKEN = "X-Access-Token";
    public final String HEADER_SECURITY_CODE = "X-Authorization-Code";

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {

        String token = request.getHeader(HEADER_SECURITY_TOKEN);
        logger.info("token found:" + token);

        String code = request.getHeader(HEADER_SECURITY_CODE);
        logger.info("code found:" + code);

        AbstractAuthenticationToken userAuthenticationToken = authUserByToken(token);
        if (userAuthenticationToken == null) {
            userAuthenticationToken = authUserByToken(code);
            if (userAuthenticationToken == null) {
                throw new AuthenticationServiceException(MessageFormat.format("Error | {0}", "Bad Token"));
            }
        }

        return userAuthenticationToken;
    }

    private AbstractAuthenticationToken authUserByToken(String token) {

        if (token == null) {
            return null;
        }

        AbstractAuthenticationToken authToken = new AuthenticationToken(token);

        try {
            return authToken;
        } catch (Exception e) {
            logger.error("Authenticate user by token error: ", e);
        }
        return authToken;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        super.doFilter(req, res, chain);
    }
}