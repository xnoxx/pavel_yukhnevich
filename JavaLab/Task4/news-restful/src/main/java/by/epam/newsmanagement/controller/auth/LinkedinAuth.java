package by.epam.newsmanagement.controller.auth;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class LinkedInAuth {

    private static final String URI_LINKEDIN_ACCESS_TOKEN = "https://www.linkedin.com/uas/oauth2/accessToken";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final String HEADER_CONTENT_TYPE_VALUE = "application/x-www-form-urlencoded";
    private static final String BODY_PARAM_REDIRECT_URI_VALUE = "http%3A%2F%2F10.6.102.66%3A8888%2Fcode";
    private static final String BODY_PARAM_CLIENT_ID_VALUE = "777gcr4o3n7zvk";
    private static final String BODY_PARAM_CLIENT_SECRET_VALUE = "TrWLsY1NmHRCpMUx";
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());


    public String getTokenByAuthorizationCode(String code) throws IOException {
        HttpClient client = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(URI_LINKEDIN_ACCESS_TOKEN);
        httpPost.setHeader(HEADER_CONTENT_TYPE, HEADER_CONTENT_TYPE_VALUE);
        String requestBody = buildBody(code);
        logger.info(requestBody);
        HttpEntity entity = new ByteArrayEntity(requestBody.getBytes());
        httpPost.setEntity(entity);
        HttpResponse response = client.execute(httpPost);
        return EntityUtils.toString(response.getEntity());
    }

    private String buildBody(String code) {
        return "grant_type=authorization_code&code=" + code
                + "&redirect_uri=" + BODY_PARAM_REDIRECT_URI_VALUE
                + "&client_id=" + BODY_PARAM_CLIENT_ID_VALUE
                + "&client_secret=" + BODY_PARAM_CLIENT_SECRET_VALUE;
    }
}
