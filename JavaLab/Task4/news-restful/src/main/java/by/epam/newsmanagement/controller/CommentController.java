package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.services.CommentService;
import by.epam.newsmanagement.model.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/news")
public class CommentController {

    private static final String PATH_COMMENTS = "/{newsId}/comments";
    private static final String PATH_COMMENTS_ID = "/{newsId}/comments/{commentId}";

    private NewsService newsService;
    private by.epam.newsmanagement.model.services.CommentService commentService;

    @Autowired
    public CommentController(NewsService newsService, CommentService commentService) {
        this.newsService = newsService;
        this.commentService = commentService;
    }

    @RequestMapping(method = RequestMethod.GET, value = PATH_COMMENTS)
    public List<Comment> getAllComments(@PathVariable Long newsId) {
        return newsService.read(newsId).getCommentsList();
    }

    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(method = RequestMethod.POST, value = PATH_COMMENTS)
    public @ResponseBody
    Comment postComment(@Valid @RequestBody Comment comment, @PathVariable Long newsId) {
        comment.setCreationDate(new Timestamp(new Date().getTime()));
        News news = new News();
        news.setId(newsId);
        comment.setNews(news);
        commentService.create(comment);
        return comment;
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @RequestMapping(method = RequestMethod.DELETE, value = PATH_COMMENTS_ID)
    public void deleteComment(@PathVariable Long newsId, @PathVariable Long commentId) {
        commentService.delete(commentId);
    }
}
