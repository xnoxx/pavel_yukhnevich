var CLIENT_ID_PARAM = "777gcr4o3n7zvk";
var REDIRECT_URI_PARAM = "http://10.6.102.66:8888/code";
var STATE_PARAM = "DCEeFWf45A53sdfKef424";
var RESPONSE_TYPE_PARAM = "code";

var goToLinkedInAuthorization = function () {
    var url = "https://www.linkedin.com/uas/oauth2/authorization?"
        + "client_id=" + CLIENT_ID_PARAM
        + "&redirect_uri=" + encodeURIComponent(REDIRECT_URI_PARAM)
        + "&state=" + STATE_PARAM
        + "&response_type=" + RESPONSE_TYPE_PARAM;
    window.location.replace(url);
};