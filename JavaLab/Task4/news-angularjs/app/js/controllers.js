'use strict';
var controllers = angular.module('controllers', []);

controllers.controller('NewsListCtrl', ['$scope', '$rootScope', '$http', '$route', '$location', '$cookies', 'News', 'Author', 'Tag', 'Pages',
    function ($scope, $rootScope, $http, $route, $location, $cookies, News, Author, Tag, Pages) {

        if ($route.current.params.page === undefined || $rootScope.newsList === undefined) {
            loadNewsList();
        }

        $scope.authors = Author.query();
        $scope.tags = Tag.query();
        $rootScope.selectedTagIds = [];
        $scope.loadNewsList = loadNewsList;

        function loadNewsList(page) {

            if ($scope.selectedAuthor !== undefined) {
                $rootScope.authorId = $scope.selectedAuthor.id;
            }

            News.query({author: $rootScope.authorId, tag: $rootScope.selectedTagIds, page: page}, function (data) {
                $rootScope.newsList = data;
                if (page !== undefined) {
                    $location.path('/news-list/page/' + page);
                }
            });

            Pages.get({author: $rootScope.authorId, tag: $rootScope.selectedTagIds}, function (pagesCount) {
                var pagesArray = [];
                for (var i = 0; i < pagesCount.value; i++) {
                    pagesArray[i] = i + 1;
                }
                $rootScope.pages = pagesArray;
            });
        }

        $rootScope.toggleSelection = function toggleSelection(tagId) {
            var idx = $rootScope.selectedTagIds.indexOf(tagId);
            if (idx > -1) {
                $rootScope.selectedTagIds.splice(idx, 1);
            } else {
                $rootScope.selectedTagIds.push(tagId);
            }
        };

        $scope.remove = function remove(id) {
            News.delete({id: id}, function () {
                loadNewsList();
            });
        };

        $scope.reset = function reset() {
            $rootScope.selectedTagIds = [];
            $scope.selectedAuthor = undefined;
            $rootScope.authorId = null;
            loadNewsList();
        };

    }]);

controllers.controller('PostNewsCtrl', ['$scope', '$rootScope', '$routeParams', 'News', 'Tag', 'Author',
    function ($scope, $rootScope, $routeParams, News, Tag, Author) {
        $scope.authors = Author.query();
        $scope.tags = Tag.query();
        $scope.news = {};
        $rootScope.selectedTagIds = [];

        if ($routeParams.id !== undefined) {
            News.get({id: $routeParams.id}, function (data) {
                $scope.news = data;
                $scope.selectedAuthor = data.author;
                for (var i = 0; i < data.tagsSet.length; i++) {
                    $rootScope.selectedTagIds[i] = data.tagsSet[i].id;
                }
            });
        }

        this.saveNews = function saveNews() {
            if ($scope.selectedAuthor !== undefined) {
                $scope.news.author = {id: ""};
                $scope.news.author.id = $scope.selectedAuthor.id;
            }

            $scope.news.tagsSet = [];

            for (var i = 0; i < $rootScope.selectedTagIds.length; i++) {
                $scope.news.tagsSet[i] = {id: ""};
                $scope.news.tagsSet[i].id = $rootScope.selectedTagIds[i];
            }

            $rootScope.selectedTagIds = [];
            if ($scope.news.id === undefined) {
                News.save($scope.news).$promise.then(
                    function (value) {
                        alert('News created successful')
                    }, function (error) {
                        alert('An error occurred.')
                    }
                );
            } else {
                News.update({id: $routeParams.id}, $scope.news).$promise.then(
                    function (value) {
                        alert('News updated successful')
                    }, function (error) {
                        alert('An error occurred.')
                    });
            }
        };

    }]);

controllers.controller('AuthorCtrl', ['$scope', '$rootScope', '$routeParams', 'Author',
    function ($scope, $rootScope, $routeParams, Author) {
        $scope.authors = Author.unexpired();

        this.saveAuthor = function saveAuthor() {
            Author.save({name: $scope.name}).$promise.then(
                function (value) {
                    $scope.authors = Author.unexpired();
                }, function (error) {
                    alert('An error occurred.')
                }
            );
        };

        $scope.choosenId = 0;
        $scope.isNotChoosen = function isNotChoosen(id) {
            return $scope.choosenId !== id;
        };
        $scope.choose = function choose(id) {
            $scope.choosenId = id;
        };

        $scope.putAuthor = function putAuthor(author) {
            if (author.name.match(PATTERN_NAME)[0].length == author.name.length) {
                Author.update({id: author.id}, author).$promise.then(
                    function (value) {
                        $scope.authors = Author.unexpired();
                    }, function (error) {
                        alert('An error occurred.')
                    });
            } else {
                alert("Please enter the correct name");
            }
        };

        $scope.expireAuthor = function expireAuthor(id) {
            Author.delete({id: id}, function () {
                $scope.authors = Author.unexpired();
            });
        };

    }]);

controllers.controller('TagCtrl', ['$scope', '$rootScope', '$routeParams', 'Tag',
    function ($scope, $rootScope, $routeParams, Tag) {
        $scope.tags = Tag.query();

        this.saveTag = function saveTag() {
            Tag.save({name: $scope.name}).$promise.then(
                function (value) {
                    $scope.tags = Tag.query();
                }, function (error) {
                    alert('An error occurred.')
                }
            );
        };

        $scope.choosenId = 0;
        $scope.isNotChoosen = function isNotChoosen(id) {
            return $scope.choosenId !== id;
        };
        $scope.choose = function choose(id) {
            $scope.choosenId = id;
        };

        $scope.putTag = function putTag(tag) {
            if (tag.name.match(PATTERN_NAME)[0].length == tag.name.length) {
                Tag.update({id: tag.id}, tag).$promise.then(
                    function (value) {
                        $scope.tags = Tag.query();
                    }, function (error) {
                        alert('An error occurred.')
                    });
            } else {
                alert("Please enter the correct name");
            }
        };

        $scope.deleteTag = function deleteTag(id) {
            Tag.delete({id: id}, function () {
                $scope.tags = Tag.query();
            });
        };

    }]);

controllers.controller('SingleNewsCtrl', ['$scope', '$routeParams', 'News', 'Comment',
    function ($scope, $routeParams, News, Comment) {
        loadSingleNews();

        function loadSingleNews() {
            News.get({id: $routeParams.id}, function (data) {
                $scope.news = data;
            });
        }

        $scope.deleteComment = function deleteComment(newsId, id) {
            Comment.delete({id: id, newsId: newsId}, function () {
                loadSingleNews();
            });
        };

        $scope.postComment = function postComment(data) {
            Comment.save({newsId: $scope.news.id}, {text: data}, function () {
                loadSingleNews();
            });
        };

    }]);

controllers.controller('LoginCtrl', function ($scope, $cookies) {
    $scope.login = goToLinkedInAuthorization;
    $scope.logout = function logout() {
        $cookies.remove("accessToken");
    };
    $scope.isNotAuthorize = function isNotAuthorize() {
        return $cookies.get("accessToken") === undefined;
    };
});

controllers.controller('TokenInterceptorCtrl', ['$location', '$http', '$cookies',
    function ($location, $http, $cookies) {
        var url = $location.url();
        var code = url.substring(url.indexOf("=") + 1, url.indexOf("&"));

        $http.get(REST_PATH + 'auth/token', {
            headers: {'X-Authorization-Code': code}
        }).then(function successCallback(response) {
            var token = response.data.access_token;
            $cookies.put("accessToken", token);
            $http.get(REST_PATH + 'auth');
            window.location.replace("/");
        });
    }]);