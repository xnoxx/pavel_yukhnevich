'use strict';
var REST_PATH = "http://10.6.102.66:8082/newsmanagement/";
var PATTERN_NAME = /[A-Za-z0-9_\-,.: %$!?]{3,30}/;

var app = angular.module('app', [
    'ngRoute',
    'ngCookies',
    'controllers',
    'ngResource',
    'services'
]);

app.factory('authInterceptor', ['$cookies', function ($cookies) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            config.headers['Content-Type'] = "application/json";
            config.headers['Accept'] = "application/json";
            config.headers['X-Access-Token'] = $cookies.get("accessToken");
            return config;
        }
    };
}]);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');

    $routeProvider
        .when('/', {
            templateUrl: 'template/news-list.html',
            controller: 'NewsListCtrl'
        })
        .when('/news-list', {
            templateUrl: 'template/news-list.html',
            controller: 'NewsListCtrl'
        })
        .when('/news-list/:id', {
            templateUrl: 'template/single-news.html',
            controller: 'SingleNewsCtrl'
        })
        .when('/login', {
            templateUrl: 'template/login.html',
            controller: 'LoginCtrl'
        })
        .when('/add-news/:id', {
            templateUrl: 'template/manage-news.html',
            controller: 'PostNewsCtrl'
        })
        .when('/add-news/', {
            templateUrl: 'template/manage-news.html',
        })
        .when('/add-authors', {
            templateUrl: 'template/manage-authors.html'
        })
        .when('/add-tags', {
            templateUrl: 'template/manage-tags.html'
        })
        .when('/news-list/page/:page', {
            templateUrl: 'template/news-list.html',
            controller: 'NewsListCtrl'
        })
        .when('/code', {
            templateUrl: 'template/news-list.html',
            controller: 'TokenInterceptorCtrl'
        })
        .otherwise({
            redirectTo: '/news-list'
        });

    $locationProvider.html5Mode(true);
}]);