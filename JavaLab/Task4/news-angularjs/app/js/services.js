'use strict';

var services = angular.module('services', ['ngResource']);

services.factory('News', ['$resource',
    function ($resource) {
        return $resource(REST_PATH + 'news/:id', {
            id: '@newsId'
        }, {
            update: {method: 'PUT'}
        });
    }]);

services.factory('Pages', ['$resource',
    function ($resource) {
        return $resource(REST_PATH + 'news/pages-count/');
    }]);

services.factory('Author', ['$resource',
    function ($resource) {
        return $resource(REST_PATH + 'authors/:id', {
            id: '@authorId'
        }, {
            update: {method: 'PUT'},
            unexpired: {method: 'GET', params: {unexpired: 'true'}, isArray: true}
        });
    }]);

services.factory('Tag', ['$resource',
    function ($resource) {
        return $resource(REST_PATH + 'tags/:id', {
            id: '@tagId'
        }, {
            update: {method: 'PUT'}
        });
    }]);

services.factory('Comment', ['$resource',
    function ($resource) {
        return $resource(REST_PATH + 'news/:newsId/comments/:id', {
            newsId: '@newsId',
            id: '@id'
        });
    }]);