package by.epam.newsmanagement.database.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.newsmanagement.database.dao.impl.NewsDAOImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.impl.NewsServiceImpl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class NewsServiceTest {
	
	/**
     * Mock object, which will be inject into NewsServiceImpl class.
     */
    @Mock
    NewsDAOImpl newsDAO;
  
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    NewsServiceImpl newsService;
    
    @Test
    public void deleteNews() throws Exception {

        Long newsId = 1L;

        newsService.delete(newsId);
        verify(newsDAO, times(1)).deleteById(newsId);
    }   
    
    @Test
    public void updateNews() throws Exception {

        News news = makeNews();

        newsService.edit(news);
        verify(newsDAO, times(1)).update(news);
    }
    
    @Test
    public void getNewsById() throws Exception {

        News news = makeNews();
        Long newsId = news.getId();
        String title = news.getTitle();
        String shortText = news.getShortText();
        String fullText = news.getFullText();
        Date creationDate = news.getCreationDate();
        Date modificationDate = news.getModificationDate();

        when(newsDAO.readById(newsId)).thenReturn(news);
        news = newsService.getById(newsId);

        verify(newsDAO, times(1)).readById(newsId);
        assertEquals(newsId, news.getId());
        assertEquals(title, news.getTitle());
        assertEquals(shortText, news.getShortText());
        assertEquals(fullText, news.getFullText());
        assertEquals(creationDate, news.getCreationDate());
        assertEquals(modificationDate, news.getModificationDate());
    }

    @Test
    @Transactional
    public void createByAuthorWithTags() throws Exception {
    	Tag tag1 = new Tag();
        tag1.setId(1L);
        Tag tag2 = new Tag();
        tag2.setId(2L);
        Long newsId = 0L;

        List<Tag> list = new ArrayList<Tag>(); 
        list.add(tag1);
        list.add(tag2);
        
        News news = makeNews();
        
        Long authorId = 1L;

        newsService.createByAuthorWithTags(news, authorId, list);

        verify(newsDAO, times(1)).create(news);
        verify(newsDAO, times(1)).addToNewsTags(newsId, list);
        verify(newsDAO, times(1)).addToNewsAuthors(newsId, authorId);
        
        
    }
    
    @Test
    public void getListOfNewsSorted() throws Exception {
    	List<News> list = new ArrayList<News>();
		News news1 = new News();
		News news2 = new News();
		News news3 = new News();
		list.add(news1);
		list.add(news2);
		list.add(news3);
		
		when(newsDAO.readNewsSorted()).thenReturn(list);
		List<News> listResult = newsService.getListOfNewsSorted();

        verify(newsDAO, times(1)).readNewsSorted();
        assertEquals(list, listResult);

    }
    
    @Test
    public void getNewsBySearchCriteria() throws Exception {
    	List<News> list = new ArrayList<News>();
		News news = newsDAO.readById(1L);
		list.add(news);
		
		SearchCriteria sc = new SearchCriteria();
		
		Author author = new Author();
		author.setId(1L);
		sc.setAuthor(author);
		
		List<Tag> listOfTags = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tag1.setId(1L);
		listOfTags.add(tag1);
		Tag tag2 = new Tag();
		tag2.setId(2L);
		listOfTags.add(tag2);
		sc.setTagsList(listOfTags);
		
		when(newsDAO.readNewsBySearchCriteria(sc, -1, -1)).thenReturn(list);
		List<News> listResult = newsService.getNewsBySearchCriteria(sc);

        verify(newsDAO, times(1)).readNewsBySearchCriteria(sc, -1, -1);
        assertEquals(list, listResult);
    }
    
    private News makeNews() {
        News news = new News();
        news.setId(1L);
        news.setTitle("Title");
        news.setShortText("Short Text");
        news.setFullText("Full Text");
        news.setCreationDate(new Date());
        news.setModificationDate(new Date());
        return news;
    }
}
