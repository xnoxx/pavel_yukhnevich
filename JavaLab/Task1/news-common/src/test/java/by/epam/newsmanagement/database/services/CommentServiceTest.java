package by.epam.newsmanagement.database.services;

import by.epam.newsmanagement.database.dao.impl.CommentDAOImpl;
import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.services.impl.CommentServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Comment service Mockito test class.
 * Class <code>CommentServiceTest</code> contains Mockito unit tests for all <code>CommentService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class CommentServiceTest {

    /**
     * Mock object, which will be inject into CommentServiceImpl class.
     */
    @Mock
    CommentDAOImpl commentDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    public void createComment() throws Exception {

        Comment comment = makeComment();
        when(commentDAO.create(comment)).thenReturn(1L);

        Long commentId = commentService.create(comment);
        verify(commentDAO, times(1)).create(comment);
        assertEquals(commentId, Long.valueOf(1L));
    }

    @Test
    public void editComment() throws Exception {

        Comment comment = makeComment();

        commentService.edit(comment);
        verify(commentDAO, times(1)).update(comment);
    }

    @Test
    public void deleteComment() throws Exception {
        Long id = 1L;
        commentService.delete(id);
        verify(commentDAO, times(1)).deleteById(id);
    }

    @Test
    public void getCommentById() throws Exception {

        Comment comment = makeComment();
        Long commentId = comment.getId();
        Long newsId = comment.getNewsId();
        String commentText = comment.getText();

        when(commentService.getById(commentId)).thenReturn(comment);
        comment = commentService.getById(commentId);

        verify(commentDAO, times(1)).readById(commentId);
        assertEquals(commentId, comment.getId());
        assertEquals(commentText, comment.getText());
        assertEquals(newsId, comment.getNewsId());
    }

    @Test
    public void getAllCommentsByNews() throws Exception {

    	Long newsId = 2L;
    	
        when(commentDAO.readAllCommentsByNews(newsId)).thenReturn(new ArrayList<Comment>());

        List<Comment> comments = commentService.getAllCommentsByNews(newsId);
        verify(commentDAO, times(1)).readAllCommentsByNews(newsId);
        assertNotNull(comments);
    }

    @Test
    public void getCommentsCountByNews() throws Exception{
        int expectedCount = 2;
        Long newsId = 2L;

        when(commentDAO.readCommentCountByNewsId(newsId)).thenReturn(expectedCount);
        int count = commentService.getCommentsCountByNews(newsId);
        verify(commentDAO, times(1)).readCommentCountByNewsId(newsId);
        assertEquals(expectedCount, count);
    }

    private Comment makeComment() {

        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("this is a comment");
        comment.setNewsId(1L);
        comment.setCreationDate(new Timestamp(new Date().getTime()));
        return comment;
    }
}