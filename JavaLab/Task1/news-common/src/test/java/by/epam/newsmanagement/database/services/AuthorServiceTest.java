package by.epam.newsmanagement.database.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import by.epam.newsmanagement.database.dao.impl.AuthorDAOImpl;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.services.impl.AuthorServiceImpl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * News service Mockito test class.
 * Class <code>AuthorServiceTest</code> contains Mockito unit tests for all <code>AuthorService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class AuthorServiceTest {

    /**
     * Mock object, which will be inject into AuthorServiceImpl class.
     */
    @Mock
    AuthorDAOImpl authorDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    AuthorServiceImpl authorService;

    @Test
    public void create() throws Exception {

        Author author = makeAuthor();
        when(authorDAO.create(author)).thenReturn(1L);

        Long authorId = authorService.create(author);
        verify(authorDAO, times(1)).create(author);
        assertEquals(authorId, Long.valueOf(1L));
    }

    @Test
    public void editAuthor() throws Exception {

        Author author = makeAuthor();

        authorService.edit(author);
        verify(authorDAO, times(1)).update(author);
    }

    @Test
    public void deleteAuthor() throws Exception {
        Long id = 1L;

        authorService.delete(id);
        verify(authorDAO, times(1)).deleteById(id);
    }

    @Test
    public void getAuthorById() throws Exception {

        Author author = makeAuthor();
        Long authorId = author.getId();
        String authorName = author.getName();

        when(authorDAO.readById(authorId)).thenReturn(author);
        author = authorService.getById(authorId);

        verify(authorDAO, times(1)).readById(authorId);
        assertEquals(authorId, author.getId());
        assertEquals(authorName, author.getName());
    }

    private Author makeAuthor() {

        Author author = new Author();
        author.setId(1L);
        author.setName("Some name");
        return author;
    }
}