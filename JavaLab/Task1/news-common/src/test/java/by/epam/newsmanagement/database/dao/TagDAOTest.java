package by.epam.newsmanagement.database.dao;

import static org.junit.Assert.*;

import by.epam.newsmanagement.database.dao.impl.TagDAOImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.newsmanagement.model.entities.Tag;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup("/DataSetTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })


public class TagDAOTest extends UnitilsJUnit4 {

	@Autowired
	private TagDAOImpl tagDAO;
	public static final Logger LOGGER = Logger.getLogger(TagDAOTest.class);

	@Test
	public void create() throws Exception {
		Long tagId;
        String tagName = "New Tag";

        Tag tag = new Tag();
        tag.setName(tagName);

        tagId = tagDAO.create(tag);
        tag = tagDAO.readById(tagId);

        assertEquals(tag.getId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful created.");
	}
	
	@Test
    public void update() throws Exception {
        Long tagId = 1L;
        String tagName = "Updated Tag";

        Tag tag = new Tag();
        tag.setId(tagId);
        tag.setName(tagName);

        tagDAO.update(tag);
        tag = tagDAO.readById(tagId);

        assertEquals(tag.getId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful updated.");
    }

    @Test
    public void read() throws Exception {
        Long tagId = 2L;
        String tagName = "tag name 2";

        Tag tag = tagDAO.readById(tagId);

        assertEquals(tag.getId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful read.");
    }
    
    @Test
    public void delete() throws Exception {
        Long tagId = 2L;
        
        tagDAO.deleteById(tagId);
       
        Tag tag = tagDAO.readById(tagId);
        assertNull(tag);

        LOGGER.info("The tag was successful deleted.");
    }

    @Test
    public void readAll() throws Exception{
        List<Tag> tagsList = null;
        tagsList = tagDAO.readAllTags();
        assertNotNull(tagsList);
        LOGGER.info("Tags was successful red.");
    }
}