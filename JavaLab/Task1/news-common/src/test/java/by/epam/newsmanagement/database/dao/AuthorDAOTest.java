package by.epam.newsmanagement.database.dao;

import static org.junit.Assert.*;

import java.util.Date;

import by.epam.newsmanagement.database.dao.impl.AuthorDAOImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

import by.epam.newsmanagement.model.entities.Author;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup("/DataSetTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

public class AuthorDAOTest {

	@Autowired
	private AuthorDAOImpl authorDAO;
	
	public static final Logger LOGGER = Logger.getLogger(TagDAOTest.class);

	@Test
	public void create() throws Exception {

		String authorName = "New user";
		Author author = new Author();
		author.setName(authorName);
		author.setExpiredDate(new Date());
		Long authorId = authorDAO.create(author);
		author = authorDAO.readById(authorId);

		assertEquals(author.getName(), authorName);
		assertEquals(author.getId(), authorId);

		LOGGER.info("The author was successful created.");
	}

	@Test
	public void update() throws Exception {

		Long authorId = 2L;
		String authorName = "Billy";

		Author author = new Author();
		author.setName(authorName);
		author.setId(authorId);
		author.setExpiredDate(new Date());
		authorDAO.update(author);
		author = authorDAO.readById(authorId);

		assertEquals(author.getId(), authorId);
		assertEquals(author.getName(), authorName);

		LOGGER.info("The author was successful updated.");
	}

	@Test
	public void read() throws Exception {

		Long authorId = 1L;

		Author author = authorDAO.readById(authorId);

		assertNotNull(author);
		assertEquals(author.getId(), authorId);

		LOGGER.info("The author was successful read.");
	}

	@Test
	public void delete() throws Exception {

		Long authorId = 3L;
		Author author = authorDAO.readById(authorId);
		assertNotNull(author);
		authorDAO.deleteById(authorId);
		author = authorDAO.readById(authorId);
		assertNull(author);

		LOGGER.info("The author was successful deleted.");
	}
}