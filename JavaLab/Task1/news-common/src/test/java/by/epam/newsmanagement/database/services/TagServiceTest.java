package by.epam.newsmanagement.database.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import by.epam.newsmanagement.database.dao.TagDAO;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.impl.TagServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Tag service Mockito test class.
 * Class <code>TagServiceTest</code> contains Mockito unit tests for all <code>TagService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class TagServiceTest {

    /**
     * Mock object, which will be inject into TagServiceImpl class.
     */
    @Mock
    TagDAO tagDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void addTag() throws Exception {

        Tag tag = makeTag();

        when(tagDAO.create(tag)).thenReturn(1L);

        Long tagId = tagService.create(tag);
        verify(tagDAO, times(1)).create(tag);
        assertEquals(tagId, Long.valueOf(1L));
    }

    @Test
    public void deleteTag() throws Exception {

        Long id = 1L;

        tagService.delete(id);
        verify(tagDAO, times(1)).deleteById(id);
    }

    @Test
    public void updateTag() throws Exception {

        Tag tag = makeTag();

        tagService.edit(tag);
        verify(tagDAO, times(1)).update(tag);
    }

    @Test
    public void getTagById() throws Exception {

        Tag tag = makeTag();
        Long tagId = tag.getId();
        String tagName = tag.getName();

        when(tagDAO.readById(tagId)).thenReturn(tag);
        tag = tagService.getById(tagId);

        verify(tagDAO, times(1)).readById(tagId);
        assertEquals(tagId, tag.getId());
        assertEquals(tagName, tag.getName());
    }

    @Test
    public void getAll() throws Exception {

        when(tagDAO.readAllTags()).thenReturn(new ArrayList<Tag>());

        List<Tag> tags = tagService.getAllTags();
        verify(tagDAO, times(1)).readAllTags();
        assertNotNull(tags);
    }

    private Tag makeTag() {

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setName("Tag name");
        return tag;
    }
}
