package by.epam.newsmanagement.database.dao;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.newsmanagement.database.dao.impl.NewsDAOImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.*;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup("/DataSetTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

public class NewsDAOTest extends UnitilsJUnit4 {

	public static final Logger LOGGER = Logger.getLogger(NewsDAOTest.class);

	@Autowired
	private NewsDAOImpl newsDAO;

	@Test
	public void create() throws Exception {
		Long newsId;
		String title = "New news";
		String shortText = "sfhasopifpasifp";
		String fullText = "klsfndlkasnflasnfliasfn";
		Date modificationDate = new Date();

		News news = new News();
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		news.setModificationDate(modificationDate);

		newsId = newsDAO.create(news);
		news = newsDAO.readById(newsId);
		assertEquals(new Long(news.getId()), newsId);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);

		LOGGER.info("The news was successful created.");
	}

	@Test
	public void update() throws Exception {

		Long newsId = 2L;
		String title = "title 2";
		String shortText = "short text 2";
		String fullText = "full text updated";

		News news = new News();
		news.setId(newsId);
		news.setTitle(title);
		news.setShortText(shortText);
		news.setFullText(fullText);
		
		
		newsDAO.update(news);
		news = newsDAO.readById(newsId);

		assertEquals(news.getTitle(), title);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);

		LOGGER.info("The news was successful updated.");
	}

	@Test
	public void read() throws Exception {

		Long newsId = 1L;
		String title = "title 1";
		String shortText = "short text 1";
		String fullText = "full text 1";

		News news = newsDAO.readById(newsId);

		assertEquals(new Long(news.getId()), newsId);
		assertEquals(news.getTitle(), title);
		assertEquals(news.getShortText(), shortText);
		assertEquals(news.getFullText(), fullText);

		LOGGER.info("The news was successful read.");
	}

	@Test
	@ExpectedDataSet("ExpectedNewsDaoDelete.xml")
	public void delete() throws Exception {
		Long newsId = 2L;
		newsDAO.deleteById(newsId);

		LOGGER.info("The news was successful deleted.");
	}

	@Test
	public void readListOfNewsSorted() throws Exception {
		List<News> list = new ArrayList<News>();
		News news1 = newsDAO.readById(2L);
		News news2 = newsDAO.readById(1L);
		News news3 = newsDAO.readById(3L);
		list.add(news1);
		list.add(news2);
		list.add(news3);
		assertEquals(list, newsDAO.readNewsSorted());
		LOGGER.info("The list of news was successful readed.");
	}

	@Test
	@ExpectedDataSet("ExpectedAddToNewsTags.xml")
	public void addToNewsTags() throws Exception {
		Long newsId = 2L;
		Tag tag2 = new Tag();
		tag2.setId(2L);
		List<Tag> list = new ArrayList<Tag>();
		list.add(tag2);

		newsDAO.addToNewsTags(newsId, list);
		LOGGER.info("Tags was successful added.");
	}

	@Test
	@ExpectedDataSet("ExpectedAddToNewsAuthors.xml")
	public void addToNewsAuthors() throws Exception {
		Long newsId = 1L;
		Long authorId = 2L;
		
		newsDAO.addToNewsAuthors(newsId, authorId);
		LOGGER.info("Tags was successful added.");
	}

	@Test
    public void readCountOfNews() throws Exception {

        int expectedCount = 3;
        int count = newsDAO.readCountOfNews();
        assertEquals(expectedCount, count);

        LOGGER.info("The news was successful counted.");
    }
	
	@Test
	public void readNewsBySearchCriteria() throws Exception{
		List<News> list = new ArrayList<News>();
		News news = newsDAO.readById(1L);
		list.add(news);
		
		SearchCriteria sc = new SearchCriteria();
		
		Author author = new Author();
		author.setId(1L);
		sc.setAuthor(author);
		
		List<Tag> listOfTags = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tag1.setId(1L);
		listOfTags.add(tag1);
		Tag tag2 = new Tag();
		tag2.setId(2L);
		listOfTags.add(tag2);
		sc.setTagsList(listOfTags);
		
		assertEquals(list, newsDAO.readNewsBySearchCriteria(sc, -1, -1));
		LOGGER.info("The list of news by Search Criteria was successful readed.");
	}
}
