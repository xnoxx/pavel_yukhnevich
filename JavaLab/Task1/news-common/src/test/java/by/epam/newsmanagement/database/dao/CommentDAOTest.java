package by.epam.newsmanagement.database.dao;

import static org.junit.Assert.*;

import java.util.List;

import by.epam.newsmanagement.database.dao.impl.CommentDAOImpl;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import by.epam.newsmanagement.model.entities.Comment;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup("/DataSetTest.xml")
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class })

public class CommentDAOTest extends UnitilsJUnit4 {

	@Autowired
	private CommentDAOImpl commentDAO;
	public static final Logger LOGGER = Logger.getLogger(CommentDAOTest.class);
	
	@Test
	public void create() throws Exception {
		Long commentId;
        Long newsId = 3L;
        String commentText = "Comment for new News";
      
        Comment comment = new Comment();
        comment.setNewsId(newsId);
        comment.setText(commentText);

        commentId = commentDAO.create(comment);
        comment = commentDAO.readById(commentId);

        assertEquals(comment.getId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getText(), commentText);

        LOGGER.info("The comment was successful created.");		
	}
	
	@Test
    public void read() throws Exception {
        Long commentId = 1L;
        Long newsId = 1L;
        String commentText = "comment text 1";

        Comment comment = commentDAO.readById(commentId);

        assertEquals(comment.getId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getText(), commentText);

        LOGGER.info("The comment was successful read.");
    }
	
	@Test
    public void update() throws Exception {

        Long commentId = 1L;
        Long newsId = 1L;
        String commentText = "Updated Comment";

        Comment comment = new Comment();
        comment.setId(commentId);
        comment.setNewsId(newsId);
        comment.setText(commentText);

        commentDAO.update(comment);
        comment = commentDAO.readById(commentId);

        assertEquals(comment.getId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getText(), commentText);

        LOGGER.info("The comment was successful updated.");
    }
	
	@Test
    public void delete() throws Exception {

        long commentId = 2;
        Comment comment;

        commentDAO.deleteById(commentId);
        comment = commentDAO.readById(commentId);
        assertNull(comment);

        LOGGER.info("The comment was successful deleted.");
    }
	
	@Test
	public void readAllCommentsByNews() throws Exception {
        Long newsId = 1L;
     
        List<Comment> list;
        list = commentDAO.readAllCommentsByNews(newsId);

        assertNotNull(list);

        LOGGER.info("The list of comments was successful read.");
	}

    @Test
    public void readCommentsCountByNewsId() throws Exception {
        Integer expectedCount = 2;
        Integer count = commentDAO.readCommentCountByNewsId(2L);
        assertEquals(expectedCount, count);

        LOGGER.info("The count of comments was successful read.");
    }
}
