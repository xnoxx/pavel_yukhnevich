package by.epam.newsmanagement.services.impl;

import java.util.ArrayList;
import java.util.List;

import by.epam.newsmanagement.database.dao.*;
import by.epam.newsmanagement.model.entities.NewsVO;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.NewsService;

public class NewsServiceImpl implements NewsService {


    private static final Logger LOGGER = Logger.getLogger(NewsService.class);
    private NewsDAO newsDAO;
    private CommentDAO commentDAO;
    private TagDAO tagDAO;
    private AuthorDAO authorDAO;

    @Override
    @Transactional
    public Long createByAuthorWithTags(News news, Long authorId, List<Tag> listOfTags)
            throws GeneralServiceException {
        long id;
        try {
            id = newsDAO.create(news);
            newsDAO.addToNewsTags(id, listOfTags);
            newsDAO.addToNewsAuthors(id, authorId);
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return id;
    }

    @Override
    public News getById(Long id) throws GeneralServiceException {
        News news;
        try {
            news = newsDAO.readById(id);
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return news;
    }

    @Override
    public void edit(News news) throws GeneralServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
    }

    @Override
    @Transactional
    public void editNewsVO(NewsVO newsVO) throws GeneralServiceException {
        try {
            long newsId = newsVO.getNews().getId();

            newsDAO.deleteRefsNewsAuthor(newsId);
            newsDAO.deleteRefsTagsNews(newsId);
            newsDAO.update(newsVO.getNews());

            if (newsVO.getAuthor().getId() != null) {
                newsDAO.addToNewsAuthors(newsId, newsVO.getAuthor().getId());
            }

            if (newsVO.getTagsList() != null) {
                newsDAO.addToNewsTags(newsId, newsVO.getTagsList());
            }

        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
    }

    @Override
    public void delete(Long id) throws GeneralServiceException {
        try {
            newsDAO.deleteById(id);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<News> getListOfNewsSorted() throws GeneralServiceException {
        List<News> list;
        try {
            list = newsDAO.readNewsSorted();
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return list;
    }

    @Override
    @Transactional
    public List<NewsVO> getListOfNewsVO(SearchCriteria searchCriteria, int start, int end) throws GeneralServiceException {
        List<NewsVO> newsVOList;
        try {
            List<News> newsList = newsDAO.readNewsBySearchCriteria(searchCriteria, start, end);
            newsVOList = new ArrayList<>();
            for (News news : newsList) {
                NewsVO newsVO = new NewsVO();
                newsVO.setCommentsList(commentDAO.readAllCommentsByNews(news.getId()));
                newsVO.setTagsList(tagDAO.readListOfTagsByNewsId(news.getId()));
                newsVO.setNews(news);
                newsVO.setAuthor(authorDAO.readByNewsId(news.getId()));
                newsVOList.add(newsVO);
            }
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return newsVOList;
    }

    @Override
    @Transactional
    public NewsVO getSingleNewsVOByNewsId(Long newsId) throws GeneralServiceException {
        NewsVO newsVO = new NewsVO();
        try {
            newsVO.setNews(newsDAO.readById(newsId));
            newsVO.setCommentsList(commentDAO.readAllCommentsByNews(newsId));
            newsVO.setAuthor(authorDAO.readByNewsId(newsId));
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return newsVO;
    }

    @Override
    public Integer getNewsCount() throws GeneralServiceException {
        int count;
        try {
            count = newsDAO.readCountOfNews();
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return count;
    }

    @Override
    public Long create(News entity) throws GeneralServiceException {
        throw new GeneralServiceException("Unsupported method");
    }

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws GeneralServiceException {
        List<News> list;
        try {
            list = newsDAO.readNewsBySearchCriteria(searchCriteria, -1, -1);
        } catch (DAOException e) {
            LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
            throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
        }
        return list;
    }

    public void setNewsDAO(NewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }

    public void setCommentDAO(CommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }
}
