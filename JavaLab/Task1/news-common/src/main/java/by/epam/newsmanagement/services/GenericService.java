package by.epam.newsmanagement.services;

import by.epam.newsmanagement.model.entities.BaseBean;

public interface GenericService<T extends BaseBean> {

	String MESSAGE_SERVICE_EXCEPTION = "Problems with service request";
	
	Long create(T entity) throws GeneralServiceException;
	T getById(Long id) throws GeneralServiceException;
	void edit(T entity) throws GeneralServiceException;
	void delete(Long id) throws GeneralServiceException;
	

}
