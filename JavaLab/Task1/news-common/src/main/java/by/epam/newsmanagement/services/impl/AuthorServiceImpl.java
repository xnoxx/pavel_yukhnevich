package by.epam.newsmanagement.services.impl;

import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.AuthorDAO;
import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.services.AuthorService;
import by.epam.newsmanagement.services.GeneralServiceException;
import org.apache.log4j.Logger;

public class AuthorServiceImpl implements AuthorService {

	private static final Logger LOGGER = Logger.getLogger(AuthorService.class);
	private AuthorDAO authorDAO;

	@Override
	public Long create(Author entity) throws GeneralServiceException {
		Long id;
		try {
			id = authorDAO.create(entity);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}	
		return id;
	}

	@Override
	public Author getById(Long id) throws GeneralServiceException {
		Author author;
		try {
			author = authorDAO.readById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return author;
	}

	@Override
	public void edit(Author entity) throws GeneralServiceException {
		try {
			authorDAO.update(entity);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	@Override
	public void delete(Long id) throws GeneralServiceException {
		try {
			authorDAO.deleteById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}	
	}

	@Override
	public void setExpired(Long id) throws GeneralServiceException {
		try {
			authorDAO.setExpired(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	@Override
	public List<Author> getAllAuthors() throws GeneralServiceException{
		List<Author> authorsList;
		try {
			authorsList = authorDAO.readAllAuthors();
		} catch (DAOException e){
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		} 

		return authorsList;
	}

	@Override
	public List<Author> getAllAuthorsExceptEspired() throws GeneralServiceException{
		List<Author> authorsList;
		try {
			authorsList = authorDAO.readAllAuthorsExceptExpired();
		} catch (DAOException e){
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}

		return authorsList;
	}

	@Override
	public void update(Author author) throws GeneralServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
}
