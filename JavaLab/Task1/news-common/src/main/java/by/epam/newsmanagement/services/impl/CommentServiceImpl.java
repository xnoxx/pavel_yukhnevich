package by.epam.newsmanagement.services.impl;

import java.sql.SQLException;
import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.CommentDAO;
import by.epam.newsmanagement.model.entities.Comment;
import by.epam.newsmanagement.services.CommentService;
import by.epam.newsmanagement.services.GeneralServiceException;
import org.apache.log4j.Logger;

public class CommentServiceImpl implements CommentService {

	private static final Logger LOGGER = Logger.getLogger(CommentService.class);
	private CommentDAO commentDAO;
	
	@Override
	public Long create(Comment comment) throws GeneralServiceException {
		Long id;
		try {
			id = commentDAO.create(comment);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return id;
	}

	@Override
	public Comment getById(Long id) throws GeneralServiceException {
		Comment comment;
		try {
			comment = commentDAO.readById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return comment;
	}

	@Override
	public void edit(Comment comment) throws GeneralServiceException {
		try {
			commentDAO.update(comment);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}	
	}

	@Override
	public void delete(Long id) throws GeneralServiceException {
		try {
			commentDAO.deleteById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	@Override
	public List<Comment> getAllCommentsByNews(Long id) throws GeneralServiceException{
		List<Comment> listOfComments;
		try {
			listOfComments = commentDAO.readAllCommentsByNews(id);
		} catch (SQLException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return listOfComments;
	}

	@Override
	public Integer getCommentsCountByNews(Long id) throws GeneralServiceException{
		Integer count;
		try {
			count = commentDAO.readCommentCountByNewsId(id);
		} catch (SQLException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return count;
	}

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
}
