package by.epam.newsmanagement.model.entities;

public class Tag extends BaseBean {
	
	private String name;

	public Tag(){

	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
