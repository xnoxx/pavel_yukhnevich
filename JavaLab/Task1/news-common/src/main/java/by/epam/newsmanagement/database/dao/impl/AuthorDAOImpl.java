package by.epam.newsmanagement.database.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.BaseDAO;
import by.epam.newsmanagement.database.dao.AuthorDAO;
import by.epam.newsmanagement.model.entities.Author;

public class AuthorDAOImpl extends BaseDAO implements AuthorDAO {

    private static final String SQL_READ_AUTHOR_BY_NEWS_ID = "SELECT * FROM authors JOIN news_authors ON news_authors.AUTHOR_ID = authors.AUTHOR_ID WHERE news_id = ?";
    private static final String SQL_CREATE_AUTHOR = "INSERT INTO authors (author_id, author_name, expired) VALUES (authors_seq.nextval, ?, ?)";
    private static final String SQL_READ_AUTHOR_BY_ID = "SELECT author_id, author_name, expired FROM authors WHERE author_id = ?";
    private static final String SQL_UPDATE_AUTHOR = "UPDATE authors SET author_name = ? WHERE author_id = ?";
    private static final String SQL_DELETE_AUTHOR_BY_ID = "DELETE FROM authors WHERE author_id = ?";
    private static final String SQL_READ_ALL_AUTHORS = "SELECT * FROM authors";
    private static final String SQL_AUTHOR_ID = "author_id";
    private static final String SQL_AUTHOR_NAME = "author_name";
    private static final String SQL_AUTHOR_EXPIRED = "expired";
    private static final String SQL_SET_EXPIRED = "UPDATE authors SET expired = ? WHERE author_id = ?";
    private static final String SQL_READ_ALL_AUTHORS_EXCEPT_EXPIRED = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM authors WHERE expired > SYSDATE OR expired IS NULL";

    public AuthorDAOImpl() {}

    public long create(Author author) throws DAOException {
        long id = 0;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_AUTHOR, new String[]{SQL_AUTHOR_ID});
            statement.setString(1, author.getName());
            if (author.getExpiredDate() != null) {
                statement.setTimestamp(2, new Timestamp(author.getExpiredDate().getTime()));
            } else {
                statement.setTimestamp(2, null);
            }
            int count = statement.executeUpdate();
            if (count != 1) {
                logger.error(MESSAGE_PERSIST_EXCEPTION);
                throw new DAOException(MESSAGE_PERSIST_EXCEPTION + count);
            }
            rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection(rs);
        }
        return id;
    }

    public Author readById(Long id) throws DAOException {
        return read(id, SQL_READ_AUTHOR_BY_ID);
    }

    public void update(Author author) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_AUTHOR);
            statement.setString(1, author.getName());
            statement.setLong(2, author.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                logger.error(MESSAGE_PERSIST_EXCEPTION);
                throw new DAOException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection();
        }
    }

    public void deleteById(Long id) throws DAOException {
        delete(SQL_DELETE_AUTHOR_BY_ID, id);
    }

    @Override
    public List<Author> readAllAuthors() throws DAOException {
        return readAuthors(SQL_READ_ALL_AUTHORS);
    }

    @Override
    public List<Author> readAllAuthorsExceptExpired() throws DAOException {
        return readAuthors(SQL_READ_ALL_AUTHORS_EXCEPT_EXPIRED);
    }

    @Override
    public Author readByNewsId(Long newsId) throws DAOException {
        return read(newsId, SQL_READ_AUTHOR_BY_NEWS_ID);
    }

    @Override
    public void setExpired(Long id) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_SET_EXPIRED);
            statement.setTimestamp(1, new Timestamp(new Date().getTime()));
            statement.setLong(2, id);
            int count = statement.executeUpdate();
            if (count != 1) {
                logger.error(MESSAGE_PERSIST_EXCEPTION);
                throw new DAOException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection();
        }
    }

    private Author read(long id, String sqlQuery) throws DAOException {
        Author author = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                author = buildAuthor(rs);
            }
        } catch (SQLException e) {
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return author;
    }

    private Author buildAuthor(ResultSet rs) throws SQLException{
        Author author = new Author();
        author.setId(rs.getLong(SQL_AUTHOR_ID));
        author.setName(rs.getString(SQL_AUTHOR_NAME));
        if (rs.getTimestamp(SQL_AUTHOR_EXPIRED) != null){
            author.setExpiredDate(new Date(rs.getTimestamp(SQL_AUTHOR_EXPIRED).getTime()));
        }
        return author;
    }

    private List<Author> readAuthors(String sqlQuery) throws DAOException {
        List<Author> authorsList = null;
        ResultSet rs = null;
        try {
            authorsList = new ArrayList<>();
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            rs = statement.executeQuery();
            while (rs.next()) {
                Author author = buildAuthor(rs);
                authorsList.add(author);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return authorsList;
    }
}
