package by.epam.newsmanagement.controller.util;

import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.services.GeneralServiceException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class PageBuilder {

	private static final String PROPERTY_NEWS_ON_PAGE = "news-on-page";
	private static final String FILE_SETTINGS = "settings.properties";
	private int newsOnPageCount = 3;

	public PageBuilder(){
		try {
			newsOnPageCount = getNewsOnPageCount();
		} catch (GeneralServiceException e) {
			e.printStackTrace();
		}
	}

	public int[] getNewsPagesArray(List<NewsVO> newsList) throws GeneralServiceException {

		int newsCount = newsList.size();
		int newsOnPageCount = getNewsOnPageCount();

		int pagesCount = newsCount / newsOnPageCount;
		if ((newsCount - pagesCount * newsOnPageCount) != 0) {
			pagesCount += 1;
		}
		int[] pagesArray = new int[pagesCount];
		for (int i = 0; i < pagesArray.length; i++) {
			pagesArray[i] = i + 1;
		}
		return pagesArray;
	}

	public int getCurrentStart(int currentPage) throws GeneralServiceException {
		return (currentPage - 1) * newsOnPageCount + 1;
	}

	public int getCurrentEnd(int currentPage) throws GeneralServiceException {
		return getCurrentStart(currentPage) + newsOnPageCount - 1;
	}

	private int getNewsOnPageCount() throws GeneralServiceException{
		Properties property = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(FILE_SETTINGS);
		try {
			property.load(inputStream);
		} catch (IOException e) {
			throw new GeneralServiceException(e.getMessage(), e);
		}
		return Integer.parseInt(property.getProperty(PROPERTY_NEWS_ON_PAGE));
	}
}
