package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;
import java.util.List;

public interface NewsDAO extends DefaultOperationsDAO<News> {

    void addToNewsTags(long newsId, List<Tag> listOfTags) throws DAOException;

    void addToNewsAuthors(long newsId, long authorId) throws DAOException;

    List<News> readNewsSorted() throws DAOException;

    Integer readCountOfNews() throws DAOException;

    List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException;

    void deleteRefsNewsAuthor(Long newsId) throws DAOException;

    void deleteRefsTagsNews(Long newsId) throws DAOException;
}
