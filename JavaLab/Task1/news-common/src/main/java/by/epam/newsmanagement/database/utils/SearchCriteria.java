package by.epam.newsmanagement.database.utils;

import java.util.List;

import by.epam.newsmanagement.model.entities.Author;
import by.epam.newsmanagement.model.entities.Tag;

public class SearchCriteria {

	private Author author;
	private List<Tag> listOfTags;
	
	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getListOfTags() {
		return listOfTags;
	}

	public void setTagsList(List<Tag> listOfTags) {
		this.listOfTags = listOfTags;
	}

}
