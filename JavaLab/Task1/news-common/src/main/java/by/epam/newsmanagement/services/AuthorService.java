package by.epam.newsmanagement.services;

import by.epam.newsmanagement.model.entities.Author;

import java.util.List;

public interface AuthorService extends GenericService<Author> {

    void setExpired(Long id) throws GeneralServiceException;

    List<Author> getAllAuthors() throws GeneralServiceException;

    List<Author> getAllAuthorsExceptEspired() throws GeneralServiceException;

    void update(Author author) throws GeneralServiceException;
}
