package by.epam.newsmanagement.services;

import by.epam.newsmanagement.model.entities.Tag;

import java.util.List;

public interface TagService extends GenericService<Tag> {

    List<Tag> getAllTags() throws GeneralServiceException;
}
