package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.model.entities.Tag;

import java.util.List;

public interface TagDAO extends DefaultOperationsDAO<Tag> {

   List<Tag> readListOfTagsByNewsId(Long newsId) throws DAOException;

   List<Tag> readAllTags() throws DAOException;

}
