package by.epam.newsmanagement.services.impl;

import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.TagDAO;
import by.epam.newsmanagement.model.entities.Tag;
import by.epam.newsmanagement.services.GeneralServiceException;
import by.epam.newsmanagement.services.TagService;
import org.apache.log4j.Logger;

public class TagServiceImpl implements TagService {

	private static final Logger LOGGER = Logger.getLogger(TagService.class);
	private TagDAO tagDAO;
	
	@Override
	public Long create(Tag entity) throws GeneralServiceException {
		Long id;
		try {
			id = tagDAO.create(entity);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return id;
	}

	@Override
	public Tag getById(Long id) throws GeneralServiceException {
		Tag tag;
		try {
			tag = tagDAO.readById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return tag;
	}

	@Override
	public void edit(Tag entity) throws GeneralServiceException {
		try {
			tagDAO.update(entity);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	@Override
	public void delete(Long id) throws GeneralServiceException {
		try {
			tagDAO.deleteById(id);
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
	}

	@Override
	public List<Tag> getAllTags() throws GeneralServiceException {
		List<Tag> tagList;
		try {
			tagList = tagDAO.readAllTags();
		} catch (DAOException e) {
			LOGGER.error(MESSAGE_SERVICE_EXCEPTION);
			throw new GeneralServiceException(MESSAGE_SERVICE_EXCEPTION, e);
		}
		return tagList;
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
}
