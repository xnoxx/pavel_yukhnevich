package by.epam.newsmanagement.database.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.BaseDAO;
import by.epam.newsmanagement.database.dao.CommentDAO;
import by.epam.newsmanagement.database.dao.PersistException;
import by.epam.newsmanagement.model.entities.Comment;

public class CommentDAOImpl extends BaseDAO implements CommentDAO {

    private static final String SQL_CREATE_COMMENT = "INSERT INTO comments (comment_id, news_id, comment_text, creation_date) VALUES (comments_seq.nextval, ?, ?, sysdate)";
    private static final String SQL_READ_COMMENT_BY_ID = "SELECT comment_id, news_id, comment_text, creation_date FROM comments WHERE comment_id = ?";
    private static final String SQL_UPDATE_COMMENT = "UPDATE comments SET comment_text = ? WHERE comment_id = ?";
    private static final String SQL_DELETE_COMMENT_BY_ID = "DELETE FROM comments WHERE comment_id = ?";
    private static final String SQL_READ_ALL_COMMENTS_BY_NEWS_ID = "SELECT * FROM comments WHERE NEWS_ID = ?";
    private static final String SQL_READ_COMMENTS_COUNT_BY_NEWS_ID = "SELECT COUNT(*) FROM comments WHERE news_id = ?";
    private static final String SQL_COMMENT_ID = "comment_id";
    private static final String SQL_COMMENT_TEXT = "comment_text";
    private static final String SQL_CREATION_DATE = "creation_date";

    public CommentDAOImpl() {}

    @Override
    public long create(Comment comment) throws DAOException {
        long id = 0;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_COMMENT, new String[]{SQL_COMMENT_ID});
            statement.setLong(1, comment.getNewsId());
            statement.setString(2, comment.getText());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(MESSAGE_PERSIST_EXCEPTION + count);
            }
            rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            logger.info(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection(rs);
        }
        return id;
    }

    @Override
    public Comment readById(Long id) throws DAOException {
        Comment comment = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_COMMENT_BY_ID);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                comment = buildComment(rs);
            }
        } catch (SQLException e) {
            logger.info(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return comment;
    }

    @Override
    public void update(Comment comment) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_COMMENT);
            statement.setString(1, comment.getText());
            statement.setLong(2, comment.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            logger.info(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public void deleteById(Long id) throws DAOException {
        delete(SQL_DELETE_COMMENT_BY_ID, id);
    }

    public List<Comment> readAllCommentsByNews(Long newsId) throws DAOException {
        List<Comment> listOfComments = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_ALL_COMMENTS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            rs = statement.executeQuery();
            listOfComments = new ArrayList<>();
            while (rs.next()) {
                Comment comment = buildComment(rs);
                listOfComments.add(comment);
            }
        } catch (SQLException e) {
            logger.info(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return listOfComments;
    }

    @Override
    public int readCommentCountByNewsId(Long newsId) throws DAOException {
        ResultSet rs = null;
        Integer count = 0;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_COMMENTS_COUNT_BY_NEWS_ID);
            statement.setLong(1, newsId);
            rs = statement.executeQuery();
            if (rs.next()) {
                count = rs.getInt(SQL_COUNT);
            }
            closeConnection();
        } catch (SQLException e) {
            logger.info(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return count;
    }

    private Comment buildComment(ResultSet rs) throws SQLException {
        Comment comment = new Comment();
        comment.setId(rs.getLong(SQL_COMMENT_ID));
        comment.setNewsId(rs.getLong(SQL_NEWS_ID));
        comment.setText(rs.getString(SQL_COMMENT_TEXT));
        comment.setCreationDate(rs.getTimestamp(SQL_CREATION_DATE));
        return comment;
    }
}
