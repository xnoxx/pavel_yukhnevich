package by.epam.newsmanagement.model.entities;

import java.sql.Timestamp;

public class Comment extends BaseBean {

	private Long newsId;
	private String text;
	private Timestamp creationDate;

	public Comment (){

	}

	public Long getNewsId() {
		return newsId;
	}
	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
	
}
