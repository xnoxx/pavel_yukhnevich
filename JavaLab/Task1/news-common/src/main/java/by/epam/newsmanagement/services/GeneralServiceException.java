package by.epam.newsmanagement.services;

public class GeneralServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7299848408534308079L;

	public GeneralServiceException() {
		// TODO Auto-generated constructor stub
	}

	public GeneralServiceException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public GeneralServiceException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public GeneralServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public GeneralServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		// TODO Auto-generated constructor stub
	}

}
