package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.utils.ConnectionManager;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class BaseDAO {

	protected static final String MESSAGE_DAO_PROBLEMS = "Problems with DAO request";
	protected static final String MESSAGE_PERSIST_EXCEPTION = "On persist modify more then 1 record: ";
	protected static final String MESSAGE_CONNECTION_EXCEPTION = "Can not close DAO connection: ";
	protected static final String MESSAGE_OPEN_CONNECTION_EXCEPTION = "Can not close DAO connection: ";
	protected static final String SQL_NEWS_ID = "news_id";
	protected static final String SQL_COUNT = "count(*)";
	protected Logger logger = Logger.getLogger(BaseDAO.class);
	protected Connection connection = null;
	private ConnectionManager manager;

	public BaseDAO() {
	}
	
	public Connection getConnection() throws DAOException {
		try {
			connection = manager.getConnection();
		} catch (SQLException e) {
			logger.error(MESSAGE_OPEN_CONNECTION_EXCEPTION);
			throw new DAOException(MESSAGE_OPEN_CONNECTION_EXCEPTION, e);
		}
		return connection;
	}
	
	public void closeConnection(ResultSet rs) throws DAOException {
		try {
			manager.releaseConnection(connection);
			if(rs != null)
				rs.close();
		} catch (SQLException e) {
			logger.error(MESSAGE_CONNECTION_EXCEPTION, e);
			throw new DAOException(MESSAGE_CONNECTION_EXCEPTION, e);
		}
	}

	public void closeConnection() throws DAOException {
			manager.releaseConnection(connection);
	}
	protected void delete(String sqlQuery, long id) throws DAOException {
		try {
			getConnection();
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			statement.setLong(1, id);
			statement.executeQuery();
		} catch (SQLException e) {
			throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
		} finally {
			closeConnection();
		}

	}

	public void setManager(by.epam.newsmanagement.database.utils.ConnectionManager manager) {
		this.manager = manager;
	}
}
