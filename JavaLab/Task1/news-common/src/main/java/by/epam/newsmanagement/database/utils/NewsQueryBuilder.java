package by.epam.newsmanagement.database.utils;

import by.epam.newsmanagement.model.entities.Tag;

import java.util.List;

public class NewsQueryBuilder {

    private static final String SQL_BASE = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE " +
            "FROM (SELECT A.NEWS_ID, A.TITLE, A.SHORT_TEXT, A.FULL_TEXT, A.CREATION_DATE, A.MODIFICATION_DATE, ROWNUM RNUM " +
            "FROM (SELECT N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE, COUNT(DISTINCT CO.COMMENT_ID) QTY " +
            "FROM NEWS N LEFT JOIN COMMENTS CO ON N.NEWS_ID = CO.NEWS_ID ";
    private static final String SQL_AUTHORS_TAGS_JOIN = "LEFT JOIN NEWS_AUTHORS NA ON N.NEWS_ID = NA.NEWS_ID LEFT JOIN NEWS_TAGS NT ON N.NEWS_ID = NT.NEWS_ID ";
    private static final String SQL_AND = "AND ";
    private static final String SQL_WHERE = "WHERE ";
    private static final String SQL_TAG_ID_OPEN = "tag_id IN (";
    private static final String SQL_AUTHOR = "WHERE author_id = ? ";
    private static final String SQL_ORDER_BY = "GROUP BY N.NEWS_ID, N.TITLE, N.SHORT_TEXT, N.FULL_TEXT, N.CREATION_DATE, N.MODIFICATION_DATE ORDER BY QTY DESC, N.MODIFICATION_DATE DESC) A ) ";
    private static final String SQL_START = "WHERE RNUM  >= ? ";
    private static final String SQL_END = "AND RNUM <= ?";
    private static final String SQL_TAG_ID_CLOSE = ") ";
    private static final int EMPTY = -1;
    private StringBuilder query;
    private SearchCriteria sc;
    private Integer start;
    private Integer end;

    public NewsQueryBuilder(SearchCriteria searchCriteria, int start, int end) {
        this.start = start;
        this.end = end;
        this.sc = searchCriteria;
        query = new StringBuilder();
    }

    public String getBuiltQuery() {
        query.append(SQL_BASE);

        if (isSearchCriteriaEmpty()) {
            appendOrderByAndPage();
            return query.toString();
        } else {
           // query.append(SQL_AUTHORS_TAGS_JOIN);
        }

        query.append(SQL_AUTHORS_TAGS_JOIN);

        if (sc.getAuthor().getId() != null) {
            String authorId = sc.getAuthor().getId().toString();
            query.append(replaceQuestionMarkByValues(SQL_AUTHOR, authorId));
        }

        if (sc.getListOfTags() != null && !sc.getListOfTags().isEmpty()) {

            if (sc.getAuthor().getId() != null) {
                query.append(SQL_AND);
            } else {
                query.append(SQL_WHERE);
            }

            List<Tag> tags = sc.getListOfTags();

            query.append(SQL_TAG_ID_OPEN);

            for (int i = 0; i < tags.size(); i++) {
                String tagId = tags.get(i).getId().toString();
                query.append(tagId);
                if (i != tags.size() - 1) {
                    query.append(",");
                }
            }
            query.append(SQL_TAG_ID_CLOSE);
        }

        appendOrderByAndPage();

        return query.toString();
    }

    private String replaceQuestionMarkByValues(String stringWithValues, String newValue) {
        return stringWithValues.replace("?", newValue);
    }

    private boolean isSearchCriteriaEmpty() {
        boolean bool = false;
        if (sc != null) {
            if (sc.getListOfTags() == null && sc.getAuthor() == null) {
                bool = true;
            }
        } else {
            bool = true;
        }

        return bool;
    }

    private void appendOrderByAndPage() {
        query.append(SQL_ORDER_BY);

        if (start != EMPTY && end != EMPTY) {
            query.append(replaceQuestionMarkByValues(SQL_START, start.toString()));
            query.append(replaceQuestionMarkByValues(SQL_END, end.toString()));
        }
    }
}

