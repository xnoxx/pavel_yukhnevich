package by.epam.newsmanagement.services;

import java.util.List;

import by.epam.newsmanagement.model.entities.Comment;

public interface CommentService extends GenericService<Comment> {

	List<Comment> getAllCommentsByNews(Long id) throws GeneralServiceException;
	Integer getCommentsCountByNews(Long id) throws GeneralServiceException;
}
