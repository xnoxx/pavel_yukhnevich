package by.epam.newsmanagement.model.entities;

public class BaseBean {

	public BaseBean(){

	}

	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
