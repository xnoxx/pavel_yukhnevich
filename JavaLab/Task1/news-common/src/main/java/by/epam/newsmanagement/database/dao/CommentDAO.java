package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.model.entities.Comment;

import java.util.List;


public interface CommentDAO extends DefaultOperationsDAO<Comment> {

    List<Comment> readAllCommentsByNews(Long newsId) throws DAOException;
    int readCommentCountByNewsId(Long newsId) throws DAOException;

}
