package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.model.entities.BaseBean;

public interface DefaultOperationsDAO <T extends BaseBean>{

	long create(T t) throws DAOException;
	T readById(Long id) throws DAOException;
	void update(T t) throws DAOException;
	void deleteById(Long id) throws DAOException;

}
