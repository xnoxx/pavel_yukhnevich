package by.epam.newsmanagement.database.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.dao.BaseDAO;
import by.epam.newsmanagement.database.dao.PersistException;
import by.epam.newsmanagement.database.dao.TagDAO;
import by.epam.newsmanagement.model.entities.Tag;

public class TagDAOImpl extends BaseDAO implements TagDAO {

    private static final String SQL_CREATE_TAG = "INSERT INTO tags (tag_id, tag_name) VALUES (tags_seq.nextval, ?)";
    private static final String SQL_READ_BY_TAG_ID = "SELECT tag_id, tag_name FROM tags WHERE tag_id = ?";
    private static final String SQL_UPDATE_TAG = "UPDATE tags SET tag_name = ? WHERE tag_id = ?";
    private static final String SQL_DELETE_BY_TAG_ID = "DELETE FROM tags WHERE tag_id = ?";
    private static final String SQL_READ_ALL_TAGS_BY_NEWS_ID = "SELECT * FROM tags INNER JOIN news_tags ON tags.tag_id = news_tags.TAG_ID WHERE NEWS_ID = ?";
    private static final String SQL_READ_ALL = "SELECT * FROM tags";
    private static final String SQL_TAG_ID = "tag_id";
    private static final String SQL_TAG_NAME = "tag_name";

    public TagDAOImpl() {}

    @Override
    public long create(Tag tag) throws DAOException {
        long id = 0;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_TAG, new String[]{SQL_TAG_ID});
            statement.setString(1, tag.getName());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(MESSAGE_PERSIST_EXCEPTION + count);
            }
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection();
        }
        return id;
    }

    @Override
    public Tag readById(Long id) throws DAOException {
        Tag tag = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_BY_TAG_ID);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                tag = buildTag(rs);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return tag;
    }

    @Override
    public void update(Tag tag) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_TAG);
            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getId());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new PersistException(e);
        } finally {
            closeConnection();
        }

    }

    @Override
    public void deleteById(Long id) throws DAOException {
        delete(SQL_DELETE_BY_TAG_ID, id);

    }

    @Override
    public List<Tag> readListOfTagsByNewsId(Long newsId) throws DAOException {
        List<Tag> listOfTags = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_ALL_TAGS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            rs = statement.executeQuery();
            listOfTags = buildTagsList(rs);
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return listOfTags;
    }

    @Override
    public List<Tag> readAllTags() throws DAOException {
        List<Tag> listOfTags = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_ALL);
            rs = statement.executeQuery();
            listOfTags = buildTagsList(rs);
        } catch (SQLException e) {
            logger.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return listOfTags;
    }


    private List<Tag> buildTagsList(ResultSet rs) throws SQLException {
        List<Tag> listOfTags = new ArrayList<>();
        while (rs.next()) {
            Tag tag = buildTag(rs);
            listOfTags.add(tag);
        }
        return listOfTags;
    }

    private Tag buildTag(ResultSet rs) throws SQLException{
        Tag tag = new Tag();
        tag.setId(rs.getLong(SQL_TAG_ID));
        tag.setName(rs.getString(SQL_TAG_NAME));
        return tag;
    }
}