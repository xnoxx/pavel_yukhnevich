package by.epam.newsmanagement.database;

import java.sql.SQLException;

public class ConnectionException extends Throwable {
    public ConnectionException(String message, SQLException e) {
        super(message, e);
    }

    public ConnectionException(String message) {
        super(message);
    }

    public ConnectionException(SQLException e) {
        super(e);
    }
}
