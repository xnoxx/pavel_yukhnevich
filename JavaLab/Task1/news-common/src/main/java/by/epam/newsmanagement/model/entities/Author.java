package by.epam.newsmanagement.model.entities;

import java.util.Date;

public class Author extends BaseBean {

	private String name;
	private Date expiredDate;

	public Author(){

	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
}
