package by.epam.newsmanagement.database.dao;


import by.epam.newsmanagement.database.DAOException;

public class PersistException extends DAOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 620293651987782142L;
	
	public PersistException(String message) {
		super(message);
	}

	public PersistException(Exception e) {
		super(e);
	}
	
	public PersistException(String message, Exception e) {
		super(message, e);
	}

}
