package by.epam.newsmanagement.controller.util;

import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.services.GeneralServiceException;

import java.util.List;

public class Navigator {

	public NewsVO getNextNewsVO(List<NewsVO> newsVOList, Long newsId) throws GeneralServiceException {

		NewsVO newsVO;
		int size = newsVOList.size();
		int nextNewsIndex = 0;

		for (int i = 0; i < size; i++) {
			Long currentNewsId = newsVOList.get(i).getNews().getId();
			if (currentNewsId.equals(newsId)) {
				nextNewsIndex = i + 1;
				break;
			}
		}

		if (nextNewsIndex >= size) {
			newsVO = newsVOList.get(0);
		} else {
			newsVO = newsVOList.get(nextNewsIndex);
		}

		return newsVO;
	}

	public NewsVO getPreviousNewsVO(List<NewsVO> newsVOList, Long newsId) throws GeneralServiceException {

		NewsVO newsVO;
		int size = newsVOList.size();
		int previousNewsIndex = 0;
		for (int i = 0; i < size; i++) {
			Long currentNewsId = newsVOList.get(i).getNews().getId();
			if (currentNewsId.equals(newsId)) {
				previousNewsIndex = i - 1;
				break;
			}
		}

		if (previousNewsIndex < 0) {
			newsVO = newsVOList.get(size - 1);
		} else {
			newsVO = newsVOList.get(previousNewsIndex);
		}

		return newsVO;
	}
}
