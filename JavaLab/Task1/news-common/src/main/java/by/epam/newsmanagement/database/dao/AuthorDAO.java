package by.epam.newsmanagement.database.dao;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.model.entities.Author;

import java.util.List;

public interface AuthorDAO extends DefaultOperationsDAO<Author> {


    List<Author> readAllAuthors() throws DAOException;

    List<Author> readAllAuthorsExceptExpired() throws DAOException;

    Author readByNewsId(Long newsId) throws DAOException;

    void setExpired(Long id) throws DAOException;
}
