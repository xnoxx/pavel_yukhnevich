package by.epam.newsmanagement.database.utils;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionManager {

    private BasicDataSource dataSource;

    public ConnectionManager(BasicDataSource dataSource){
        this.dataSource = dataSource;
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void releaseConnection(Connection connection){
        DataSourceUtils.releaseConnection(connection, dataSource);
    }
}
