package by.epam.newsmanagement.database.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.newsmanagement.database.dao.BaseDAO;
import by.epam.newsmanagement.database.dao.NewsDAO;
import by.epam.newsmanagement.database.dao.PersistException;
import org.apache.log4j.Logger;

import by.epam.newsmanagement.database.DAOException;
import by.epam.newsmanagement.database.utils.NewsQueryBuilder;
import by.epam.newsmanagement.database.utils.SearchCriteria;
import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.Tag;

public class NewsDAOImpl extends BaseDAO implements NewsDAO {

    private static final Logger LOGGER = Logger.getLogger(NewsDAOImpl.class);

    private static final String SQL_CREATE_NEWS = "INSERT INTO news (news_id, title, short_text, full_text, creation_date, modification_date) VALUES (news_seq.nextval, ?, ?, ?, sysdate, sysdate)";
    private static final String SQL_READ_BY_NEWS_ID = "SELECT news_id, title, short_text, full_text, creation_date, modification_date FROM news WHERE news_id = ?";
    private static final String SQL_UPDATE_NEWS = "UPDATE news SET title = ?, short_text = ?, full_text = ?, modification_date = sysdate WHERE news_id = ?";
    private static final String SQL_DELETE_NEWS_BY_ID = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    private static final String SQL_ADD_TO_NEWS_TAGS = "INSERT INTO news_tags (news_id, tag_id) VALUES (?, ?)";
    private static final String SQL_ADD_TO_NEWS_AUTHORS = "INSERT INTO news_authors (news_id, author_id) VALUES (?, ?)";
    private static final String SQL_READ_LIST_OF_NEWS_SORTED = "SELECT n.*, c.cntr FROM NEWS n LEFT JOIN (SELECT news_id, count(*) cntr FROM COMMENTS GROUP BY news_id) c ON n.news_id = c.news_id ORDER BY nvl(c.cntr, 0) desc";
    private static final String SQL_READ_COUNT_OF_NEWS = "SELECT COUNT(*) FROM news";
    private static final String SQL_DELETE_NEWS_TAGS_REFS = "DELETE FROM news_tags WHERE NEWS_ID = ?";
    private static final String SQL_DELETE_NEWS_AUTHORS_REFS = "DELETE FROM news_authors WHERE NEWS_ID = ?";
    private static final String SQL_TITLE = "title";
    private static final String SQL_SHORT_TEXT = "short_text";
    private static final String SQL_FULL_TEXT = "full_text";
    private static final String SQL_CREATION_DATE = "creation_date";
    private static final String SQL_MODIFICATION_DATE = "modification_date";

    public NewsDAOImpl() {
    }

    @Override
    public long create(News news) throws DAOException {
        long id = 0;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_NEWS, new String[]{SQL_NEWS_ID});
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new PersistException(MESSAGE_PERSIST_EXCEPTION + count);
            }
            rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1);
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection(rs);
        }
        return id;
    }

    public void addToNewsTags(long newsId, List<Tag> listOfTags) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_ADD_TO_NEWS_TAGS);

            for (Tag tag : listOfTags) {
                statement.setLong(1, newsId);
                statement.setLong(2, tag.getId());
                statement.addBatch();
            }

            statement.executeBatch();

        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    public void addToNewsAuthors(long newsId, long authorId) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_ADD_TO_NEWS_AUTHORS);
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public News readById(Long id) throws DAOException {
        News news = null;
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_BY_NEWS_ID);
            statement.setLong(1, id);
            rs = statement.executeQuery();
            if (rs.next()) {
                news = buildNews(rs);
            }
            closeConnection();
        } catch (SQLException e) {
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return news;
    }

    @Override
    public void update(News news) throws DAOException {
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_NEWS);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setLong(4, news.getId());
            logger.info(news.toString());
            int count = statement.executeUpdate();
            if (count != 1) {
                throw new DAOException(MESSAGE_PERSIST_EXCEPTION + count);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeConnection();
        }
    }

    @Override
    public void deleteById(Long id) throws DAOException {
        delete(SQL_DELETE_NEWS_BY_ID, id);
    }

    public List<News> readNewsSorted() throws DAOException {
        List<News> listOfNews = new ArrayList<>();
        ResultSet rs = null;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_LIST_OF_NEWS_SORTED);
            rs = statement.executeQuery();
            while (rs.next()) {
                News news = buildNews(rs);
                listOfNews.add(news);
            }
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return listOfNews;
    }

    public Integer readCountOfNews() throws DAOException {
        ResultSet rs = null;
        Integer count = 0;
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_READ_COUNT_OF_NEWS);
            rs = statement.executeQuery();
            if (rs.next()) {
                count = rs.getInt(SQL_COUNT);
            }
            closeConnection();
        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return count;
    }

    public List<News> readNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException {
        List<News> listOfNews = null;
        ResultSet rs = null;
        NewsQueryBuilder queryBuilder = new NewsQueryBuilder(searchCriteria, start, end);
        String sqlQuery = queryBuilder.getBuiltQuery();
        try {
            this.connection = getConnection();
            PreparedStatement statement = connection.prepareStatement(sqlQuery);
            rs = statement.executeQuery();
            listOfNews = new ArrayList<>();

            while (rs.next()) {
                News news = buildNews(rs);
                listOfNews.add(news);
            }

        } catch (SQLException e) {
            LOGGER.error(MESSAGE_DAO_PROBLEMS, e);
            throw new DAOException(MESSAGE_DAO_PROBLEMS, e);
        } finally {
            closeConnection(rs);
        }
        return listOfNews;
    }

    @Override
    public void deleteRefsNewsAuthor(Long newsId) throws DAOException {
        delete(SQL_DELETE_NEWS_AUTHORS_REFS, newsId);
    }

    @Override
    public void deleteRefsTagsNews(Long newsId) throws DAOException {
        delete(SQL_DELETE_NEWS_TAGS_REFS, newsId);
    }

    private News buildNews(ResultSet rs) throws SQLException {
        News news = new News();
        news.setId(rs.getLong(SQL_NEWS_ID));
        news.setTitle(rs.getString(SQL_TITLE));
        news.setShortText(rs.getString(SQL_SHORT_TEXT));
        news.setFullText(rs.getString(SQL_FULL_TEXT));
        news.setCreationDate(rs.getTimestamp(SQL_CREATION_DATE));
        news.setModificationDate(rs.getTimestamp(SQL_MODIFICATION_DATE));
        return news;
    }
}