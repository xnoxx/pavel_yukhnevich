package by.epam.newsmanagement.services;

import java.util.List;

import by.epam.newsmanagement.model.entities.News;
import by.epam.newsmanagement.model.entities.NewsVO;
import by.epam.newsmanagement.model.entities.Tag;

import by.epam.newsmanagement.database.utils.SearchCriteria;

public interface NewsService extends GenericService<News> {

	Long createByAuthorWithTags(News news, Long authorId, List<Tag> listOfTags) throws GeneralServiceException;
	Integer getNewsCount() throws GeneralServiceException;
	List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws GeneralServiceException;
	List<News> getListOfNewsSorted() throws GeneralServiceException;
	List<NewsVO> getListOfNewsVO(SearchCriteria searchCriteria, int i, int newsCount) throws GeneralServiceException;
	NewsVO getSingleNewsVOByNewsId(Long newsId) throws GeneralServiceException;
	void editNewsVO(NewsVO newsVO) throws GeneralServiceException;
}
